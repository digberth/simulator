/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.porvenir.luis.portafolio.model.Diversificado;
import com.porvenir.luis.portafolio.service.base.DiversificadoLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the diversificado local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.porvenir.luis.portafolio.service.DiversificadoLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DiversificadoLocalServiceBaseImpl
 */
public class DiversificadoLocalServiceImpl
	extends DiversificadoLocalServiceBaseImpl {

	public List<Diversificado> getByScore(int score){
		return diversificadoPersistence.findByScore(score);
	}
	
	public Diversificado addDiversificado(long userId, int score, String nombre, double sumaPeso, String factorRiesgo, double descobertura , ServiceContext context) throws PortalException {
		
		long groupId = context.getScopeGroupId();
		User user = userLocalService.getUserById(userId);
		Date now = new Date();
		
		long divId = counterLocalService.increment();
		Diversificado diversificado = diversificadoPersistence.create(divId);
		
		diversificado.setUuid(context.getUuid());
		diversificado.setUserId(userId);
		diversificado.setGroupId(groupId);
		diversificado.setCompanyId(user.getCompanyId());
		diversificado.setUserName(user.getFullName());
		diversificado.setCreateDate(context.getCreateDate(now));
		diversificado.setModifiedDate(context.getModifiedDate(now));
		
		diversificado.setScore(score);
		diversificado.setNombre(nombre);
		diversificado.setSuma_peso(sumaPeso);
		diversificado.setFactor_riesgo(factorRiesgo);
		diversificado.setDescobertura(descobertura);
		
		diversificadoPersistence.update(diversificado);
		
		return diversificado;
		
	}
	
	public void deleteAll() {
		diversificadoPersistence.removeAll();
	}
}