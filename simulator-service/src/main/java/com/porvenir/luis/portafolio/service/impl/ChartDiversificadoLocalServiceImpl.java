/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.porvenir.luis.portafolio.exception.NoSuchChartDiversificadoException;
import com.porvenir.luis.portafolio.model.ChartDiversificado;
import com.porvenir.luis.portafolio.service.base.ChartDiversificadoLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the chart diversificado local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.porvenir.luis.portafolio.service.ChartDiversificadoLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartDiversificadoLocalServiceBaseImpl
 */
public class ChartDiversificadoLocalServiceImpl
	extends ChartDiversificadoLocalServiceBaseImpl {

	public ChartDiversificado findByName_Time(String name, int time) throws NoSuchChartDiversificadoException {
		return chartDiversificadoPersistence.findByNombre_Tiempo(name, time);
	}
	
	public List<ChartDiversificado> getByName(String name, int max){
		return chartDiversificadoPersistence.findByNombre(name, 0, max);
	}
	
	public ChartDiversificado addChartDiversificado(long userId, String nombre, int tiempo, double inferior, double medio, double superior , ServiceContext context) throws PortalException {
		
		long groupId = context.getScopeGroupId();
		User user = userLocalService.getUserById(userId);
		Date now = new Date();
		
		long divId = counterLocalService.increment();
		ChartDiversificado chartDiversificado = chartDiversificadoPersistence.create(divId);
		
		chartDiversificado.setUuid(context.getUuid());
		chartDiversificado.setUserId(userId);
		chartDiversificado.setGroupId(groupId);
		chartDiversificado.setCompanyId(user.getCompanyId());
		chartDiversificado.setUserName(user.getFullName());
		chartDiversificado.setCreateDate(context.getCreateDate(now));
		chartDiversificado.setModifiedDate(context.getModifiedDate(now));
		
		chartDiversificado.setNombre(nombre);
		chartDiversificado.setTiempo(tiempo);
		chartDiversificado.setInferior(inferior);
		chartDiversificado.setMedio(medio);
		chartDiversificado.setSuperior(superior);
		
		chartDiversificadoPersistence.update(chartDiversificado);
		
		return chartDiversificado;
		
	}
	
	public void deleteAll() {
		chartDiversificadoPersistence.removeAll();
	}
}