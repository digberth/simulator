/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import com.porvenir.luis.portafolio.model.Pura;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Pura in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class PuraCacheModel implements CacheModel<Pura>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PuraCacheModel)) {
			return false;
		}

		PuraCacheModel puraCacheModel = (PuraCacheModel)obj;

		if (divId == puraCacheModel.divId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, divId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", divId=");
		sb.append(divId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", score=");
		sb.append(score);
		sb.append(", alternativa=");
		sb.append(alternativa);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", peso=");
		sb.append(peso);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Pura toEntityModel() {
		PuraImpl puraImpl = new PuraImpl();

		if (uuid == null) {
			puraImpl.setUuid("");
		}
		else {
			puraImpl.setUuid(uuid);
		}

		puraImpl.setDivId(divId);
		puraImpl.setGroupId(groupId);
		puraImpl.setCompanyId(companyId);
		puraImpl.setUserId(userId);

		if (userName == null) {
			puraImpl.setUserName("");
		}
		else {
			puraImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			puraImpl.setCreateDate(null);
		}
		else {
			puraImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			puraImpl.setModifiedDate(null);
		}
		else {
			puraImpl.setModifiedDate(new Date(modifiedDate));
		}

		puraImpl.setScore(score);

		if (alternativa == null) {
			puraImpl.setAlternativa("");
		}
		else {
			puraImpl.setAlternativa(alternativa);
		}

		if (nombre == null) {
			puraImpl.setNombre("");
		}
		else {
			puraImpl.setNombre(nombre);
		}

		puraImpl.setPeso(peso);

		puraImpl.resetOriginalValues();

		return puraImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		divId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		score = objectInput.readInt();
		alternativa = objectInput.readUTF();
		nombre = objectInput.readUTF();

		peso = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(divId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeInt(score);

		if (alternativa == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(alternativa);
		}

		if (nombre == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		objectOutput.writeDouble(peso);
	}

	public String uuid;
	public long divId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public int score;
	public String alternativa;
	public String nombre;
	public double peso;

}