/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import com.porvenir.luis.portafolio.model.Diversificado;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Diversificado in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class DiversificadoCacheModel
	implements CacheModel<Diversificado>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DiversificadoCacheModel)) {
			return false;
		}

		DiversificadoCacheModel diversificadoCacheModel =
			(DiversificadoCacheModel)obj;

		if (divId == diversificadoCacheModel.divId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, divId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", divId=");
		sb.append(divId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", score=");
		sb.append(score);
		sb.append(", nombre=");
		sb.append(nombre);
		sb.append(", suma_peso=");
		sb.append(suma_peso);
		sb.append(", factor_riesgo=");
		sb.append(factor_riesgo);
		sb.append(", descobertura=");
		sb.append(descobertura);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Diversificado toEntityModel() {
		DiversificadoImpl diversificadoImpl = new DiversificadoImpl();

		if (uuid == null) {
			diversificadoImpl.setUuid("");
		}
		else {
			diversificadoImpl.setUuid(uuid);
		}

		diversificadoImpl.setDivId(divId);
		diversificadoImpl.setGroupId(groupId);
		diversificadoImpl.setCompanyId(companyId);
		diversificadoImpl.setUserId(userId);

		if (userName == null) {
			diversificadoImpl.setUserName("");
		}
		else {
			diversificadoImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			diversificadoImpl.setCreateDate(null);
		}
		else {
			diversificadoImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			diversificadoImpl.setModifiedDate(null);
		}
		else {
			diversificadoImpl.setModifiedDate(new Date(modifiedDate));
		}

		diversificadoImpl.setScore(score);

		if (nombre == null) {
			diversificadoImpl.setNombre("");
		}
		else {
			diversificadoImpl.setNombre(nombre);
		}

		diversificadoImpl.setSuma_peso(suma_peso);

		if (factor_riesgo == null) {
			diversificadoImpl.setFactor_riesgo("");
		}
		else {
			diversificadoImpl.setFactor_riesgo(factor_riesgo);
		}

		diversificadoImpl.setDescobertura(descobertura);

		diversificadoImpl.resetOriginalValues();

		return diversificadoImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		divId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		score = objectInput.readInt();
		nombre = objectInput.readUTF();

		suma_peso = objectInput.readDouble();
		factor_riesgo = objectInput.readUTF();

		descobertura = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(divId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeInt(score);

		if (nombre == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nombre);
		}

		objectOutput.writeDouble(suma_peso);

		if (factor_riesgo == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(factor_riesgo);
		}

		objectOutput.writeDouble(descobertura);
	}

	public String uuid;
	public long divId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public int score;
	public String nombre;
	public double suma_peso;
	public String factor_riesgo;
	public double descobertura;

}