/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.porvenir.luis.portafolio.exception.NoSuchChartPuraException;
import com.porvenir.luis.portafolio.model.ChartPura;
import com.porvenir.luis.portafolio.service.base.ChartPuraLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the chart pura local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.porvenir.luis.portafolio.service.ChartPuraLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartPuraLocalServiceBaseImpl
 */
public class ChartPuraLocalServiceImpl extends ChartPuraLocalServiceBaseImpl {

	public ChartPura findByScore_Time(int score, int time) throws NoSuchChartPuraException {
		return chartPuraPersistence.findByScore_Tiempo(score, time);
	}
	
	public List<ChartPura> getByScore(int score, int max){
		return chartPuraPersistence.findByScore(score, 0, max);
	}
	
	public ChartPura addChartPura(long userId, int score, int tiempo, double inferior, double medio, double superior , ServiceContext context) throws PortalException {
		
		long groupId = context.getScopeGroupId();
		User user = userLocalService.getUserById(userId);
		Date now = new Date();
		
		long divId = counterLocalService.increment();
		ChartPura chartPura = chartPuraPersistence.create(divId);
		
		chartPura.setUuid(context.getUuid());
		chartPura.setUserId(userId);
		chartPura.setGroupId(groupId);
		chartPura.setCompanyId(user.getCompanyId());
		chartPura.setUserName(user.getFullName());
		chartPura.setCreateDate(context.getCreateDate(now));
		chartPura.setModifiedDate(context.getModifiedDate(now));
		
		chartPura.setScore(score);
		chartPura.setTiempo(tiempo);
		chartPura.setInferior(inferior);
		chartPura.setMedio(medio);
		chartPura.setSuperior(superior);
		
		chartPuraPersistence.update(chartPura);
		
		return chartPura;
		
	}
	
	public void deleteAll() {
		chartPuraPersistence.removeAll();
	}
}