/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.porvenir.luis.portafolio.exception.NoSuchDiversificadoException;
import com.porvenir.luis.portafolio.model.Diversificado;
import com.porvenir.luis.portafolio.model.impl.DiversificadoImpl;
import com.porvenir.luis.portafolio.model.impl.DiversificadoModelImpl;
import com.porvenir.luis.portafolio.service.persistence.DiversificadoPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the diversificado service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class DiversificadoPersistenceImpl
	extends BasePersistenceImpl<Diversificado>
	implements DiversificadoPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>DiversificadoUtil</code> to access the diversificado persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		DiversificadoImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid;
			finderArgs = new Object[] {uuid};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<Diversificado> list = null;

		if (retrieveFromCache) {
			list = (List<Diversificado>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Diversificado diversificado : list) {
					if (!uuid.equals(diversificado.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(DiversificadoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado findByUuid_First(
			String uuid, OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByUuid_First(
			uuid, orderByComparator);

		if (diversificado != null) {
			return diversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchDiversificadoException(msg.toString());
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByUuid_First(
		String uuid, OrderByComparator<Diversificado> orderByComparator) {

		List<Diversificado> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado findByUuid_Last(
			String uuid, OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByUuid_Last(uuid, orderByComparator);

		if (diversificado != null) {
			return diversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchDiversificadoException(msg.toString());
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByUuid_Last(
		String uuid, OrderByComparator<Diversificado> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<Diversificado> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado[] findByUuid_PrevAndNext(
			long divId, String uuid,
			OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		uuid = Objects.toString(uuid, "");

		Diversificado diversificado = findByPrimaryKey(divId);

		Session session = null;

		try {
			session = openSession();

			Diversificado[] array = new DiversificadoImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, diversificado, uuid, orderByComparator, true);

			array[1] = diversificado;

			array[2] = getByUuid_PrevAndNext(
				session, diversificado, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Diversificado getByUuid_PrevAndNext(
		Session session, Diversificado diversificado, String uuid,
		OrderByComparator<Diversificado> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DIVERSIFICADO_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DiversificadoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						diversificado)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Diversificado> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the diversificados where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (Diversificado diversificado :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(diversificado);
		}
	}

	/**
	 * Returns the number of diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching diversificados
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"diversificado.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(diversificado.uuid IS NULL OR diversificado.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDiversificadoException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado findByUUID_G(String uuid, long groupId)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByUUID_G(uuid, groupId);

		if (diversificado == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchDiversificadoException(msg.toString());
		}

		return diversificado;
	}

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = new Object[] {uuid, groupId};

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof Diversificado) {
			Diversificado diversificado = (Diversificado)result;

			if (!Objects.equals(uuid, diversificado.getUuid()) ||
				(groupId != diversificado.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_DIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<Diversificado> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByUUID_G, finderArgs, list);
				}
				else {
					Diversificado diversificado = list.get(0);

					result = diversificado;

					cacheResult(diversificado);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByUUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (Diversificado)result;
		}
	}

	/**
	 * Removes the diversificado where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the diversificado that was removed
	 */
	@Override
	public Diversificado removeByUUID_G(String uuid, long groupId)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = findByUUID_G(uuid, groupId);

		return remove(diversificado);
	}

	/**
	 * Returns the number of diversificados where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching diversificados
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"diversificado.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(diversificado.uuid IS NULL OR diversificado.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"diversificado.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid_C;
			finderArgs = new Object[] {uuid, companyId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<Diversificado> list = null;

		if (retrieveFromCache) {
			list = (List<Diversificado>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Diversificado diversificado : list) {
					if (!uuid.equals(diversificado.getUuid()) ||
						(companyId != diversificado.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_DIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(DiversificadoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (diversificado != null) {
			return diversificado;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchDiversificadoException(msg.toString());
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Diversificado> orderByComparator) {

		List<Diversificado> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (diversificado != null) {
			return diversificado;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchDiversificadoException(msg.toString());
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Diversificado> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<Diversificado> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado[] findByUuid_C_PrevAndNext(
			long divId, String uuid, long companyId,
			OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		uuid = Objects.toString(uuid, "");

		Diversificado diversificado = findByPrimaryKey(divId);

		Session session = null;

		try {
			session = openSession();

			Diversificado[] array = new DiversificadoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, diversificado, uuid, companyId, orderByComparator,
				true);

			array[1] = diversificado;

			array[2] = getByUuid_C_PrevAndNext(
				session, diversificado, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Diversificado getByUuid_C_PrevAndNext(
		Session session, Diversificado diversificado, String uuid,
		long companyId, OrderByComparator<Diversificado> orderByComparator,
		boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_DIVERSIFICADO_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DiversificadoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						diversificado)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Diversificado> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the diversificados where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (Diversificado diversificado :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(diversificado);
		}
	}

	/**
	 * Returns the number of diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching diversificados
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_DIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"diversificado.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(diversificado.uuid IS NULL OR diversificado.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"diversificado.companyId = ?";

	private FinderPath _finderPathWithPaginationFindByScore;
	private FinderPath _finderPathWithoutPaginationFindByScore;
	private FinderPath _finderPathCountByScore;

	/**
	 * Returns all the diversificados where score = &#63;.
	 *
	 * @param score the score
	 * @return the matching diversificados
	 */
	@Override
	public List<Diversificado> findByScore(int score) {
		return findByScore(score, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByScore(int score, int start, int end) {
		return findByScore(score, start, end, null);
	}

	/**
	 * Returns an ordered range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByScore(
		int score, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return findByScore(score, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	@Override
	public List<Diversificado> findByScore(
		int score, int start, int end,
		OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByScore;
			finderArgs = new Object[] {score};
		}
		else {
			finderPath = _finderPathWithPaginationFindByScore;
			finderArgs = new Object[] {score, start, end, orderByComparator};
		}

		List<Diversificado> list = null;

		if (retrieveFromCache) {
			list = (List<Diversificado>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Diversificado diversificado : list) {
					if ((score != diversificado.getScore())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_DIVERSIFICADO_WHERE);

			query.append(_FINDER_COLUMN_SCORE_SCORE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(DiversificadoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(score);

				if (!pagination) {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado findByScore_First(
			int score, OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByScore_First(
			score, orderByComparator);

		if (diversificado != null) {
			return diversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("score=");
		msg.append(score);

		msg.append("}");

		throw new NoSuchDiversificadoException(msg.toString());
	}

	/**
	 * Returns the first diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByScore_First(
		int score, OrderByComparator<Diversificado> orderByComparator) {

		List<Diversificado> list = findByScore(score, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado findByScore_Last(
			int score, OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByScore_Last(
			score, orderByComparator);

		if (diversificado != null) {
			return diversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("score=");
		msg.append(score);

		msg.append("}");

		throw new NoSuchDiversificadoException(msg.toString());
	}

	/**
	 * Returns the last diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchByScore_Last(
		int score, OrderByComparator<Diversificado> orderByComparator) {

		int count = countByScore(score);

		if (count == 0) {
			return null;
		}

		List<Diversificado> list = findByScore(
			score, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where score = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado[] findByScore_PrevAndNext(
			long divId, int score,
			OrderByComparator<Diversificado> orderByComparator)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = findByPrimaryKey(divId);

		Session session = null;

		try {
			session = openSession();

			Diversificado[] array = new DiversificadoImpl[3];

			array[0] = getByScore_PrevAndNext(
				session, diversificado, score, orderByComparator, true);

			array[1] = diversificado;

			array[2] = getByScore_PrevAndNext(
				session, diversificado, score, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Diversificado getByScore_PrevAndNext(
		Session session, Diversificado diversificado, int score,
		OrderByComparator<Diversificado> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_DIVERSIFICADO_WHERE);

		query.append(_FINDER_COLUMN_SCORE_SCORE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(DiversificadoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(score);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						diversificado)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<Diversificado> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the diversificados where score = &#63; from the database.
	 *
	 * @param score the score
	 */
	@Override
	public void removeByScore(int score) {
		for (Diversificado diversificado :
				findByScore(
					score, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(diversificado);
		}
	}

	/**
	 * Returns the number of diversificados where score = &#63;.
	 *
	 * @param score the score
	 * @return the number of matching diversificados
	 */
	@Override
	public int countByScore(int score) {
		FinderPath finderPath = _finderPathCountByScore;

		Object[] finderArgs = new Object[] {score};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_DIVERSIFICADO_WHERE);

			query.append(_FINDER_COLUMN_SCORE_SCORE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(score);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCORE_SCORE_2 =
		"diversificado.score = ?";

	public DiversificadoPersistenceImpl() {
		setModelClass(Diversificado.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
				"_dbColumnNames");

			field.setAccessible(true);

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the diversificado in the entity cache if it is enabled.
	 *
	 * @param diversificado the diversificado
	 */
	@Override
	public void cacheResult(Diversificado diversificado) {
		entityCache.putResult(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoImpl.class, diversificado.getPrimaryKey(),
			diversificado);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {diversificado.getUuid(), diversificado.getGroupId()},
			diversificado);

		diversificado.resetOriginalValues();
	}

	/**
	 * Caches the diversificados in the entity cache if it is enabled.
	 *
	 * @param diversificados the diversificados
	 */
	@Override
	public void cacheResult(List<Diversificado> diversificados) {
		for (Diversificado diversificado : diversificados) {
			if (entityCache.getResult(
					DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
					DiversificadoImpl.class, diversificado.getPrimaryKey()) ==
						null) {

				cacheResult(diversificado);
			}
			else {
				diversificado.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all diversificados.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(DiversificadoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the diversificado.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Diversificado diversificado) {
		entityCache.removeResult(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoImpl.class, diversificado.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((DiversificadoModelImpl)diversificado, true);
	}

	@Override
	public void clearCache(List<Diversificado> diversificados) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Diversificado diversificado : diversificados) {
			entityCache.removeResult(
				DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
				DiversificadoImpl.class, diversificado.getPrimaryKey());

			clearUniqueFindersCache(
				(DiversificadoModelImpl)diversificado, true);
		}
	}

	protected void cacheUniqueFindersCache(
		DiversificadoModelImpl diversificadoModelImpl) {

		Object[] args = new Object[] {
			diversificadoModelImpl.getUuid(),
			diversificadoModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, diversificadoModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		DiversificadoModelImpl diversificadoModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				diversificadoModelImpl.getUuid(),
				diversificadoModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if ((diversificadoModelImpl.getColumnBitmask() &
			 _finderPathFetchByUUID_G.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				diversificadoModelImpl.getOriginalUuid(),
				diversificadoModelImpl.getOriginalGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}
	}

	/**
	 * Creates a new diversificado with the primary key. Does not add the diversificado to the database.
	 *
	 * @param divId the primary key for the new diversificado
	 * @return the new diversificado
	 */
	@Override
	public Diversificado create(long divId) {
		Diversificado diversificado = new DiversificadoImpl();

		diversificado.setNew(true);
		diversificado.setPrimaryKey(divId);

		String uuid = PortalUUIDUtil.generate();

		diversificado.setUuid(uuid);

		diversificado.setCompanyId(companyProvider.getCompanyId());

		return diversificado;
	}

	/**
	 * Removes the diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado that was removed
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado remove(long divId)
		throws NoSuchDiversificadoException {

		return remove((Serializable)divId);
	}

	/**
	 * Removes the diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the diversificado
	 * @return the diversificado that was removed
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado remove(Serializable primaryKey)
		throws NoSuchDiversificadoException {

		Session session = null;

		try {
			session = openSession();

			Diversificado diversificado = (Diversificado)session.get(
				DiversificadoImpl.class, primaryKey);

			if (diversificado == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchDiversificadoException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(diversificado);
		}
		catch (NoSuchDiversificadoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Diversificado removeImpl(Diversificado diversificado) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(diversificado)) {
				diversificado = (Diversificado)session.get(
					DiversificadoImpl.class, diversificado.getPrimaryKeyObj());
			}

			if (diversificado != null) {
				session.delete(diversificado);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (diversificado != null) {
			clearCache(diversificado);
		}

		return diversificado;
	}

	@Override
	public Diversificado updateImpl(Diversificado diversificado) {
		boolean isNew = diversificado.isNew();

		if (!(diversificado instanceof DiversificadoModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(diversificado.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					diversificado);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in diversificado proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom Diversificado implementation " +
					diversificado.getClass());
		}

		DiversificadoModelImpl diversificadoModelImpl =
			(DiversificadoModelImpl)diversificado;

		if (Validator.isNull(diversificado.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			diversificado.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (diversificado.getCreateDate() == null)) {
			if (serviceContext == null) {
				diversificado.setCreateDate(now);
			}
			else {
				diversificado.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!diversificadoModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				diversificado.setModifiedDate(now);
			}
			else {
				diversificado.setModifiedDate(
					serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (diversificado.isNew()) {
				session.save(diversificado);

				diversificado.setNew(false);
			}
			else {
				diversificado = (Diversificado)session.merge(diversificado);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!DiversificadoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {diversificadoModelImpl.getUuid()};

			finderCache.removeResult(_finderPathCountByUuid, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid, args);

			args = new Object[] {
				diversificadoModelImpl.getUuid(),
				diversificadoModelImpl.getCompanyId()
			};

			finderCache.removeResult(_finderPathCountByUuid_C, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid_C, args);

			args = new Object[] {diversificadoModelImpl.getScore()};

			finderCache.removeResult(_finderPathCountByScore, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByScore, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((diversificadoModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					diversificadoModelImpl.getOriginalUuid()
				};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);

				args = new Object[] {diversificadoModelImpl.getUuid()};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);
			}

			if ((diversificadoModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid_C.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					diversificadoModelImpl.getOriginalUuid(),
					diversificadoModelImpl.getOriginalCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);

				args = new Object[] {
					diversificadoModelImpl.getUuid(),
					diversificadoModelImpl.getCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);
			}

			if ((diversificadoModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByScore.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					diversificadoModelImpl.getOriginalScore()
				};

				finderCache.removeResult(_finderPathCountByScore, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByScore, args);

				args = new Object[] {diversificadoModelImpl.getScore()};

				finderCache.removeResult(_finderPathCountByScore, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByScore, args);
			}
		}

		entityCache.putResult(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoImpl.class, diversificado.getPrimaryKey(),
			diversificado, false);

		clearUniqueFindersCache(diversificadoModelImpl, false);
		cacheUniqueFindersCache(diversificadoModelImpl);

		diversificado.resetOriginalValues();

		return diversificado;
	}

	/**
	 * Returns the diversificado with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the diversificado
	 * @return the diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado findByPrimaryKey(Serializable primaryKey)
		throws NoSuchDiversificadoException {

		Diversificado diversificado = fetchByPrimaryKey(primaryKey);

		if (diversificado == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchDiversificadoException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return diversificado;
	}

	/**
	 * Returns the diversificado with the primary key or throws a <code>NoSuchDiversificadoException</code> if it could not be found.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado findByPrimaryKey(long divId)
		throws NoSuchDiversificadoException {

		return findByPrimaryKey((Serializable)divId);
	}

	/**
	 * Returns the diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the diversificado
	 * @return the diversificado, or <code>null</code> if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Diversificado diversificado = (Diversificado)serializable;

		if (diversificado == null) {
			Session session = null;

			try {
				session = openSession();

				diversificado = (Diversificado)session.get(
					DiversificadoImpl.class, primaryKey);

				if (diversificado != null) {
					cacheResult(diversificado);
				}
				else {
					entityCache.putResult(
						DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
						DiversificadoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(
					DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
					DiversificadoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return diversificado;
	}

	/**
	 * Returns the diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado, or <code>null</code> if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado fetchByPrimaryKey(long divId) {
		return fetchByPrimaryKey((Serializable)divId);
	}

	@Override
	public Map<Serializable, Diversificado> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Diversificado> map =
			new HashMap<Serializable, Diversificado>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Diversificado diversificado = fetchByPrimaryKey(primaryKey);

			if (diversificado != null) {
				map.put(primaryKey, diversificado);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
				DiversificadoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Diversificado)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		query.append(_SQL_SELECT_DIVERSIFICADO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Diversificado diversificado : (List<Diversificado>)q.list()) {
				map.put(diversificado.getPrimaryKeyObj(), diversificado);

				cacheResult(diversificado);

				uncachedPrimaryKeys.remove(diversificado.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
					DiversificadoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the diversificados.
	 *
	 * @return the diversificados
	 */
	@Override
	public List<Diversificado> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of diversificados
	 */
	@Override
	public List<Diversificado> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of diversificados
	 */
	@Override
	public List<Diversificado> findAll(
		int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of diversificados
	 */
	@Override
	public List<Diversificado> findAll(
		int start, int end, OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<Diversificado> list = null;

		if (retrieveFromCache) {
			list = (List<Diversificado>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_DIVERSIFICADO);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_DIVERSIFICADO;

				if (pagination) {
					sql = sql.concat(DiversificadoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Diversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the diversificados from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Diversificado diversificado : findAll()) {
			remove(diversificado);
		}
	}

	/**
	 * Returns the number of diversificados.
	 *
	 * @return the number of diversificados
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_DIVERSIFICADO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return DiversificadoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the diversificado persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findAll", new String[0]);

		_finderPathCountAll = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid", new String[] {String.class.getName()},
			DiversificadoModelImpl.UUID_COLUMN_BITMASK |
			DiversificadoModelImpl.NOMBRE_COLUMN_BITMASK);

		_finderPathCountByUuid = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()});

		_finderPathFetchByUUID_G = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			DiversificadoModelImpl.UUID_COLUMN_BITMASK |
			DiversificadoModelImpl.GROUPID_COLUMN_BITMASK);

		_finderPathCountByUUID_G = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			DiversificadoModelImpl.UUID_COLUMN_BITMASK |
			DiversificadoModelImpl.COMPANYID_COLUMN_BITMASK |
			DiversificadoModelImpl.NOMBRE_COLUMN_BITMASK);

		_finderPathCountByUuid_C = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByScore = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByScore",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByScore = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED,
			DiversificadoImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByScore", new String[] {Integer.class.getName()},
			DiversificadoModelImpl.SCORE_COLUMN_BITMASK |
			DiversificadoModelImpl.NOMBRE_COLUMN_BITMASK);

		_finderPathCountByScore = new FinderPath(
			DiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			DiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScore",
			new String[] {Integer.class.getName()});
	}

	public void destroy() {
		entityCache.removeCache(DiversificadoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_DIVERSIFICADO =
		"SELECT diversificado FROM Diversificado diversificado";

	private static final String _SQL_SELECT_DIVERSIFICADO_WHERE_PKS_IN =
		"SELECT diversificado FROM Diversificado diversificado WHERE divId IN (";

	private static final String _SQL_SELECT_DIVERSIFICADO_WHERE =
		"SELECT diversificado FROM Diversificado diversificado WHERE ";

	private static final String _SQL_COUNT_DIVERSIFICADO =
		"SELECT COUNT(diversificado) FROM Diversificado diversificado";

	private static final String _SQL_COUNT_DIVERSIFICADO_WHERE =
		"SELECT COUNT(diversificado) FROM Diversificado diversificado WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "diversificado.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No Diversificado exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No Diversificado exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		DiversificadoPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid"});

}