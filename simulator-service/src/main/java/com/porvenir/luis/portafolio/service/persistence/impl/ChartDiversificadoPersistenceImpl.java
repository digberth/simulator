/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.porvenir.luis.portafolio.exception.NoSuchChartDiversificadoException;
import com.porvenir.luis.portafolio.model.ChartDiversificado;
import com.porvenir.luis.portafolio.model.impl.ChartDiversificadoImpl;
import com.porvenir.luis.portafolio.model.impl.ChartDiversificadoModelImpl;
import com.porvenir.luis.portafolio.service.persistence.ChartDiversificadoPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the chart diversificado service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ChartDiversificadoPersistenceImpl
	extends BasePersistenceImpl<ChartDiversificado>
	implements ChartDiversificadoPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>ChartDiversificadoUtil</code> to access the chart diversificado persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		ChartDiversificadoImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the chart diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid(
		String uuid, int start, int end) {

		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid;
			finderArgs = new Object[] {uuid};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<ChartDiversificado> list = null;

		if (retrieveFromCache) {
			list = (List<ChartDiversificado>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChartDiversificado chartDiversificado : list) {
					if (!uuid.equals(chartDiversificado.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(ChartDiversificadoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByUuid_First(
			String uuid,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByUuid_First(
			uuid, orderByComparator);

		if (chartDiversificado != null) {
			return chartDiversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchChartDiversificadoException(msg.toString());
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByUuid_First(
		String uuid, OrderByComparator<ChartDiversificado> orderByComparator) {

		List<ChartDiversificado> list = findByUuid(
			uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByUuid_Last(
			String uuid,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByUuid_Last(
			uuid, orderByComparator);

		if (chartDiversificado != null) {
			return chartDiversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchChartDiversificadoException(msg.toString());
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByUuid_Last(
		String uuid, OrderByComparator<ChartDiversificado> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ChartDiversificado> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado[] findByUuid_PrevAndNext(
			long id, String uuid,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		uuid = Objects.toString(uuid, "");

		ChartDiversificado chartDiversificado = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			ChartDiversificado[] array = new ChartDiversificadoImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, chartDiversificado, uuid, orderByComparator, true);

			array[1] = chartDiversificado;

			array[2] = getByUuid_PrevAndNext(
				session, chartDiversificado, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChartDiversificado getByUuid_PrevAndNext(
		Session session, ChartDiversificado chartDiversificado, String uuid,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ChartDiversificadoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						chartDiversificado)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<ChartDiversificado> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the chart diversificados where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ChartDiversificado chartDiversificado :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(chartDiversificado);
		}
	}

	/**
	 * Returns the number of chart diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching chart diversificados
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CHARTDIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"chartDiversificado.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(chartDiversificado.uuid IS NULL OR chartDiversificado.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByUUID_G(String uuid, long groupId)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByUUID_G(uuid, groupId);

		if (chartDiversificado == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchChartDiversificadoException(msg.toString());
		}

		return chartDiversificado;
	}

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = new Object[] {uuid, groupId};

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof ChartDiversificado) {
			ChartDiversificado chartDiversificado = (ChartDiversificado)result;

			if (!Objects.equals(uuid, chartDiversificado.getUuid()) ||
				(groupId != chartDiversificado.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<ChartDiversificado> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByUUID_G, finderArgs, list);
				}
				else {
					ChartDiversificado chartDiversificado = list.get(0);

					result = chartDiversificado;

					cacheResult(chartDiversificado);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByUUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ChartDiversificado)result;
		}
	}

	/**
	 * Removes the chart diversificado where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the chart diversificado that was removed
	 */
	@Override
	public ChartDiversificado removeByUUID_G(String uuid, long groupId)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = findByUUID_G(uuid, groupId);

		return remove(chartDiversificado);
	}

	/**
	 * Returns the number of chart diversificados where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching chart diversificados
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CHARTDIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"chartDiversificado.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(chartDiversificado.uuid IS NULL OR chartDiversificado.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"chartDiversificado.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid_C;
			finderArgs = new Object[] {uuid, companyId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<ChartDiversificado> list = null;

		if (retrieveFromCache) {
			list = (List<ChartDiversificado>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChartDiversificado chartDiversificado : list) {
					if (!uuid.equals(chartDiversificado.getUuid()) ||
						(companyId != chartDiversificado.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(ChartDiversificadoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (chartDiversificado != null) {
			return chartDiversificado;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchChartDiversificadoException(msg.toString());
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		List<ChartDiversificado> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (chartDiversificado != null) {
			return chartDiversificado;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchChartDiversificadoException(msg.toString());
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ChartDiversificado> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado[] findByUuid_C_PrevAndNext(
			long id, String uuid, long companyId,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		uuid = Objects.toString(uuid, "");

		ChartDiversificado chartDiversificado = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			ChartDiversificado[] array = new ChartDiversificadoImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, chartDiversificado, uuid, companyId, orderByComparator,
				true);

			array[1] = chartDiversificado;

			array[2] = getByUuid_C_PrevAndNext(
				session, chartDiversificado, uuid, companyId, orderByComparator,
				false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChartDiversificado getByUuid_C_PrevAndNext(
		Session session, ChartDiversificado chartDiversificado, String uuid,
		long companyId, OrderByComparator<ChartDiversificado> orderByComparator,
		boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ChartDiversificadoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						chartDiversificado)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<ChartDiversificado> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the chart diversificados where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ChartDiversificado chartDiversificado :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(chartDiversificado);
		}
	}

	/**
	 * Returns the number of chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching chart diversificados
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CHARTDIVERSIFICADO_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"chartDiversificado.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(chartDiversificado.uuid IS NULL OR chartDiversificado.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"chartDiversificado.companyId = ?";

	private FinderPath _finderPathFetchByNombre_Tiempo;
	private FinderPath _finderPathCountByNombre_Tiempo;

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByNombre_Tiempo(String nombre, int tiempo)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByNombre_Tiempo(
			nombre, tiempo);

		if (chartDiversificado == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("nombre=");
			msg.append(nombre);

			msg.append(", tiempo=");
			msg.append(tiempo);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchChartDiversificadoException(msg.toString());
		}

		return chartDiversificado;
	}

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByNombre_Tiempo(String nombre, int tiempo) {
		return fetchByNombre_Tiempo(nombre, tiempo, true);
	}

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByNombre_Tiempo(
		String nombre, int tiempo, boolean retrieveFromCache) {

		nombre = Objects.toString(nombre, "");

		Object[] finderArgs = new Object[] {nombre, tiempo};

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(
				_finderPathFetchByNombre_Tiempo, finderArgs, this);
		}

		if (result instanceof ChartDiversificado) {
			ChartDiversificado chartDiversificado = (ChartDiversificado)result;

			if (!Objects.equals(nombre, chartDiversificado.getNombre()) ||
				(tiempo != chartDiversificado.getTiempo())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

			boolean bindNombre = false;

			if (nombre.isEmpty()) {
				query.append(_FINDER_COLUMN_NOMBRE_TIEMPO_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_TIEMPO_NOMBRE_2);
			}

			query.append(_FINDER_COLUMN_NOMBRE_TIEMPO_TIEMPO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(StringUtil.toLowerCase(nombre));
				}

				qPos.add(tiempo);

				List<ChartDiversificado> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByNombre_Tiempo, finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ChartDiversificadoPersistenceImpl.fetchByNombre_Tiempo(String, int, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ChartDiversificado chartDiversificado = list.get(0);

					result = chartDiversificado;

					cacheResult(chartDiversificado);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathFetchByNombre_Tiempo, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ChartDiversificado)result;
		}
	}

	/**
	 * Removes the chart diversificado where nombre = &#63; and tiempo = &#63; from the database.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the chart diversificado that was removed
	 */
	@Override
	public ChartDiversificado removeByNombre_Tiempo(String nombre, int tiempo)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = findByNombre_Tiempo(
			nombre, tiempo);

		return remove(chartDiversificado);
	}

	/**
	 * Returns the number of chart diversificados where nombre = &#63; and tiempo = &#63;.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the number of matching chart diversificados
	 */
	@Override
	public int countByNombre_Tiempo(String nombre, int tiempo) {
		nombre = Objects.toString(nombre, "");

		FinderPath finderPath = _finderPathCountByNombre_Tiempo;

		Object[] finderArgs = new Object[] {nombre, tiempo};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CHARTDIVERSIFICADO_WHERE);

			boolean bindNombre = false;

			if (nombre.isEmpty()) {
				query.append(_FINDER_COLUMN_NOMBRE_TIEMPO_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_TIEMPO_NOMBRE_2);
			}

			query.append(_FINDER_COLUMN_NOMBRE_TIEMPO_TIEMPO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(StringUtil.toLowerCase(nombre));
				}

				qPos.add(tiempo);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NOMBRE_TIEMPO_NOMBRE_2 =
		"lower(chartDiversificado.nombre) = ? AND ";

	private static final String _FINDER_COLUMN_NOMBRE_TIEMPO_NOMBRE_3 =
		"(chartDiversificado.nombre IS NULL OR chartDiversificado.nombre = '') AND ";

	private static final String _FINDER_COLUMN_NOMBRE_TIEMPO_TIEMPO_2 =
		"chartDiversificado.tiempo = ?";

	private FinderPath _finderPathWithPaginationFindByNombre;
	private FinderPath _finderPathWithoutPaginationFindByNombre;
	private FinderPath _finderPathCountByNombre;

	/**
	 * Returns all the chart diversificados where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByNombre(String nombre) {
		return findByNombre(nombre, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByNombre(
		String nombre, int start, int end) {

		return findByNombre(nombre, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByNombre(
		String nombre, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return findByNombre(nombre, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findByNombre(
		String nombre, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		nombre = Objects.toString(nombre, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByNombre;
			finderArgs = new Object[] {nombre};
		}
		else {
			finderPath = _finderPathWithPaginationFindByNombre;
			finderArgs = new Object[] {nombre, start, end, orderByComparator};
		}

		List<ChartDiversificado> list = null;

		if (retrieveFromCache) {
			list = (List<ChartDiversificado>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChartDiversificado chartDiversificado : list) {
					if (!nombre.equals(chartDiversificado.getNombre())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

			boolean bindNombre = false;

			if (nombre.isEmpty()) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(ChartDiversificadoModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(StringUtil.toLowerCase(nombre));
				}

				if (!pagination) {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByNombre_First(
			String nombre,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByNombre_First(
			nombre, orderByComparator);

		if (chartDiversificado != null) {
			return chartDiversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append("}");

		throw new NoSuchChartDiversificadoException(msg.toString());
	}

	/**
	 * Returns the first chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByNombre_First(
		String nombre,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		List<ChartDiversificado> list = findByNombre(
			nombre, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado findByNombre_Last(
			String nombre,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByNombre_Last(
			nombre, orderByComparator);

		if (chartDiversificado != null) {
			return chartDiversificado;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("nombre=");
		msg.append(nombre);

		msg.append("}");

		throw new NoSuchChartDiversificadoException(msg.toString());
	}

	/**
	 * Returns the last chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public ChartDiversificado fetchByNombre_Last(
		String nombre,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		int count = countByNombre(nombre);

		if (count == 0) {
			return null;
		}

		List<ChartDiversificado> list = findByNombre(
			nombre, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado[] findByNombre_PrevAndNext(
			long id, String nombre,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws NoSuchChartDiversificadoException {

		nombre = Objects.toString(nombre, "");

		ChartDiversificado chartDiversificado = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			ChartDiversificado[] array = new ChartDiversificadoImpl[3];

			array[0] = getByNombre_PrevAndNext(
				session, chartDiversificado, nombre, orderByComparator, true);

			array[1] = chartDiversificado;

			array[2] = getByNombre_PrevAndNext(
				session, chartDiversificado, nombre, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChartDiversificado getByNombre_PrevAndNext(
		Session session, ChartDiversificado chartDiversificado, String nombre,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE);

		boolean bindNombre = false;

		if (nombre.isEmpty()) {
			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
		}
		else {
			bindNombre = true;

			query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ChartDiversificadoModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindNombre) {
			qPos.add(StringUtil.toLowerCase(nombre));
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(
						chartDiversificado)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<ChartDiversificado> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the chart diversificados where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 */
	@Override
	public void removeByNombre(String nombre) {
		for (ChartDiversificado chartDiversificado :
				findByNombre(
					nombre, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(chartDiversificado);
		}
	}

	/**
	 * Returns the number of chart diversificados where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching chart diversificados
	 */
	@Override
	public int countByNombre(String nombre) {
		nombre = Objects.toString(nombre, "");

		FinderPath finderPath = _finderPathCountByNombre;

		Object[] finderArgs = new Object[] {nombre};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CHARTDIVERSIFICADO_WHERE);

			boolean bindNombre = false;

			if (nombre.isEmpty()) {
				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_3);
			}
			else {
				bindNombre = true;

				query.append(_FINDER_COLUMN_NOMBRE_NOMBRE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindNombre) {
					qPos.add(StringUtil.toLowerCase(nombre));
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_2 =
		"lower(chartDiversificado.nombre) = ?";

	private static final String _FINDER_COLUMN_NOMBRE_NOMBRE_3 =
		"(chartDiversificado.nombre IS NULL OR chartDiversificado.nombre = '')";

	public ChartDiversificadoPersistenceImpl() {
		setModelClass(ChartDiversificado.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("id", "id_");

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
				"_dbColumnNames");

			field.setAccessible(true);

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the chart diversificado in the entity cache if it is enabled.
	 *
	 * @param chartDiversificado the chart diversificado
	 */
	@Override
	public void cacheResult(ChartDiversificado chartDiversificado) {
		entityCache.putResult(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoImpl.class, chartDiversificado.getPrimaryKey(),
			chartDiversificado);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {
				chartDiversificado.getUuid(), chartDiversificado.getGroupId()
			},
			chartDiversificado);

		finderCache.putResult(
			_finderPathFetchByNombre_Tiempo,
			new Object[] {
				chartDiversificado.getNombre(), chartDiversificado.getTiempo()
			},
			chartDiversificado);

		chartDiversificado.resetOriginalValues();
	}

	/**
	 * Caches the chart diversificados in the entity cache if it is enabled.
	 *
	 * @param chartDiversificados the chart diversificados
	 */
	@Override
	public void cacheResult(List<ChartDiversificado> chartDiversificados) {
		for (ChartDiversificado chartDiversificado : chartDiversificados) {
			if (entityCache.getResult(
					ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
					ChartDiversificadoImpl.class,
					chartDiversificado.getPrimaryKey()) == null) {

				cacheResult(chartDiversificado);
			}
			else {
				chartDiversificado.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all chart diversificados.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ChartDiversificadoImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the chart diversificado.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ChartDiversificado chartDiversificado) {
		entityCache.removeResult(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoImpl.class, chartDiversificado.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache(
			(ChartDiversificadoModelImpl)chartDiversificado, true);
	}

	@Override
	public void clearCache(List<ChartDiversificado> chartDiversificados) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ChartDiversificado chartDiversificado : chartDiversificados) {
			entityCache.removeResult(
				ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
				ChartDiversificadoImpl.class,
				chartDiversificado.getPrimaryKey());

			clearUniqueFindersCache(
				(ChartDiversificadoModelImpl)chartDiversificado, true);
		}
	}

	protected void cacheUniqueFindersCache(
		ChartDiversificadoModelImpl chartDiversificadoModelImpl) {

		Object[] args = new Object[] {
			chartDiversificadoModelImpl.getUuid(),
			chartDiversificadoModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, chartDiversificadoModelImpl, false);

		args = new Object[] {
			chartDiversificadoModelImpl.getNombre(),
			chartDiversificadoModelImpl.getTiempo()
		};

		finderCache.putResult(
			_finderPathCountByNombre_Tiempo, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByNombre_Tiempo, args, chartDiversificadoModelImpl,
			false);
	}

	protected void clearUniqueFindersCache(
		ChartDiversificadoModelImpl chartDiversificadoModelImpl,
		boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				chartDiversificadoModelImpl.getUuid(),
				chartDiversificadoModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if ((chartDiversificadoModelImpl.getColumnBitmask() &
			 _finderPathFetchByUUID_G.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				chartDiversificadoModelImpl.getOriginalUuid(),
				chartDiversificadoModelImpl.getOriginalGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if (clearCurrent) {
			Object[] args = new Object[] {
				chartDiversificadoModelImpl.getNombre(),
				chartDiversificadoModelImpl.getTiempo()
			};

			finderCache.removeResult(_finderPathCountByNombre_Tiempo, args);
			finderCache.removeResult(_finderPathFetchByNombre_Tiempo, args);
		}

		if ((chartDiversificadoModelImpl.getColumnBitmask() &
			 _finderPathFetchByNombre_Tiempo.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				chartDiversificadoModelImpl.getOriginalNombre(),
				chartDiversificadoModelImpl.getOriginalTiempo()
			};

			finderCache.removeResult(_finderPathCountByNombre_Tiempo, args);
			finderCache.removeResult(_finderPathFetchByNombre_Tiempo, args);
		}
	}

	/**
	 * Creates a new chart diversificado with the primary key. Does not add the chart diversificado to the database.
	 *
	 * @param id the primary key for the new chart diversificado
	 * @return the new chart diversificado
	 */
	@Override
	public ChartDiversificado create(long id) {
		ChartDiversificado chartDiversificado = new ChartDiversificadoImpl();

		chartDiversificado.setNew(true);
		chartDiversificado.setPrimaryKey(id);

		String uuid = PortalUUIDUtil.generate();

		chartDiversificado.setUuid(uuid);

		chartDiversificado.setCompanyId(companyProvider.getCompanyId());

		return chartDiversificado;
	}

	/**
	 * Removes the chart diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado that was removed
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado remove(long id)
		throws NoSuchChartDiversificadoException {

		return remove((Serializable)id);
	}

	/**
	 * Removes the chart diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the chart diversificado
	 * @return the chart diversificado that was removed
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado remove(Serializable primaryKey)
		throws NoSuchChartDiversificadoException {

		Session session = null;

		try {
			session = openSession();

			ChartDiversificado chartDiversificado =
				(ChartDiversificado)session.get(
					ChartDiversificadoImpl.class, primaryKey);

			if (chartDiversificado == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchChartDiversificadoException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(chartDiversificado);
		}
		catch (NoSuchChartDiversificadoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ChartDiversificado removeImpl(
		ChartDiversificado chartDiversificado) {

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(chartDiversificado)) {
				chartDiversificado = (ChartDiversificado)session.get(
					ChartDiversificadoImpl.class,
					chartDiversificado.getPrimaryKeyObj());
			}

			if (chartDiversificado != null) {
				session.delete(chartDiversificado);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (chartDiversificado != null) {
			clearCache(chartDiversificado);
		}

		return chartDiversificado;
	}

	@Override
	public ChartDiversificado updateImpl(
		ChartDiversificado chartDiversificado) {

		boolean isNew = chartDiversificado.isNew();

		if (!(chartDiversificado instanceof ChartDiversificadoModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(chartDiversificado.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(
					chartDiversificado);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in chartDiversificado proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom ChartDiversificado implementation " +
					chartDiversificado.getClass());
		}

		ChartDiversificadoModelImpl chartDiversificadoModelImpl =
			(ChartDiversificadoModelImpl)chartDiversificado;

		if (Validator.isNull(chartDiversificado.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			chartDiversificado.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (chartDiversificado.getCreateDate() == null)) {
			if (serviceContext == null) {
				chartDiversificado.setCreateDate(now);
			}
			else {
				chartDiversificado.setCreateDate(
					serviceContext.getCreateDate(now));
			}
		}

		if (!chartDiversificadoModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				chartDiversificado.setModifiedDate(now);
			}
			else {
				chartDiversificado.setModifiedDate(
					serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (chartDiversificado.isNew()) {
				session.save(chartDiversificado);

				chartDiversificado.setNew(false);
			}
			else {
				chartDiversificado = (ChartDiversificado)session.merge(
					chartDiversificado);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ChartDiversificadoModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {
				chartDiversificadoModelImpl.getUuid()
			};

			finderCache.removeResult(_finderPathCountByUuid, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid, args);

			args = new Object[] {
				chartDiversificadoModelImpl.getUuid(),
				chartDiversificadoModelImpl.getCompanyId()
			};

			finderCache.removeResult(_finderPathCountByUuid_C, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid_C, args);

			args = new Object[] {chartDiversificadoModelImpl.getNombre()};

			finderCache.removeResult(_finderPathCountByNombre, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByNombre, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((chartDiversificadoModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					chartDiversificadoModelImpl.getOriginalUuid()
				};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);

				args = new Object[] {chartDiversificadoModelImpl.getUuid()};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);
			}

			if ((chartDiversificadoModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid_C.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					chartDiversificadoModelImpl.getOriginalUuid(),
					chartDiversificadoModelImpl.getOriginalCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);

				args = new Object[] {
					chartDiversificadoModelImpl.getUuid(),
					chartDiversificadoModelImpl.getCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);
			}

			if ((chartDiversificadoModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByNombre.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					chartDiversificadoModelImpl.getOriginalNombre()
				};

				finderCache.removeResult(_finderPathCountByNombre, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByNombre, args);

				args = new Object[] {chartDiversificadoModelImpl.getNombre()};

				finderCache.removeResult(_finderPathCountByNombre, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByNombre, args);
			}
		}

		entityCache.putResult(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoImpl.class, chartDiversificado.getPrimaryKey(),
			chartDiversificado, false);

		clearUniqueFindersCache(chartDiversificadoModelImpl, false);
		cacheUniqueFindersCache(chartDiversificadoModelImpl);

		chartDiversificado.resetOriginalValues();

		return chartDiversificado;
	}

	/**
	 * Returns the chart diversificado with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the chart diversificado
	 * @return the chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado findByPrimaryKey(Serializable primaryKey)
		throws NoSuchChartDiversificadoException {

		ChartDiversificado chartDiversificado = fetchByPrimaryKey(primaryKey);

		if (chartDiversificado == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchChartDiversificadoException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return chartDiversificado;
	}

	/**
	 * Returns the chart diversificado with the primary key or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado findByPrimaryKey(long id)
		throws NoSuchChartDiversificadoException {

		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the chart diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the chart diversificado
	 * @return the chart diversificado, or <code>null</code> if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ChartDiversificado chartDiversificado =
			(ChartDiversificado)serializable;

		if (chartDiversificado == null) {
			Session session = null;

			try {
				session = openSession();

				chartDiversificado = (ChartDiversificado)session.get(
					ChartDiversificadoImpl.class, primaryKey);

				if (chartDiversificado != null) {
					cacheResult(chartDiversificado);
				}
				else {
					entityCache.putResult(
						ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
						ChartDiversificadoImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(
					ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
					ChartDiversificadoImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return chartDiversificado;
	}

	/**
	 * Returns the chart diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado, or <code>null</code> if a chart diversificado with the primary key could not be found
	 */
	@Override
	public ChartDiversificado fetchByPrimaryKey(long id) {
		return fetchByPrimaryKey((Serializable)id);
	}

	@Override
	public Map<Serializable, ChartDiversificado> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ChartDiversificado> map =
			new HashMap<Serializable, ChartDiversificado>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ChartDiversificado chartDiversificado = fetchByPrimaryKey(
				primaryKey);

			if (chartDiversificado != null) {
				map.put(primaryKey, chartDiversificado);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
				ChartDiversificadoImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ChartDiversificado)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		query.append(_SQL_SELECT_CHARTDIVERSIFICADO_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ChartDiversificado chartDiversificado :
					(List<ChartDiversificado>)q.list()) {

				map.put(
					chartDiversificado.getPrimaryKeyObj(), chartDiversificado);

				cacheResult(chartDiversificado);

				uncachedPrimaryKeys.remove(
					chartDiversificado.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
					ChartDiversificadoImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the chart diversificados.
	 *
	 * @return the chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findAll(
		int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of chart diversificados
	 */
	@Override
	public List<ChartDiversificado> findAll(
		int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<ChartDiversificado> list = null;

		if (retrieveFromCache) {
			list = (List<ChartDiversificado>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CHARTDIVERSIFICADO);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CHARTDIVERSIFICADO;

				if (pagination) {
					sql = sql.concat(ChartDiversificadoModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartDiversificado>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the chart diversificados from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ChartDiversificado chartDiversificado : findAll()) {
			remove(chartDiversificado);
		}
	}

	/**
	 * Returns the number of chart diversificados.
	 *
	 * @return the number of chart diversificados
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CHARTDIVERSIFICADO);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ChartDiversificadoModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the chart diversificado persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()},
			ChartDiversificadoModelImpl.UUID_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.NOMBRE_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByUuid = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()});

		_finderPathFetchByUUID_G = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			ChartDiversificadoModelImpl.UUID_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.GROUPID_COLUMN_BITMASK);

		_finderPathCountByUUID_G = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			ChartDiversificadoModelImpl.UUID_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.COMPANYID_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.NOMBRE_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByUuid_C = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathFetchByNombre_Tiempo = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class, FINDER_CLASS_NAME_ENTITY,
			"fetchByNombre_Tiempo",
			new String[] {String.class.getName(), Integer.class.getName()},
			ChartDiversificadoModelImpl.NOMBRE_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByNombre_Tiempo = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNombre_Tiempo",
			new String[] {String.class.getName(), Integer.class.getName()});

		_finderPathWithPaginationFindByNombre = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByNombre",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByNombre = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED,
			ChartDiversificadoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByNombre",
			new String[] {String.class.getName()},
			ChartDiversificadoModelImpl.NOMBRE_COLUMN_BITMASK |
			ChartDiversificadoModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByNombre = new FinderPath(
			ChartDiversificadoModelImpl.ENTITY_CACHE_ENABLED,
			ChartDiversificadoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByNombre",
			new String[] {String.class.getName()});
	}

	public void destroy() {
		entityCache.removeCache(ChartDiversificadoImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_CHARTDIVERSIFICADO =
		"SELECT chartDiversificado FROM ChartDiversificado chartDiversificado";

	private static final String _SQL_SELECT_CHARTDIVERSIFICADO_WHERE_PKS_IN =
		"SELECT chartDiversificado FROM ChartDiversificado chartDiversificado WHERE id_ IN (";

	private static final String _SQL_SELECT_CHARTDIVERSIFICADO_WHERE =
		"SELECT chartDiversificado FROM ChartDiversificado chartDiversificado WHERE ";

	private static final String _SQL_COUNT_CHARTDIVERSIFICADO =
		"SELECT COUNT(chartDiversificado) FROM ChartDiversificado chartDiversificado";

	private static final String _SQL_COUNT_CHARTDIVERSIFICADO_WHERE =
		"SELECT COUNT(chartDiversificado) FROM ChartDiversificado chartDiversificado WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "chartDiversificado.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No ChartDiversificado exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No ChartDiversificado exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		ChartDiversificadoPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "id"});

}