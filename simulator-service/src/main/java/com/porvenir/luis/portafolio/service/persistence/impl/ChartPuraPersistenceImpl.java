/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextThreadLocal;
import com.liferay.portal.kernel.service.persistence.CompanyProvider;
import com.liferay.portal.kernel.service.persistence.CompanyProviderWrapper;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.uuid.PortalUUIDUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.porvenir.luis.portafolio.exception.NoSuchChartPuraException;
import com.porvenir.luis.portafolio.model.ChartPura;
import com.porvenir.luis.portafolio.model.impl.ChartPuraImpl;
import com.porvenir.luis.portafolio.model.impl.ChartPuraModelImpl;
import com.porvenir.luis.portafolio.service.persistence.ChartPuraPersistence;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the chart pura service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ChartPuraPersistenceImpl
	extends BasePersistenceImpl<ChartPura> implements ChartPuraPersistence {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use <code>ChartPuraUtil</code> to access the chart pura persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY =
		ChartPuraImpl.class.getName();

	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List1";

	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION =
		FINDER_CLASS_NAME_ENTITY + ".List2";

	private FinderPath _finderPathWithPaginationFindAll;
	private FinderPath _finderPathWithoutPaginationFindAll;
	private FinderPath _finderPathCountAll;
	private FinderPath _finderPathWithPaginationFindByUuid;
	private FinderPath _finderPathWithoutPaginationFindByUuid;
	private FinderPath _finderPathCountByUuid;

	/**
	 * Returns all the chart puras where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid(String uuid) {
		return findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid(String uuid, int start, int end) {
		return findByUuid(uuid, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartPura> orderByComparator) {

		return findByUuid(uuid, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid;
			finderArgs = new Object[] {uuid};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid;
			finderArgs = new Object[] {uuid, start, end, orderByComparator};
		}

		List<ChartPura> list = null;

		if (retrieveFromCache) {
			list = (List<ChartPura>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChartPura chartPura : list) {
					if (!uuid.equals(chartPura.getUuid())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CHARTPURA_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(ChartPuraModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				if (!pagination) {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByUuid_First(
			String uuid, OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByUuid_First(uuid, orderByComparator);

		if (chartPura != null) {
			return chartPura;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchChartPuraException(msg.toString());
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByUuid_First(
		String uuid, OrderByComparator<ChartPura> orderByComparator) {

		List<ChartPura> list = findByUuid(uuid, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByUuid_Last(
			String uuid, OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByUuid_Last(uuid, orderByComparator);

		if (chartPura != null) {
			return chartPura;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append("}");

		throw new NoSuchChartPuraException(msg.toString());
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByUuid_Last(
		String uuid, OrderByComparator<ChartPura> orderByComparator) {

		int count = countByUuid(uuid);

		if (count == 0) {
			return null;
		}

		List<ChartPura> list = findByUuid(
			uuid, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura[] findByUuid_PrevAndNext(
			long id, String uuid,
			OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		uuid = Objects.toString(uuid, "");

		ChartPura chartPura = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			ChartPura[] array = new ChartPuraImpl[3];

			array[0] = getByUuid_PrevAndNext(
				session, chartPura, uuid, orderByComparator, true);

			array[1] = chartPura;

			array[2] = getByUuid_PrevAndNext(
				session, chartPura, uuid, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChartPura getByUuid_PrevAndNext(
		Session session, ChartPura chartPura, String uuid,
		OrderByComparator<ChartPura> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CHARTPURA_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_UUID_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ChartPuraModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(chartPura)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<ChartPura> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the chart puras where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	@Override
	public void removeByUuid(String uuid) {
		for (ChartPura chartPura :
				findByUuid(uuid, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(chartPura);
		}
	}

	/**
	 * Returns the number of chart puras where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching chart puras
	 */
	@Override
	public int countByUuid(String uuid) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid;

		Object[] finderArgs = new Object[] {uuid};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CHARTPURA_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_UUID_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_UUID_2 =
		"chartPura.uuid = ?";

	private static final String _FINDER_COLUMN_UUID_UUID_3 =
		"(chartPura.uuid IS NULL OR chartPura.uuid = '')";

	private FinderPath _finderPathFetchByUUID_G;
	private FinderPath _finderPathCountByUUID_G;

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByUUID_G(String uuid, long groupId)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByUUID_G(uuid, groupId);

		if (chartPura == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("uuid=");
			msg.append(uuid);

			msg.append(", groupId=");
			msg.append(groupId);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchChartPuraException(msg.toString());
		}

		return chartPura;
	}

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByUUID_G(String uuid, long groupId) {
		return fetchByUUID_G(uuid, groupId, true);
	}

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		Object[] finderArgs = new Object[] {uuid, groupId};

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(
				_finderPathFetchByUUID_G, finderArgs, this);
		}

		if (result instanceof ChartPura) {
			ChartPura chartPura = (ChartPura)result;

			if (!Objects.equals(uuid, chartPura.getUuid()) ||
				(groupId != chartPura.getGroupId())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CHARTPURA_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				List<ChartPura> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByUUID_G, finderArgs, list);
				}
				else {
					ChartPura chartPura = list.get(0);

					result = chartPura;

					cacheResult(chartPura);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(_finderPathFetchByUUID_G, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ChartPura)result;
		}
	}

	/**
	 * Removes the chart pura where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the chart pura that was removed
	 */
	@Override
	public ChartPura removeByUUID_G(String uuid, long groupId)
		throws NoSuchChartPuraException {

		ChartPura chartPura = findByUUID_G(uuid, groupId);

		return remove(chartPura);
	}

	/**
	 * Returns the number of chart puras where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching chart puras
	 */
	@Override
	public int countByUUID_G(String uuid, long groupId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUUID_G;

		Object[] finderArgs = new Object[] {uuid, groupId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CHARTPURA_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_G_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_G_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_G_GROUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(groupId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_G_UUID_2 =
		"chartPura.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_G_UUID_3 =
		"(chartPura.uuid IS NULL OR chartPura.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_G_GROUPID_2 =
		"chartPura.groupId = ?";

	private FinderPath _finderPathWithPaginationFindByUuid_C;
	private FinderPath _finderPathWithoutPaginationFindByUuid_C;
	private FinderPath _finderPathCountByUuid_C;

	/**
	 * Returns all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid_C(String uuid, long companyId) {
		return findByUuid_C(
			uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return findByUuid_C(uuid, companyId, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartPura> orderByComparator) {

		return findByUuid_C(
			uuid, companyId, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		uuid = Objects.toString(uuid, "");

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByUuid_C;
			finderArgs = new Object[] {uuid, companyId};
		}
		else {
			finderPath = _finderPathWithPaginationFindByUuid_C;
			finderArgs = new Object[] {
				uuid, companyId, start, end, orderByComparator
			};
		}

		List<ChartPura> list = null;

		if (retrieveFromCache) {
			list = (List<ChartPura>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChartPura chartPura : list) {
					if (!uuid.equals(chartPura.getUuid()) ||
						(companyId != chartPura.getCompanyId())) {

						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					4 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(4);
			}

			query.append(_SQL_SELECT_CHARTPURA_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(ChartPuraModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				if (!pagination) {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByUuid_C_First(
			uuid, companyId, orderByComparator);

		if (chartPura != null) {
			return chartPura;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchChartPuraException(msg.toString());
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ChartPura> orderByComparator) {

		List<ChartPura> list = findByUuid_C(
			uuid, companyId, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);

		if (chartPura != null) {
			return chartPura;
		}

		StringBundler msg = new StringBundler(6);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("uuid=");
		msg.append(uuid);

		msg.append(", companyId=");
		msg.append(companyId);

		msg.append("}");

		throw new NoSuchChartPuraException(msg.toString());
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ChartPura> orderByComparator) {

		int count = countByUuid_C(uuid, companyId);

		if (count == 0) {
			return null;
		}

		List<ChartPura> list = findByUuid_C(
			uuid, companyId, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura[] findByUuid_C_PrevAndNext(
			long id, String uuid, long companyId,
			OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		uuid = Objects.toString(uuid, "");

		ChartPura chartPura = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			ChartPura[] array = new ChartPuraImpl[3];

			array[0] = getByUuid_C_PrevAndNext(
				session, chartPura, uuid, companyId, orderByComparator, true);

			array[1] = chartPura;

			array[2] = getByUuid_C_PrevAndNext(
				session, chartPura, uuid, companyId, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChartPura getByUuid_C_PrevAndNext(
		Session session, ChartPura chartPura, String uuid, long companyId,
		OrderByComparator<ChartPura> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				5 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(4);
		}

		query.append(_SQL_SELECT_CHARTPURA_WHERE);

		boolean bindUuid = false;

		if (uuid.isEmpty()) {
			query.append(_FINDER_COLUMN_UUID_C_UUID_3);
		}
		else {
			bindUuid = true;

			query.append(_FINDER_COLUMN_UUID_C_UUID_2);
		}

		query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ChartPuraModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindUuid) {
			qPos.add(uuid);
		}

		qPos.add(companyId);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(chartPura)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<ChartPura> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the chart puras where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	@Override
	public void removeByUuid_C(String uuid, long companyId) {
		for (ChartPura chartPura :
				findByUuid_C(
					uuid, companyId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null)) {

			remove(chartPura);
		}
	}

	/**
	 * Returns the number of chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching chart puras
	 */
	@Override
	public int countByUuid_C(String uuid, long companyId) {
		uuid = Objects.toString(uuid, "");

		FinderPath finderPath = _finderPathCountByUuid_C;

		Object[] finderArgs = new Object[] {uuid, companyId};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CHARTPURA_WHERE);

			boolean bindUuid = false;

			if (uuid.isEmpty()) {
				query.append(_FINDER_COLUMN_UUID_C_UUID_3);
			}
			else {
				bindUuid = true;

				query.append(_FINDER_COLUMN_UUID_C_UUID_2);
			}

			query.append(_FINDER_COLUMN_UUID_C_COMPANYID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindUuid) {
					qPos.add(uuid);
				}

				qPos.add(companyId);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_UUID_C_UUID_2 =
		"chartPura.uuid = ? AND ";

	private static final String _FINDER_COLUMN_UUID_C_UUID_3 =
		"(chartPura.uuid IS NULL OR chartPura.uuid = '') AND ";

	private static final String _FINDER_COLUMN_UUID_C_COMPANYID_2 =
		"chartPura.companyId = ?";

	private FinderPath _finderPathFetchByScore_Tiempo;
	private FinderPath _finderPathCountByScore_Tiempo;

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByScore_Tiempo(int score, int tiempo)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByScore_Tiempo(score, tiempo);

		if (chartPura == null) {
			StringBundler msg = new StringBundler(6);

			msg.append(_NO_SUCH_ENTITY_WITH_KEY);

			msg.append("score=");
			msg.append(score);

			msg.append(", tiempo=");
			msg.append(tiempo);

			msg.append("}");

			if (_log.isDebugEnabled()) {
				_log.debug(msg.toString());
			}

			throw new NoSuchChartPuraException(msg.toString());
		}

		return chartPura;
	}

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByScore_Tiempo(int score, int tiempo) {
		return fetchByScore_Tiempo(score, tiempo, true);
	}

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByScore_Tiempo(
		int score, int tiempo, boolean retrieveFromCache) {

		Object[] finderArgs = new Object[] {score, tiempo};

		Object result = null;

		if (retrieveFromCache) {
			result = finderCache.getResult(
				_finderPathFetchByScore_Tiempo, finderArgs, this);
		}

		if (result instanceof ChartPura) {
			ChartPura chartPura = (ChartPura)result;

			if ((score != chartPura.getScore()) ||
				(tiempo != chartPura.getTiempo())) {

				result = null;
			}
		}

		if (result == null) {
			StringBundler query = new StringBundler(4);

			query.append(_SQL_SELECT_CHARTPURA_WHERE);

			query.append(_FINDER_COLUMN_SCORE_TIEMPO_SCORE_2);

			query.append(_FINDER_COLUMN_SCORE_TIEMPO_TIEMPO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(score);

				qPos.add(tiempo);

				List<ChartPura> list = q.list();

				if (list.isEmpty()) {
					finderCache.putResult(
						_finderPathFetchByScore_Tiempo, finderArgs, list);
				}
				else {
					if (list.size() > 1) {
						Collections.sort(list, Collections.reverseOrder());

						if (_log.isWarnEnabled()) {
							_log.warn(
								"ChartPuraPersistenceImpl.fetchByScore_Tiempo(int, int, boolean) with parameters (" +
									StringUtil.merge(finderArgs) +
										") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
						}
					}

					ChartPura chartPura = list.get(0);

					result = chartPura;

					cacheResult(chartPura);
				}
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathFetchByScore_Tiempo, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		if (result instanceof List<?>) {
			return null;
		}
		else {
			return (ChartPura)result;
		}
	}

	/**
	 * Removes the chart pura where score = &#63; and tiempo = &#63; from the database.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the chart pura that was removed
	 */
	@Override
	public ChartPura removeByScore_Tiempo(int score, int tiempo)
		throws NoSuchChartPuraException {

		ChartPura chartPura = findByScore_Tiempo(score, tiempo);

		return remove(chartPura);
	}

	/**
	 * Returns the number of chart puras where score = &#63; and tiempo = &#63;.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the number of matching chart puras
	 */
	@Override
	public int countByScore_Tiempo(int score, int tiempo) {
		FinderPath finderPath = _finderPathCountByScore_Tiempo;

		Object[] finderArgs = new Object[] {score, tiempo};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(3);

			query.append(_SQL_COUNT_CHARTPURA_WHERE);

			query.append(_FINDER_COLUMN_SCORE_TIEMPO_SCORE_2);

			query.append(_FINDER_COLUMN_SCORE_TIEMPO_TIEMPO_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(score);

				qPos.add(tiempo);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCORE_TIEMPO_SCORE_2 =
		"chartPura.score = ? AND ";

	private static final String _FINDER_COLUMN_SCORE_TIEMPO_TIEMPO_2 =
		"chartPura.tiempo = ?";

	private FinderPath _finderPathWithPaginationFindByScore;
	private FinderPath _finderPathWithoutPaginationFindByScore;
	private FinderPath _finderPathCountByScore;

	/**
	 * Returns all the chart puras where score = &#63;.
	 *
	 * @param score the score
	 * @return the matching chart puras
	 */
	@Override
	public List<ChartPura> findByScore(int score) {
		return findByScore(score, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByScore(int score, int start, int end) {
		return findByScore(score, start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByScore(
		int score, int start, int end,
		OrderByComparator<ChartPura> orderByComparator) {

		return findByScore(score, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	@Override
	public List<ChartPura> findByScore(
		int score, int start, int end,
		OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindByScore;
			finderArgs = new Object[] {score};
		}
		else {
			finderPath = _finderPathWithPaginationFindByScore;
			finderArgs = new Object[] {score, start, end, orderByComparator};
		}

		List<ChartPura> list = null;

		if (retrieveFromCache) {
			list = (List<ChartPura>)finderCache.getResult(
				finderPath, finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (ChartPura chartPura : list) {
					if ((score != chartPura.getScore())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					3 + (orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CHARTPURA_WHERE);

			query.append(_FINDER_COLUMN_SCORE_SCORE_2);

			if (orderByComparator != null) {
				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);
			}
			else if (pagination) {
				query.append(ChartPuraModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(score);

				if (!pagination) {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByScore_First(
			int score, OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByScore_First(score, orderByComparator);

		if (chartPura != null) {
			return chartPura;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("score=");
		msg.append(score);

		msg.append("}");

		throw new NoSuchChartPuraException(msg.toString());
	}

	/**
	 * Returns the first chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByScore_First(
		int score, OrderByComparator<ChartPura> orderByComparator) {

		List<ChartPura> list = findByScore(score, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	@Override
	public ChartPura findByScore_Last(
			int score, OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByScore_Last(score, orderByComparator);

		if (chartPura != null) {
			return chartPura;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("score=");
		msg.append(score);

		msg.append("}");

		throw new NoSuchChartPuraException(msg.toString());
	}

	/**
	 * Returns the last chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public ChartPura fetchByScore_Last(
		int score, OrderByComparator<ChartPura> orderByComparator) {

		int count = countByScore(score);

		if (count == 0) {
			return null;
		}

		List<ChartPura> list = findByScore(
			score, count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where score = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura[] findByScore_PrevAndNext(
			long id, int score, OrderByComparator<ChartPura> orderByComparator)
		throws NoSuchChartPuraException {

		ChartPura chartPura = findByPrimaryKey(id);

		Session session = null;

		try {
			session = openSession();

			ChartPura[] array = new ChartPuraImpl[3];

			array[0] = getByScore_PrevAndNext(
				session, chartPura, score, orderByComparator, true);

			array[1] = chartPura;

			array[2] = getByScore_PrevAndNext(
				session, chartPura, score, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected ChartPura getByScore_PrevAndNext(
		Session session, ChartPura chartPura, int score,
		OrderByComparator<ChartPura> orderByComparator, boolean previous) {

		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(
				4 + (orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CHARTPURA_WHERE);

		query.append(_FINDER_COLUMN_SCORE_SCORE_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields =
				orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(ChartPuraModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(score);

		if (orderByComparator != null) {
			for (Object orderByConditionValue :
					orderByComparator.getOrderByConditionValues(chartPura)) {

				qPos.add(orderByConditionValue);
			}
		}

		List<ChartPura> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the chart puras where score = &#63; from the database.
	 *
	 * @param score the score
	 */
	@Override
	public void removeByScore(int score) {
		for (ChartPura chartPura :
				findByScore(
					score, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {

			remove(chartPura);
		}
	}

	/**
	 * Returns the number of chart puras where score = &#63;.
	 *
	 * @param score the score
	 * @return the number of matching chart puras
	 */
	@Override
	public int countByScore(int score) {
		FinderPath finderPath = _finderPathCountByScore;

		Object[] finderArgs = new Object[] {score};

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CHARTPURA_WHERE);

			query.append(_FINDER_COLUMN_SCORE_SCORE_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(score);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_SCORE_SCORE_2 =
		"chartPura.score = ?";

	public ChartPuraPersistenceImpl() {
		setModelClass(ChartPura.class);

		Map<String, String> dbColumnNames = new HashMap<String, String>();

		dbColumnNames.put("uuid", "uuid_");
		dbColumnNames.put("id", "id_");

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
				"_dbColumnNames");

			field.setAccessible(true);

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the chart pura in the entity cache if it is enabled.
	 *
	 * @param chartPura the chart pura
	 */
	@Override
	public void cacheResult(ChartPura chartPura) {
		entityCache.putResult(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED, ChartPuraImpl.class,
			chartPura.getPrimaryKey(), chartPura);

		finderCache.putResult(
			_finderPathFetchByUUID_G,
			new Object[] {chartPura.getUuid(), chartPura.getGroupId()},
			chartPura);

		finderCache.putResult(
			_finderPathFetchByScore_Tiempo,
			new Object[] {chartPura.getScore(), chartPura.getTiempo()},
			chartPura);

		chartPura.resetOriginalValues();
	}

	/**
	 * Caches the chart puras in the entity cache if it is enabled.
	 *
	 * @param chartPuras the chart puras
	 */
	@Override
	public void cacheResult(List<ChartPura> chartPuras) {
		for (ChartPura chartPura : chartPuras) {
			if (entityCache.getResult(
					ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
					ChartPuraImpl.class, chartPura.getPrimaryKey()) == null) {

				cacheResult(chartPura);
			}
			else {
				chartPura.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all chart puras.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ChartPuraImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the chart pura.
	 *
	 * <p>
	 * The <code>EntityCache</code> and <code>FinderCache</code> are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ChartPura chartPura) {
		entityCache.removeResult(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED, ChartPuraImpl.class,
			chartPura.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		clearUniqueFindersCache((ChartPuraModelImpl)chartPura, true);
	}

	@Override
	public void clearCache(List<ChartPura> chartPuras) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ChartPura chartPura : chartPuras) {
			entityCache.removeResult(
				ChartPuraModelImpl.ENTITY_CACHE_ENABLED, ChartPuraImpl.class,
				chartPura.getPrimaryKey());

			clearUniqueFindersCache((ChartPuraModelImpl)chartPura, true);
		}
	}

	protected void cacheUniqueFindersCache(
		ChartPuraModelImpl chartPuraModelImpl) {

		Object[] args = new Object[] {
			chartPuraModelImpl.getUuid(), chartPuraModelImpl.getGroupId()
		};

		finderCache.putResult(
			_finderPathCountByUUID_G, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByUUID_G, args, chartPuraModelImpl, false);

		args = new Object[] {
			chartPuraModelImpl.getScore(), chartPuraModelImpl.getTiempo()
		};

		finderCache.putResult(
			_finderPathCountByScore_Tiempo, args, Long.valueOf(1), false);
		finderCache.putResult(
			_finderPathFetchByScore_Tiempo, args, chartPuraModelImpl, false);
	}

	protected void clearUniqueFindersCache(
		ChartPuraModelImpl chartPuraModelImpl, boolean clearCurrent) {

		if (clearCurrent) {
			Object[] args = new Object[] {
				chartPuraModelImpl.getUuid(), chartPuraModelImpl.getGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if ((chartPuraModelImpl.getColumnBitmask() &
			 _finderPathFetchByUUID_G.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				chartPuraModelImpl.getOriginalUuid(),
				chartPuraModelImpl.getOriginalGroupId()
			};

			finderCache.removeResult(_finderPathCountByUUID_G, args);
			finderCache.removeResult(_finderPathFetchByUUID_G, args);
		}

		if (clearCurrent) {
			Object[] args = new Object[] {
				chartPuraModelImpl.getScore(), chartPuraModelImpl.getTiempo()
			};

			finderCache.removeResult(_finderPathCountByScore_Tiempo, args);
			finderCache.removeResult(_finderPathFetchByScore_Tiempo, args);
		}

		if ((chartPuraModelImpl.getColumnBitmask() &
			 _finderPathFetchByScore_Tiempo.getColumnBitmask()) != 0) {

			Object[] args = new Object[] {
				chartPuraModelImpl.getOriginalScore(),
				chartPuraModelImpl.getOriginalTiempo()
			};

			finderCache.removeResult(_finderPathCountByScore_Tiempo, args);
			finderCache.removeResult(_finderPathFetchByScore_Tiempo, args);
		}
	}

	/**
	 * Creates a new chart pura with the primary key. Does not add the chart pura to the database.
	 *
	 * @param id the primary key for the new chart pura
	 * @return the new chart pura
	 */
	@Override
	public ChartPura create(long id) {
		ChartPura chartPura = new ChartPuraImpl();

		chartPura.setNew(true);
		chartPura.setPrimaryKey(id);

		String uuid = PortalUUIDUtil.generate();

		chartPura.setUuid(uuid);

		chartPura.setCompanyId(companyProvider.getCompanyId());

		return chartPura;
	}

	/**
	 * Removes the chart pura with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura that was removed
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura remove(long id) throws NoSuchChartPuraException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the chart pura with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the chart pura
	 * @return the chart pura that was removed
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura remove(Serializable primaryKey)
		throws NoSuchChartPuraException {

		Session session = null;

		try {
			session = openSession();

			ChartPura chartPura = (ChartPura)session.get(
				ChartPuraImpl.class, primaryKey);

			if (chartPura == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchChartPuraException(
					_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			return remove(chartPura);
		}
		catch (NoSuchChartPuraException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ChartPura removeImpl(ChartPura chartPura) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(chartPura)) {
				chartPura = (ChartPura)session.get(
					ChartPuraImpl.class, chartPura.getPrimaryKeyObj());
			}

			if (chartPura != null) {
				session.delete(chartPura);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (chartPura != null) {
			clearCache(chartPura);
		}

		return chartPura;
	}

	@Override
	public ChartPura updateImpl(ChartPura chartPura) {
		boolean isNew = chartPura.isNew();

		if (!(chartPura instanceof ChartPuraModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(chartPura.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(chartPura);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in chartPura proxy " +
						invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom ChartPura implementation " +
					chartPura.getClass());
		}

		ChartPuraModelImpl chartPuraModelImpl = (ChartPuraModelImpl)chartPura;

		if (Validator.isNull(chartPura.getUuid())) {
			String uuid = PortalUUIDUtil.generate();

			chartPura.setUuid(uuid);
		}

		ServiceContext serviceContext =
			ServiceContextThreadLocal.getServiceContext();

		Date now = new Date();

		if (isNew && (chartPura.getCreateDate() == null)) {
			if (serviceContext == null) {
				chartPura.setCreateDate(now);
			}
			else {
				chartPura.setCreateDate(serviceContext.getCreateDate(now));
			}
		}

		if (!chartPuraModelImpl.hasSetModifiedDate()) {
			if (serviceContext == null) {
				chartPura.setModifiedDate(now);
			}
			else {
				chartPura.setModifiedDate(serviceContext.getModifiedDate(now));
			}
		}

		Session session = null;

		try {
			session = openSession();

			if (chartPura.isNew()) {
				session.save(chartPura);

				chartPura.setNew(false);
			}
			else {
				chartPura = (ChartPura)session.merge(chartPura);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!ChartPuraModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else if (isNew) {
			Object[] args = new Object[] {chartPuraModelImpl.getUuid()};

			finderCache.removeResult(_finderPathCountByUuid, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid, args);

			args = new Object[] {
				chartPuraModelImpl.getUuid(), chartPuraModelImpl.getCompanyId()
			};

			finderCache.removeResult(_finderPathCountByUuid_C, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByUuid_C, args);

			args = new Object[] {chartPuraModelImpl.getScore()};

			finderCache.removeResult(_finderPathCountByScore, args);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindByScore, args);

			finderCache.removeResult(_finderPathCountAll, FINDER_ARGS_EMPTY);
			finderCache.removeResult(
				_finderPathWithoutPaginationFindAll, FINDER_ARGS_EMPTY);
		}
		else {
			if ((chartPuraModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					chartPuraModelImpl.getOriginalUuid()
				};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);

				args = new Object[] {chartPuraModelImpl.getUuid()};

				finderCache.removeResult(_finderPathCountByUuid, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid, args);
			}

			if ((chartPuraModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByUuid_C.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					chartPuraModelImpl.getOriginalUuid(),
					chartPuraModelImpl.getOriginalCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);

				args = new Object[] {
					chartPuraModelImpl.getUuid(),
					chartPuraModelImpl.getCompanyId()
				};

				finderCache.removeResult(_finderPathCountByUuid_C, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByUuid_C, args);
			}

			if ((chartPuraModelImpl.getColumnBitmask() &
				 _finderPathWithoutPaginationFindByScore.getColumnBitmask()) !=
					 0) {

				Object[] args = new Object[] {
					chartPuraModelImpl.getOriginalScore()
				};

				finderCache.removeResult(_finderPathCountByScore, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByScore, args);

				args = new Object[] {chartPuraModelImpl.getScore()};

				finderCache.removeResult(_finderPathCountByScore, args);
				finderCache.removeResult(
					_finderPathWithoutPaginationFindByScore, args);
			}
		}

		entityCache.putResult(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED, ChartPuraImpl.class,
			chartPura.getPrimaryKey(), chartPura, false);

		clearUniqueFindersCache(chartPuraModelImpl, false);
		cacheUniqueFindersCache(chartPuraModelImpl);

		chartPura.resetOriginalValues();

		return chartPura;
	}

	/**
	 * Returns the chart pura with the primary key or throws a <code>com.liferay.portal.kernel.exception.NoSuchModelException</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the chart pura
	 * @return the chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura findByPrimaryKey(Serializable primaryKey)
		throws NoSuchChartPuraException {

		ChartPura chartPura = fetchByPrimaryKey(primaryKey);

		if (chartPura == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchChartPuraException(
				_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
		}

		return chartPura;
	}

	/**
	 * Returns the chart pura with the primary key or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura findByPrimaryKey(long id) throws NoSuchChartPuraException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the chart pura with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the chart pura
	 * @return the chart pura, or <code>null</code> if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED, ChartPuraImpl.class,
			primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ChartPura chartPura = (ChartPura)serializable;

		if (chartPura == null) {
			Session session = null;

			try {
				session = openSession();

				chartPura = (ChartPura)session.get(
					ChartPuraImpl.class, primaryKey);

				if (chartPura != null) {
					cacheResult(chartPura);
				}
				else {
					entityCache.putResult(
						ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
						ChartPuraImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(
					ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
					ChartPuraImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return chartPura;
	}

	/**
	 * Returns the chart pura with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura, or <code>null</code> if a chart pura with the primary key could not be found
	 */
	@Override
	public ChartPura fetchByPrimaryKey(long id) {
		return fetchByPrimaryKey((Serializable)id);
	}

	@Override
	public Map<Serializable, ChartPura> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ChartPura> map =
			new HashMap<Serializable, ChartPura>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ChartPura chartPura = fetchByPrimaryKey(primaryKey);

			if (chartPura != null) {
				map.put(primaryKey, chartPura);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(
				ChartPuraModelImpl.ENTITY_CACHE_ENABLED, ChartPuraImpl.class,
				primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ChartPura)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler(
			uncachedPrimaryKeys.size() * 2 + 1);

		query.append(_SQL_SELECT_CHARTPURA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ChartPura chartPura : (List<ChartPura>)q.list()) {
				map.put(chartPura.getPrimaryKeyObj(), chartPura);

				cacheResult(chartPura);

				uncachedPrimaryKeys.remove(chartPura.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(
					ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
					ChartPuraImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the chart puras.
	 *
	 * @return the chart puras
	 */
	@Override
	public List<ChartPura> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of chart puras
	 */
	@Override
	public List<ChartPura> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of chart puras
	 */
	@Override
	public List<ChartPura> findAll(
		int start, int end, OrderByComparator<ChartPura> orderByComparator) {

		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of chart puras
	 */
	@Override
	public List<ChartPura> findAll(
		int start, int end, OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
			(orderByComparator == null)) {

			pagination = false;
			finderPath = _finderPathWithoutPaginationFindAll;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = _finderPathWithPaginationFindAll;
			finderArgs = new Object[] {start, end, orderByComparator};
		}

		List<ChartPura> list = null;

		if (retrieveFromCache) {
			list = (List<ChartPura>)finderCache.getResult(
				finderPath, finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(
					2 + (orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CHARTPURA);

				appendOrderByComparator(
					query, _ORDER_BY_ENTITY_ALIAS, orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CHARTPURA;

				if (pagination) {
					sql = sql.concat(ChartPuraModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ChartPura>)QueryUtil.list(
						q, getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the chart puras from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ChartPura chartPura : findAll()) {
			remove(chartPura);
		}
	}

	/**
	 * Returns the number of chart puras.
	 *
	 * @return the number of chart puras
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(
			_finderPathCountAll, FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CHARTPURA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				finderCache.removeResult(
					_finderPathCountAll, FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ChartPuraModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the chart pura persistence.
	 */
	public void afterPropertiesSet() {
		_finderPathWithPaginationFindAll = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);

		_finderPathWithoutPaginationFindAll = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll",
			new String[0]);

		_finderPathCountAll = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll",
			new String[0]);

		_finderPathWithPaginationFindByUuid = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid",
			new String[] {
				String.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid",
			new String[] {String.class.getName()},
			ChartPuraModelImpl.UUID_COLUMN_BITMASK |
			ChartPuraModelImpl.SCORE_COLUMN_BITMASK |
			ChartPuraModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByUuid = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid",
			new String[] {String.class.getName()});

		_finderPathFetchByUUID_G = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()},
			ChartPuraModelImpl.UUID_COLUMN_BITMASK |
			ChartPuraModelImpl.GROUPID_COLUMN_BITMASK);

		_finderPathCountByUUID_G = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUUID_G",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathWithPaginationFindByUuid_C = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByUuid_C",
			new String[] {
				String.class.getName(), Long.class.getName(),
				Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByUuid_C = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()},
			ChartPuraModelImpl.UUID_COLUMN_BITMASK |
			ChartPuraModelImpl.COMPANYID_COLUMN_BITMASK |
			ChartPuraModelImpl.SCORE_COLUMN_BITMASK |
			ChartPuraModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByUuid_C = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUuid_C",
			new String[] {String.class.getName(), Long.class.getName()});

		_finderPathFetchByScore_Tiempo = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_ENTITY, "fetchByScore_Tiempo",
			new String[] {Integer.class.getName(), Integer.class.getName()},
			ChartPuraModelImpl.SCORE_COLUMN_BITMASK |
			ChartPuraModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByScore_Tiempo = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScore_Tiempo",
			new String[] {Integer.class.getName(), Integer.class.getName()});

		_finderPathWithPaginationFindByScore = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByScore",
			new String[] {
				Integer.class.getName(), Integer.class.getName(),
				Integer.class.getName(), OrderByComparator.class.getName()
			});

		_finderPathWithoutPaginationFindByScore = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, ChartPuraImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByScore",
			new String[] {Integer.class.getName()},
			ChartPuraModelImpl.SCORE_COLUMN_BITMASK |
			ChartPuraModelImpl.TIEMPO_COLUMN_BITMASK);

		_finderPathCountByScore = new FinderPath(
			ChartPuraModelImpl.ENTITY_CACHE_ENABLED,
			ChartPuraModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByScore",
			new String[] {Integer.class.getName()});
	}

	public void destroy() {
		entityCache.removeCache(ChartPuraImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = CompanyProviderWrapper.class)
	protected CompanyProvider companyProvider;

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;

	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;

	private static final String _SQL_SELECT_CHARTPURA =
		"SELECT chartPura FROM ChartPura chartPura";

	private static final String _SQL_SELECT_CHARTPURA_WHERE_PKS_IN =
		"SELECT chartPura FROM ChartPura chartPura WHERE id_ IN (";

	private static final String _SQL_SELECT_CHARTPURA_WHERE =
		"SELECT chartPura FROM ChartPura chartPura WHERE ";

	private static final String _SQL_COUNT_CHARTPURA =
		"SELECT COUNT(chartPura) FROM ChartPura chartPura";

	private static final String _SQL_COUNT_CHARTPURA_WHERE =
		"SELECT COUNT(chartPura) FROM ChartPura chartPura WHERE ";

	private static final String _ORDER_BY_ENTITY_ALIAS = "chartPura.";

	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY =
		"No ChartPura exists with the primary key ";

	private static final String _NO_SUCH_ENTITY_WITH_KEY =
		"No ChartPura exists with the key {";

	private static final Log _log = LogFactoryUtil.getLog(
		ChartPuraPersistenceImpl.class);

	private static final Set<String> _badColumnNames = SetUtil.fromArray(
		new String[] {"uuid", "id"});

}