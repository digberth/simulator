/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.porvenir.luis.portafolio.model.Pura;
import com.porvenir.luis.portafolio.service.base.PuraLocalServiceBaseImpl;

import java.util.Date;
import java.util.List;

/**
 * The implementation of the pura local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.porvenir.luis.portafolio.service.PuraLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PuraLocalServiceBaseImpl
 */
public class PuraLocalServiceImpl extends PuraLocalServiceBaseImpl {

	public List<Pura> getByScore(int score){
		return puraPersistence.findByScore(score);
	}
	
	public Pura addPura(long userId, int score, String alternativa, String nombre, double peso, ServiceContext context) throws PortalException {
		
		long groupId = context.getScopeGroupId();
		User user = userLocalService.getUserById(userId);
		Date now = new Date();
		
		long puraId = counterLocalService.increment();
		Pura pura = puraPersistence.create(puraId);
		
		pura.setUuid(context.getUuid());
		pura.setUserId(userId);
		pura.setGroupId(groupId);
		pura.setCompanyId(user.getCompanyId());
		pura.setUserName(user.getFullName());
		pura.setCreateDate(context.getCreateDate(now));
		pura.setModifiedDate(context.getModifiedDate(now));
		
		pura.setScore(score);
		pura.setAlternativa(alternativa);
		pura.setNombre(nombre);
		pura.setPeso(peso);
		
		puraPersistence.update(pura);
		
		return pura;
		
	}
	
	public void deleteAll() {
		puraPersistence.removeAll();
	}
}