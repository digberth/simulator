/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import com.porvenir.luis.portafolio.model.ChartPura;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ChartPura in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ChartPuraCacheModel
	implements CacheModel<ChartPura>, Externalizable {

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ChartPuraCacheModel)) {
			return false;
		}

		ChartPuraCacheModel chartPuraCacheModel = (ChartPuraCacheModel)obj;

		if (id == chartPuraCacheModel.id) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, id);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", id=");
		sb.append(id);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", score=");
		sb.append(score);
		sb.append(", tiempo=");
		sb.append(tiempo);
		sb.append(", inferior=");
		sb.append(inferior);
		sb.append(", medio=");
		sb.append(medio);
		sb.append(", superior=");
		sb.append(superior);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ChartPura toEntityModel() {
		ChartPuraImpl chartPuraImpl = new ChartPuraImpl();

		if (uuid == null) {
			chartPuraImpl.setUuid("");
		}
		else {
			chartPuraImpl.setUuid(uuid);
		}

		chartPuraImpl.setId(id);
		chartPuraImpl.setGroupId(groupId);
		chartPuraImpl.setCompanyId(companyId);
		chartPuraImpl.setUserId(userId);

		if (userName == null) {
			chartPuraImpl.setUserName("");
		}
		else {
			chartPuraImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			chartPuraImpl.setCreateDate(null);
		}
		else {
			chartPuraImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			chartPuraImpl.setModifiedDate(null);
		}
		else {
			chartPuraImpl.setModifiedDate(new Date(modifiedDate));
		}

		chartPuraImpl.setScore(score);
		chartPuraImpl.setTiempo(tiempo);
		chartPuraImpl.setInferior(inferior);
		chartPuraImpl.setMedio(medio);
		chartPuraImpl.setSuperior(superior);

		chartPuraImpl.resetOriginalValues();

		return chartPuraImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		id = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();

		score = objectInput.readInt();

		tiempo = objectInput.readInt();

		inferior = objectInput.readDouble();

		medio = objectInput.readDouble();

		superior = objectInput.readDouble();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(id);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		objectOutput.writeInt(score);

		objectOutput.writeInt(tiempo);

		objectOutput.writeDouble(inferior);

		objectOutput.writeDouble(medio);

		objectOutput.writeDouble(superior);
	}

	public String uuid;
	public long id;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public int score;
	public int tiempo;
	public double inferior;
	public double medio;
	public double superior;

}