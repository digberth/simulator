/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.base;

import aQute.bnd.annotation.ProviderType;

import com.liferay.exportimport.kernel.lar.ExportImportHelperUtil;
import com.liferay.exportimport.kernel.lar.ManifestSummary;
import com.liferay.exportimport.kernel.lar.PortletDataContext;
import com.liferay.exportimport.kernel.lar.StagedModelDataHandlerUtil;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.db.DB;
import com.liferay.portal.kernel.dao.db.DBManagerUtil;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdate;
import com.liferay.portal.kernel.dao.jdbc.SqlUpdateFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DefaultActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.module.framework.service.IdentifiableOSGiService;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalServiceImpl;
import com.liferay.portal.kernel.service.PersistedModelLocalServiceRegistry;
import com.liferay.portal.kernel.service.persistence.ClassNamePersistence;
import com.liferay.portal.kernel.service.persistence.UserPersistence;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import com.porvenir.luis.portafolio.model.Diversificado;
import com.porvenir.luis.portafolio.service.DiversificadoLocalService;
import com.porvenir.luis.portafolio.service.persistence.ChartDiversificadoPersistence;
import com.porvenir.luis.portafolio.service.persistence.ChartPuraPersistence;
import com.porvenir.luis.portafolio.service.persistence.DiversificadoPersistence;
import com.porvenir.luis.portafolio.service.persistence.PuraPersistence;

import java.io.Serializable;

import java.util.List;

import javax.sql.DataSource;

/**
 * Provides the base implementation for the diversificado local service.
 *
 * <p>
 * This implementation exists only as a container for the default service methods generated by ServiceBuilder. All custom service methods should be put in {@link com.porvenir.luis.portafolio.service.impl.DiversificadoLocalServiceImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.porvenir.luis.portafolio.service.impl.DiversificadoLocalServiceImpl
 * @generated
 */
@ProviderType
public abstract class DiversificadoLocalServiceBaseImpl
	extends BaseLocalServiceImpl
	implements DiversificadoLocalService, IdentifiableOSGiService {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Use <code>DiversificadoLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.porvenir.luis.portafolio.service.DiversificadoLocalServiceUtil</code>.
	 */

	/**
	 * Adds the diversificado to the database. Also notifies the appropriate model listeners.
	 *
	 * @param diversificado the diversificado
	 * @return the diversificado that was added
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Diversificado addDiversificado(Diversificado diversificado) {
		diversificado.setNew(true);

		return diversificadoPersistence.update(diversificado);
	}

	/**
	 * Creates a new diversificado with the primary key. Does not add the diversificado to the database.
	 *
	 * @param divId the primary key for the new diversificado
	 * @return the new diversificado
	 */
	@Override
	@Transactional(enabled = false)
	public Diversificado createDiversificado(long divId) {
		return diversificadoPersistence.create(divId);
	}

	/**
	 * Deletes the diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado that was removed
	 * @throws PortalException if a diversificado with the primary key could not be found
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Diversificado deleteDiversificado(long divId)
		throws PortalException {

		return diversificadoPersistence.remove(divId);
	}

	/**
	 * Deletes the diversificado from the database. Also notifies the appropriate model listeners.
	 *
	 * @param diversificado the diversificado
	 * @return the diversificado that was removed
	 */
	@Indexable(type = IndexableType.DELETE)
	@Override
	public Diversificado deleteDiversificado(Diversificado diversificado) {
		return diversificadoPersistence.remove(diversificado);
	}

	@Override
	public DynamicQuery dynamicQuery() {
		Class<?> clazz = getClass();

		return DynamicQueryFactoryUtil.forClass(
			Diversificado.class, clazz.getClassLoader());
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery) {
		return diversificadoPersistence.findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return diversificadoPersistence.findWithDynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> List<T> dynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<T> orderByComparator) {

		return diversificadoPersistence.findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(DynamicQuery dynamicQuery) {
		return diversificadoPersistence.countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		DynamicQuery dynamicQuery, Projection projection) {

		return diversificadoPersistence.countWithDynamicQuery(
			dynamicQuery, projection);
	}

	@Override
	public Diversificado fetchDiversificado(long divId) {
		return diversificadoPersistence.fetchByPrimaryKey(divId);
	}

	/**
	 * Returns the diversificado matching the UUID and group.
	 *
	 * @param uuid the diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public Diversificado fetchDiversificadoByUuidAndGroupId(
		String uuid, long groupId) {

		return diversificadoPersistence.fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the diversificado with the primary key.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado
	 * @throws PortalException if a diversificado with the primary key could not be found
	 */
	@Override
	public Diversificado getDiversificado(long divId) throws PortalException {
		return diversificadoPersistence.findByPrimaryKey(divId);
	}

	@Override
	public ActionableDynamicQuery getActionableDynamicQuery() {
		ActionableDynamicQuery actionableDynamicQuery =
			new DefaultActionableDynamicQuery();

		actionableDynamicQuery.setBaseLocalService(diversificadoLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Diversificado.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("divId");

		return actionableDynamicQuery;
	}

	@Override
	public IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		IndexableActionableDynamicQuery indexableActionableDynamicQuery =
			new IndexableActionableDynamicQuery();

		indexableActionableDynamicQuery.setBaseLocalService(
			diversificadoLocalService);
		indexableActionableDynamicQuery.setClassLoader(getClassLoader());
		indexableActionableDynamicQuery.setModelClass(Diversificado.class);

		indexableActionableDynamicQuery.setPrimaryKeyPropertyName("divId");

		return indexableActionableDynamicQuery;
	}

	protected void initActionableDynamicQuery(
		ActionableDynamicQuery actionableDynamicQuery) {

		actionableDynamicQuery.setBaseLocalService(diversificadoLocalService);
		actionableDynamicQuery.setClassLoader(getClassLoader());
		actionableDynamicQuery.setModelClass(Diversificado.class);

		actionableDynamicQuery.setPrimaryKeyPropertyName("divId");
	}

	@Override
	public ExportActionableDynamicQuery getExportActionableDynamicQuery(
		final PortletDataContext portletDataContext) {

		final ExportActionableDynamicQuery exportActionableDynamicQuery =
			new ExportActionableDynamicQuery() {

				@Override
				public long performCount() throws PortalException {
					ManifestSummary manifestSummary =
						portletDataContext.getManifestSummary();

					StagedModelType stagedModelType = getStagedModelType();

					long modelAdditionCount = super.performCount();

					manifestSummary.addModelAdditionCount(
						stagedModelType, modelAdditionCount);

					long modelDeletionCount =
						ExportImportHelperUtil.getModelDeletionCount(
							portletDataContext, stagedModelType);

					manifestSummary.addModelDeletionCount(
						stagedModelType, modelDeletionCount);

					return modelAdditionCount;
				}

			};

		initActionableDynamicQuery(exportActionableDynamicQuery);

		exportActionableDynamicQuery.setAddCriteriaMethod(
			new ActionableDynamicQuery.AddCriteriaMethod() {

				@Override
				public void addCriteria(DynamicQuery dynamicQuery) {
					portletDataContext.addDateRangeCriteria(
						dynamicQuery, "modifiedDate");
				}

			});

		exportActionableDynamicQuery.setCompanyId(
			portletDataContext.getCompanyId());

		exportActionableDynamicQuery.setPerformActionMethod(
			new ActionableDynamicQuery.PerformActionMethod<Diversificado>() {

				@Override
				public void performAction(Diversificado diversificado)
					throws PortalException {

					StagedModelDataHandlerUtil.exportStagedModel(
						portletDataContext, diversificado);
				}

			});
		exportActionableDynamicQuery.setStagedModelType(
			new StagedModelType(
				PortalUtil.getClassNameId(Diversificado.class.getName())));

		return exportActionableDynamicQuery;
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException {

		return diversificadoLocalService.deleteDiversificado(
			(Diversificado)persistedModel);
	}

	@Override
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException {

		return diversificadoPersistence.findByPrimaryKey(primaryKeyObj);
	}

	/**
	 * Returns all the diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the diversificados
	 * @param companyId the primary key of the company
	 * @return the matching diversificados, or an empty list if no matches were found
	 */
	@Override
	public List<Diversificado> getDiversificadosByUuidAndCompanyId(
		String uuid, long companyId) {

		return diversificadoPersistence.findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the diversificados
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching diversificados, or an empty list if no matches were found
	 */
	@Override
	public List<Diversificado> getDiversificadosByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return diversificadoPersistence.findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the diversificado matching the UUID and group.
	 *
	 * @param uuid the diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching diversificado
	 * @throws PortalException if a matching diversificado could not be found
	 */
	@Override
	public Diversificado getDiversificadoByUuidAndGroupId(
			String uuid, long groupId)
		throws PortalException {

		return diversificadoPersistence.findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns a range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of diversificados
	 */
	@Override
	public List<Diversificado> getDiversificados(int start, int end) {
		return diversificadoPersistence.findAll(start, end);
	}

	/**
	 * Returns the number of diversificados.
	 *
	 * @return the number of diversificados
	 */
	@Override
	public int getDiversificadosCount() {
		return diversificadoPersistence.countAll();
	}

	/**
	 * Updates the diversificado in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param diversificado the diversificado
	 * @return the diversificado that was updated
	 */
	@Indexable(type = IndexableType.REINDEX)
	@Override
	public Diversificado updateDiversificado(Diversificado diversificado) {
		return diversificadoPersistence.update(diversificado);
	}

	/**
	 * Returns the chart diversificado local service.
	 *
	 * @return the chart diversificado local service
	 */
	public com.porvenir.luis.portafolio.service.ChartDiversificadoLocalService
		getChartDiversificadoLocalService() {

		return chartDiversificadoLocalService;
	}

	/**
	 * Sets the chart diversificado local service.
	 *
	 * @param chartDiversificadoLocalService the chart diversificado local service
	 */
	public void setChartDiversificadoLocalService(
		com.porvenir.luis.portafolio.service.ChartDiversificadoLocalService
			chartDiversificadoLocalService) {

		this.chartDiversificadoLocalService = chartDiversificadoLocalService;
	}

	/**
	 * Returns the chart diversificado persistence.
	 *
	 * @return the chart diversificado persistence
	 */
	public ChartDiversificadoPersistence getChartDiversificadoPersistence() {
		return chartDiversificadoPersistence;
	}

	/**
	 * Sets the chart diversificado persistence.
	 *
	 * @param chartDiversificadoPersistence the chart diversificado persistence
	 */
	public void setChartDiversificadoPersistence(
		ChartDiversificadoPersistence chartDiversificadoPersistence) {

		this.chartDiversificadoPersistence = chartDiversificadoPersistence;
	}

	/**
	 * Returns the chart pura local service.
	 *
	 * @return the chart pura local service
	 */
	public com.porvenir.luis.portafolio.service.ChartPuraLocalService
		getChartPuraLocalService() {

		return chartPuraLocalService;
	}

	/**
	 * Sets the chart pura local service.
	 *
	 * @param chartPuraLocalService the chart pura local service
	 */
	public void setChartPuraLocalService(
		com.porvenir.luis.portafolio.service.ChartPuraLocalService
			chartPuraLocalService) {

		this.chartPuraLocalService = chartPuraLocalService;
	}

	/**
	 * Returns the chart pura persistence.
	 *
	 * @return the chart pura persistence
	 */
	public ChartPuraPersistence getChartPuraPersistence() {
		return chartPuraPersistence;
	}

	/**
	 * Sets the chart pura persistence.
	 *
	 * @param chartPuraPersistence the chart pura persistence
	 */
	public void setChartPuraPersistence(
		ChartPuraPersistence chartPuraPersistence) {

		this.chartPuraPersistence = chartPuraPersistence;
	}

	/**
	 * Returns the diversificado local service.
	 *
	 * @return the diversificado local service
	 */
	public DiversificadoLocalService getDiversificadoLocalService() {
		return diversificadoLocalService;
	}

	/**
	 * Sets the diversificado local service.
	 *
	 * @param diversificadoLocalService the diversificado local service
	 */
	public void setDiversificadoLocalService(
		DiversificadoLocalService diversificadoLocalService) {

		this.diversificadoLocalService = diversificadoLocalService;
	}

	/**
	 * Returns the diversificado persistence.
	 *
	 * @return the diversificado persistence
	 */
	public DiversificadoPersistence getDiversificadoPersistence() {
		return diversificadoPersistence;
	}

	/**
	 * Sets the diversificado persistence.
	 *
	 * @param diversificadoPersistence the diversificado persistence
	 */
	public void setDiversificadoPersistence(
		DiversificadoPersistence diversificadoPersistence) {

		this.diversificadoPersistence = diversificadoPersistence;
	}

	/**
	 * Returns the pura local service.
	 *
	 * @return the pura local service
	 */
	public com.porvenir.luis.portafolio.service.PuraLocalService
		getPuraLocalService() {

		return puraLocalService;
	}

	/**
	 * Sets the pura local service.
	 *
	 * @param puraLocalService the pura local service
	 */
	public void setPuraLocalService(
		com.porvenir.luis.portafolio.service.PuraLocalService
			puraLocalService) {

		this.puraLocalService = puraLocalService;
	}

	/**
	 * Returns the pura persistence.
	 *
	 * @return the pura persistence
	 */
	public PuraPersistence getPuraPersistence() {
		return puraPersistence;
	}

	/**
	 * Sets the pura persistence.
	 *
	 * @param puraPersistence the pura persistence
	 */
	public void setPuraPersistence(PuraPersistence puraPersistence) {
		this.puraPersistence = puraPersistence;
	}

	/**
	 * Returns the counter local service.
	 *
	 * @return the counter local service
	 */
	public com.liferay.counter.kernel.service.CounterLocalService
		getCounterLocalService() {

		return counterLocalService;
	}

	/**
	 * Sets the counter local service.
	 *
	 * @param counterLocalService the counter local service
	 */
	public void setCounterLocalService(
		com.liferay.counter.kernel.service.CounterLocalService
			counterLocalService) {

		this.counterLocalService = counterLocalService;
	}

	/**
	 * Returns the class name local service.
	 *
	 * @return the class name local service
	 */
	public com.liferay.portal.kernel.service.ClassNameLocalService
		getClassNameLocalService() {

		return classNameLocalService;
	}

	/**
	 * Sets the class name local service.
	 *
	 * @param classNameLocalService the class name local service
	 */
	public void setClassNameLocalService(
		com.liferay.portal.kernel.service.ClassNameLocalService
			classNameLocalService) {

		this.classNameLocalService = classNameLocalService;
	}

	/**
	 * Returns the class name persistence.
	 *
	 * @return the class name persistence
	 */
	public ClassNamePersistence getClassNamePersistence() {
		return classNamePersistence;
	}

	/**
	 * Sets the class name persistence.
	 *
	 * @param classNamePersistence the class name persistence
	 */
	public void setClassNamePersistence(
		ClassNamePersistence classNamePersistence) {

		this.classNamePersistence = classNamePersistence;
	}

	/**
	 * Returns the resource local service.
	 *
	 * @return the resource local service
	 */
	public com.liferay.portal.kernel.service.ResourceLocalService
		getResourceLocalService() {

		return resourceLocalService;
	}

	/**
	 * Sets the resource local service.
	 *
	 * @param resourceLocalService the resource local service
	 */
	public void setResourceLocalService(
		com.liferay.portal.kernel.service.ResourceLocalService
			resourceLocalService) {

		this.resourceLocalService = resourceLocalService;
	}

	/**
	 * Returns the user local service.
	 *
	 * @return the user local service
	 */
	public com.liferay.portal.kernel.service.UserLocalService
		getUserLocalService() {

		return userLocalService;
	}

	/**
	 * Sets the user local service.
	 *
	 * @param userLocalService the user local service
	 */
	public void setUserLocalService(
		com.liferay.portal.kernel.service.UserLocalService userLocalService) {

		this.userLocalService = userLocalService;
	}

	/**
	 * Returns the user persistence.
	 *
	 * @return the user persistence
	 */
	public UserPersistence getUserPersistence() {
		return userPersistence;
	}

	/**
	 * Sets the user persistence.
	 *
	 * @param userPersistence the user persistence
	 */
	public void setUserPersistence(UserPersistence userPersistence) {
		this.userPersistence = userPersistence;
	}

	public void afterPropertiesSet() {
		persistedModelLocalServiceRegistry.register(
			"com.porvenir.luis.portafolio.model.Diversificado",
			diversificadoLocalService);
	}

	public void destroy() {
		persistedModelLocalServiceRegistry.unregister(
			"com.porvenir.luis.portafolio.model.Diversificado");
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return DiversificadoLocalService.class.getName();
	}

	protected Class<?> getModelClass() {
		return Diversificado.class;
	}

	protected String getModelClassName() {
		return Diversificado.class.getName();
	}

	/**
	 * Performs a SQL query.
	 *
	 * @param sql the sql query
	 */
	protected void runSQL(String sql) {
		try {
			DataSource dataSource = diversificadoPersistence.getDataSource();

			DB db = DBManagerUtil.getDB();

			sql = db.buildSQL(sql);
			sql = PortalUtil.transformSQL(sql);

			SqlUpdate sqlUpdate = SqlUpdateFactoryUtil.getSqlUpdate(
				dataSource, sql);

			sqlUpdate.update();
		}
		catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@BeanReference(
		type = com.porvenir.luis.portafolio.service.ChartDiversificadoLocalService.class
	)
	protected
		com.porvenir.luis.portafolio.service.ChartDiversificadoLocalService
			chartDiversificadoLocalService;

	@BeanReference(type = ChartDiversificadoPersistence.class)
	protected ChartDiversificadoPersistence chartDiversificadoPersistence;

	@BeanReference(
		type = com.porvenir.luis.portafolio.service.ChartPuraLocalService.class
	)
	protected com.porvenir.luis.portafolio.service.ChartPuraLocalService
		chartPuraLocalService;

	@BeanReference(type = ChartPuraPersistence.class)
	protected ChartPuraPersistence chartPuraPersistence;

	@BeanReference(type = DiversificadoLocalService.class)
	protected DiversificadoLocalService diversificadoLocalService;

	@BeanReference(type = DiversificadoPersistence.class)
	protected DiversificadoPersistence diversificadoPersistence;

	@BeanReference(
		type = com.porvenir.luis.portafolio.service.PuraLocalService.class
	)
	protected com.porvenir.luis.portafolio.service.PuraLocalService
		puraLocalService;

	@BeanReference(type = PuraPersistence.class)
	protected PuraPersistence puraPersistence;

	@ServiceReference(
		type = com.liferay.counter.kernel.service.CounterLocalService.class
	)
	protected com.liferay.counter.kernel.service.CounterLocalService
		counterLocalService;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.ClassNameLocalService.class
	)
	protected com.liferay.portal.kernel.service.ClassNameLocalService
		classNameLocalService;

	@ServiceReference(type = ClassNamePersistence.class)
	protected ClassNamePersistence classNamePersistence;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.ResourceLocalService.class
	)
	protected com.liferay.portal.kernel.service.ResourceLocalService
		resourceLocalService;

	@ServiceReference(
		type = com.liferay.portal.kernel.service.UserLocalService.class
	)
	protected com.liferay.portal.kernel.service.UserLocalService
		userLocalService;

	@ServiceReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;

	@ServiceReference(type = PersistedModelLocalServiceRegistry.class)
	protected PersistedModelLocalServiceRegistry
		persistedModelLocalServiceRegistry;

}