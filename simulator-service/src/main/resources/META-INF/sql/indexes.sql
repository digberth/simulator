create index IX_1F48100B on PORTAFOLIO_ChartDiversificado (nombre[$COLUMN_LENGTH:75$], tiempo);
create index IX_2087A972 on PORTAFOLIO_ChartDiversificado (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_366FE474 on PORTAFOLIO_ChartDiversificado (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_5EA1D690 on PORTAFOLIO_ChartPura (score, tiempo);
create index IX_C34C2A76 on PORTAFOLIO_ChartPura (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_43A8D678 on PORTAFOLIO_ChartPura (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_D44EA8B2 on PORTAFOLIO_Diversificado (score);
create index IX_90EEFC04 on PORTAFOLIO_Diversificado (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_634EB386 on PORTAFOLIO_Diversificado (uuid_[$COLUMN_LENGTH:75$], groupId);

create index IX_86C6DD12 on PORTAFOLIO_Pura (score);
create index IX_1BAC4BA4 on PORTAFOLIO_Pura (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_7E5B6B26 on PORTAFOLIO_Pura (uuid_[$COLUMN_LENGTH:75$], groupId);