create table PORTAFOLIO_ChartDiversificado (
	uuid_ VARCHAR(75) null,
	id_ LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nombre VARCHAR(75) null,
	tiempo INTEGER,
	inferior DOUBLE,
	medio DOUBLE,
	superior DOUBLE
);

create table PORTAFOLIO_ChartPura (
	uuid_ VARCHAR(75) null,
	id_ LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	score INTEGER,
	tiempo INTEGER,
	inferior DOUBLE,
	medio DOUBLE,
	superior DOUBLE
);

create table PORTAFOLIO_Diversificado (
	uuid_ VARCHAR(75) null,
	divId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	score INTEGER,
	nombre VARCHAR(75) null,
	suma_peso DOUBLE,
	factor_riesgo VARCHAR(75) null,
	descobertura DOUBLE
);

create table PORTAFOLIO_Pura (
	uuid_ VARCHAR(75) null,
	divId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	score INTEGER,
	alternativa VARCHAR(75) null,
	nombre VARCHAR(75) null,
	peso DOUBLE
);