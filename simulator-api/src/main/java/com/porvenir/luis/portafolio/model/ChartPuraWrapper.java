/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ChartPura}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartPura
 * @generated
 */
@ProviderType
public class ChartPuraWrapper implements ChartPura, ModelWrapper<ChartPura> {

	public ChartPuraWrapper(ChartPura chartPura) {
		_chartPura = chartPura;
	}

	@Override
	public Class<?> getModelClass() {
		return ChartPura.class;
	}

	@Override
	public String getModelClassName() {
		return ChartPura.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("id", getId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("score", getScore());
		attributes.put("tiempo", getTiempo());
		attributes.put("inferior", getInferior());
		attributes.put("medio", getMedio());
		attributes.put("superior", getSuperior());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Integer score = (Integer)attributes.get("score");

		if (score != null) {
			setScore(score);
		}

		Integer tiempo = (Integer)attributes.get("tiempo");

		if (tiempo != null) {
			setTiempo(tiempo);
		}

		Double inferior = (Double)attributes.get("inferior");

		if (inferior != null) {
			setInferior(inferior);
		}

		Double medio = (Double)attributes.get("medio");

		if (medio != null) {
			setMedio(medio);
		}

		Double superior = (Double)attributes.get("superior");

		if (superior != null) {
			setSuperior(superior);
		}
	}

	@Override
	public Object clone() {
		return new ChartPuraWrapper((ChartPura)_chartPura.clone());
	}

	@Override
	public int compareTo(ChartPura chartPura) {
		return _chartPura.compareTo(chartPura);
	}

	/**
	 * Returns the company ID of this chart pura.
	 *
	 * @return the company ID of this chart pura
	 */
	@Override
	public long getCompanyId() {
		return _chartPura.getCompanyId();
	}

	/**
	 * Returns the create date of this chart pura.
	 *
	 * @return the create date of this chart pura
	 */
	@Override
	public Date getCreateDate() {
		return _chartPura.getCreateDate();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _chartPura.getExpandoBridge();
	}

	/**
	 * Returns the group ID of this chart pura.
	 *
	 * @return the group ID of this chart pura
	 */
	@Override
	public long getGroupId() {
		return _chartPura.getGroupId();
	}

	/**
	 * Returns the ID of this chart pura.
	 *
	 * @return the ID of this chart pura
	 */
	@Override
	public long getId() {
		return _chartPura.getId();
	}

	/**
	 * Returns the inferior of this chart pura.
	 *
	 * @return the inferior of this chart pura
	 */
	@Override
	public double getInferior() {
		return _chartPura.getInferior();
	}

	/**
	 * Returns the medio of this chart pura.
	 *
	 * @return the medio of this chart pura
	 */
	@Override
	public double getMedio() {
		return _chartPura.getMedio();
	}

	/**
	 * Returns the modified date of this chart pura.
	 *
	 * @return the modified date of this chart pura
	 */
	@Override
	public Date getModifiedDate() {
		return _chartPura.getModifiedDate();
	}

	/**
	 * Returns the primary key of this chart pura.
	 *
	 * @return the primary key of this chart pura
	 */
	@Override
	public long getPrimaryKey() {
		return _chartPura.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _chartPura.getPrimaryKeyObj();
	}

	/**
	 * Returns the score of this chart pura.
	 *
	 * @return the score of this chart pura
	 */
	@Override
	public int getScore() {
		return _chartPura.getScore();
	}

	/**
	 * Returns the superior of this chart pura.
	 *
	 * @return the superior of this chart pura
	 */
	@Override
	public double getSuperior() {
		return _chartPura.getSuperior();
	}

	/**
	 * Returns the tiempo of this chart pura.
	 *
	 * @return the tiempo of this chart pura
	 */
	@Override
	public int getTiempo() {
		return _chartPura.getTiempo();
	}

	/**
	 * Returns the user ID of this chart pura.
	 *
	 * @return the user ID of this chart pura
	 */
	@Override
	public long getUserId() {
		return _chartPura.getUserId();
	}

	/**
	 * Returns the user name of this chart pura.
	 *
	 * @return the user name of this chart pura
	 */
	@Override
	public String getUserName() {
		return _chartPura.getUserName();
	}

	/**
	 * Returns the user uuid of this chart pura.
	 *
	 * @return the user uuid of this chart pura
	 */
	@Override
	public String getUserUuid() {
		return _chartPura.getUserUuid();
	}

	/**
	 * Returns the uuid of this chart pura.
	 *
	 * @return the uuid of this chart pura
	 */
	@Override
	public String getUuid() {
		return _chartPura.getUuid();
	}

	@Override
	public int hashCode() {
		return _chartPura.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _chartPura.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _chartPura.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _chartPura.isNew();
	}

	@Override
	public void persist() {
		_chartPura.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_chartPura.setCachedModel(cachedModel);
	}

	/**
	 * Sets the company ID of this chart pura.
	 *
	 * @param companyId the company ID of this chart pura
	 */
	@Override
	public void setCompanyId(long companyId) {
		_chartPura.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this chart pura.
	 *
	 * @param createDate the create date of this chart pura
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_chartPura.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_chartPura.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_chartPura.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_chartPura.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the group ID of this chart pura.
	 *
	 * @param groupId the group ID of this chart pura
	 */
	@Override
	public void setGroupId(long groupId) {
		_chartPura.setGroupId(groupId);
	}

	/**
	 * Sets the ID of this chart pura.
	 *
	 * @param id the ID of this chart pura
	 */
	@Override
	public void setId(long id) {
		_chartPura.setId(id);
	}

	/**
	 * Sets the inferior of this chart pura.
	 *
	 * @param inferior the inferior of this chart pura
	 */
	@Override
	public void setInferior(double inferior) {
		_chartPura.setInferior(inferior);
	}

	/**
	 * Sets the medio of this chart pura.
	 *
	 * @param medio the medio of this chart pura
	 */
	@Override
	public void setMedio(double medio) {
		_chartPura.setMedio(medio);
	}

	/**
	 * Sets the modified date of this chart pura.
	 *
	 * @param modifiedDate the modified date of this chart pura
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_chartPura.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_chartPura.setNew(n);
	}

	/**
	 * Sets the primary key of this chart pura.
	 *
	 * @param primaryKey the primary key of this chart pura
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_chartPura.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_chartPura.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the score of this chart pura.
	 *
	 * @param score the score of this chart pura
	 */
	@Override
	public void setScore(int score) {
		_chartPura.setScore(score);
	}

	/**
	 * Sets the superior of this chart pura.
	 *
	 * @param superior the superior of this chart pura
	 */
	@Override
	public void setSuperior(double superior) {
		_chartPura.setSuperior(superior);
	}

	/**
	 * Sets the tiempo of this chart pura.
	 *
	 * @param tiempo the tiempo of this chart pura
	 */
	@Override
	public void setTiempo(int tiempo) {
		_chartPura.setTiempo(tiempo);
	}

	/**
	 * Sets the user ID of this chart pura.
	 *
	 * @param userId the user ID of this chart pura
	 */
	@Override
	public void setUserId(long userId) {
		_chartPura.setUserId(userId);
	}

	/**
	 * Sets the user name of this chart pura.
	 *
	 * @param userName the user name of this chart pura
	 */
	@Override
	public void setUserName(String userName) {
		_chartPura.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this chart pura.
	 *
	 * @param userUuid the user uuid of this chart pura
	 */
	@Override
	public void setUserUuid(String userUuid) {
		_chartPura.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this chart pura.
	 *
	 * @param uuid the uuid of this chart pura
	 */
	@Override
	public void setUuid(String uuid) {
		_chartPura.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ChartPura>
		toCacheModel() {

		return _chartPura.toCacheModel();
	}

	@Override
	public ChartPura toEscapedModel() {
		return new ChartPuraWrapper(_chartPura.toEscapedModel());
	}

	@Override
	public String toString() {
		return _chartPura.toString();
	}

	@Override
	public ChartPura toUnescapedModel() {
		return new ChartPuraWrapper(_chartPura.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _chartPura.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ChartPuraWrapper)) {
			return false;
		}

		ChartPuraWrapper chartPuraWrapper = (ChartPuraWrapper)obj;

		if (Objects.equals(_chartPura, chartPuraWrapper._chartPura)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _chartPura.getStagedModelType();
	}

	@Override
	public ChartPura getWrappedModel() {
		return _chartPura;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _chartPura.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _chartPura.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_chartPura.resetOriginalValues();
	}

	private final ChartPura _chartPura;

}