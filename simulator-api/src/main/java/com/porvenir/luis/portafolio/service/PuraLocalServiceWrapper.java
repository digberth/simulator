/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PuraLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PuraLocalService
 * @generated
 */
@ProviderType
public class PuraLocalServiceWrapper
	implements PuraLocalService, ServiceWrapper<PuraLocalService> {

	public PuraLocalServiceWrapper(PuraLocalService puraLocalService) {
		_puraLocalService = puraLocalService;
	}

	@Override
	public com.porvenir.luis.portafolio.model.Pura addPura(
			long userId, int score, String alternativa, String nombre,
			double peso,
			com.liferay.portal.kernel.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _puraLocalService.addPura(
			userId, score, alternativa, nombre, peso, context);
	}

	/**
	 * Adds the pura to the database. Also notifies the appropriate model listeners.
	 *
	 * @param pura the pura
	 * @return the pura that was added
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura addPura(
		com.porvenir.luis.portafolio.model.Pura pura) {

		return _puraLocalService.addPura(pura);
	}

	/**
	 * Creates a new pura with the primary key. Does not add the pura to the database.
	 *
	 * @param divId the primary key for the new pura
	 * @return the new pura
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura createPura(long divId) {
		return _puraLocalService.createPura(divId);
	}

	@Override
	public void deleteAll() {
		_puraLocalService.deleteAll();
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _puraLocalService.deletePersistedModel(persistedModel);
	}

	/**
	 * Deletes the pura with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param divId the primary key of the pura
	 * @return the pura that was removed
	 * @throws PortalException if a pura with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura deletePura(long divId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _puraLocalService.deletePura(divId);
	}

	/**
	 * Deletes the pura from the database. Also notifies the appropriate model listeners.
	 *
	 * @param pura the pura
	 * @return the pura that was removed
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura deletePura(
		com.porvenir.luis.portafolio.model.Pura pura) {

		return _puraLocalService.deletePura(pura);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _puraLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _puraLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.PuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _puraLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.PuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _puraLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _puraLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _puraLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.porvenir.luis.portafolio.model.Pura fetchPura(long divId) {
		return _puraLocalService.fetchPura(divId);
	}

	/**
	 * Returns the pura matching the UUID and group.
	 *
	 * @param uuid the pura's UUID
	 * @param groupId the primary key of the group
	 * @return the matching pura, or <code>null</code> if a matching pura could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura fetchPuraByUuidAndGroupId(
		String uuid, long groupId) {

		return _puraLocalService.fetchPuraByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _puraLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Pura> getByScore(
		int score) {

		return _puraLocalService.getByScore(score);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _puraLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _puraLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _puraLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _puraLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns the pura with the primary key.
	 *
	 * @param divId the primary key of the pura
	 * @return the pura
	 * @throws PortalException if a pura with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura getPura(long divId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _puraLocalService.getPura(divId);
	}

	/**
	 * Returns the pura matching the UUID and group.
	 *
	 * @param uuid the pura's UUID
	 * @param groupId the primary key of the group
	 * @return the matching pura
	 * @throws PortalException if a matching pura could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura getPuraByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _puraLocalService.getPuraByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.PuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of puras
	 * @param end the upper bound of the range of puras (not inclusive)
	 * @return the range of puras
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Pura> getPuras(
		int start, int end) {

		return _puraLocalService.getPuras(start, end);
	}

	/**
	 * Returns all the puras matching the UUID and company.
	 *
	 * @param uuid the UUID of the puras
	 * @param companyId the primary key of the company
	 * @return the matching puras, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Pura>
		getPurasByUuidAndCompanyId(String uuid, long companyId) {

		return _puraLocalService.getPurasByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of puras matching the UUID and company.
	 *
	 * @param uuid the UUID of the puras
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of puras
	 * @param end the upper bound of the range of puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching puras, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Pura>
		getPurasByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.porvenir.luis.portafolio.model.Pura> orderByComparator) {

		return _puraLocalService.getPurasByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of puras.
	 *
	 * @return the number of puras
	 */
	@Override
	public int getPurasCount() {
		return _puraLocalService.getPurasCount();
	}

	/**
	 * Updates the pura in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param pura the pura
	 * @return the pura that was updated
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Pura updatePura(
		com.porvenir.luis.portafolio.model.Pura pura) {

		return _puraLocalService.updatePura(pura);
	}

	@Override
	public PuraLocalService getWrappedService() {
		return _puraLocalService;
	}

	@Override
	public void setWrappedService(PuraLocalService puraLocalService) {
		_puraLocalService = puraLocalService;
	}

	private PuraLocalService _puraLocalService;

}