/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ChartDiversificado}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartDiversificado
 * @generated
 */
@ProviderType
public class ChartDiversificadoWrapper
	implements ChartDiversificado, ModelWrapper<ChartDiversificado> {

	public ChartDiversificadoWrapper(ChartDiversificado chartDiversificado) {
		_chartDiversificado = chartDiversificado;
	}

	@Override
	public Class<?> getModelClass() {
		return ChartDiversificado.class;
	}

	@Override
	public String getModelClassName() {
		return ChartDiversificado.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("id", getId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nombre", getNombre());
		attributes.put("tiempo", getTiempo());
		attributes.put("inferior", getInferior());
		attributes.put("medio", getMedio());
		attributes.put("superior", getSuperior());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Integer tiempo = (Integer)attributes.get("tiempo");

		if (tiempo != null) {
			setTiempo(tiempo);
		}

		Double inferior = (Double)attributes.get("inferior");

		if (inferior != null) {
			setInferior(inferior);
		}

		Double medio = (Double)attributes.get("medio");

		if (medio != null) {
			setMedio(medio);
		}

		Double superior = (Double)attributes.get("superior");

		if (superior != null) {
			setSuperior(superior);
		}
	}

	@Override
	public Object clone() {
		return new ChartDiversificadoWrapper(
			(ChartDiversificado)_chartDiversificado.clone());
	}

	@Override
	public int compareTo(ChartDiversificado chartDiversificado) {
		return _chartDiversificado.compareTo(chartDiversificado);
	}

	/**
	 * Returns the company ID of this chart diversificado.
	 *
	 * @return the company ID of this chart diversificado
	 */
	@Override
	public long getCompanyId() {
		return _chartDiversificado.getCompanyId();
	}

	/**
	 * Returns the create date of this chart diversificado.
	 *
	 * @return the create date of this chart diversificado
	 */
	@Override
	public Date getCreateDate() {
		return _chartDiversificado.getCreateDate();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _chartDiversificado.getExpandoBridge();
	}

	/**
	 * Returns the group ID of this chart diversificado.
	 *
	 * @return the group ID of this chart diversificado
	 */
	@Override
	public long getGroupId() {
		return _chartDiversificado.getGroupId();
	}

	/**
	 * Returns the ID of this chart diversificado.
	 *
	 * @return the ID of this chart diversificado
	 */
	@Override
	public long getId() {
		return _chartDiversificado.getId();
	}

	/**
	 * Returns the inferior of this chart diversificado.
	 *
	 * @return the inferior of this chart diversificado
	 */
	@Override
	public double getInferior() {
		return _chartDiversificado.getInferior();
	}

	/**
	 * Returns the medio of this chart diversificado.
	 *
	 * @return the medio of this chart diversificado
	 */
	@Override
	public double getMedio() {
		return _chartDiversificado.getMedio();
	}

	/**
	 * Returns the modified date of this chart diversificado.
	 *
	 * @return the modified date of this chart diversificado
	 */
	@Override
	public Date getModifiedDate() {
		return _chartDiversificado.getModifiedDate();
	}

	/**
	 * Returns the nombre of this chart diversificado.
	 *
	 * @return the nombre of this chart diversificado
	 */
	@Override
	public String getNombre() {
		return _chartDiversificado.getNombre();
	}

	/**
	 * Returns the primary key of this chart diversificado.
	 *
	 * @return the primary key of this chart diversificado
	 */
	@Override
	public long getPrimaryKey() {
		return _chartDiversificado.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _chartDiversificado.getPrimaryKeyObj();
	}

	/**
	 * Returns the superior of this chart diversificado.
	 *
	 * @return the superior of this chart diversificado
	 */
	@Override
	public double getSuperior() {
		return _chartDiversificado.getSuperior();
	}

	/**
	 * Returns the tiempo of this chart diversificado.
	 *
	 * @return the tiempo of this chart diversificado
	 */
	@Override
	public int getTiempo() {
		return _chartDiversificado.getTiempo();
	}

	/**
	 * Returns the user ID of this chart diversificado.
	 *
	 * @return the user ID of this chart diversificado
	 */
	@Override
	public long getUserId() {
		return _chartDiversificado.getUserId();
	}

	/**
	 * Returns the user name of this chart diversificado.
	 *
	 * @return the user name of this chart diversificado
	 */
	@Override
	public String getUserName() {
		return _chartDiversificado.getUserName();
	}

	/**
	 * Returns the user uuid of this chart diversificado.
	 *
	 * @return the user uuid of this chart diversificado
	 */
	@Override
	public String getUserUuid() {
		return _chartDiversificado.getUserUuid();
	}

	/**
	 * Returns the uuid of this chart diversificado.
	 *
	 * @return the uuid of this chart diversificado
	 */
	@Override
	public String getUuid() {
		return _chartDiversificado.getUuid();
	}

	@Override
	public int hashCode() {
		return _chartDiversificado.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _chartDiversificado.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _chartDiversificado.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _chartDiversificado.isNew();
	}

	@Override
	public void persist() {
		_chartDiversificado.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_chartDiversificado.setCachedModel(cachedModel);
	}

	/**
	 * Sets the company ID of this chart diversificado.
	 *
	 * @param companyId the company ID of this chart diversificado
	 */
	@Override
	public void setCompanyId(long companyId) {
		_chartDiversificado.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this chart diversificado.
	 *
	 * @param createDate the create date of this chart diversificado
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_chartDiversificado.setCreateDate(createDate);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_chartDiversificado.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_chartDiversificado.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_chartDiversificado.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the group ID of this chart diversificado.
	 *
	 * @param groupId the group ID of this chart diversificado
	 */
	@Override
	public void setGroupId(long groupId) {
		_chartDiversificado.setGroupId(groupId);
	}

	/**
	 * Sets the ID of this chart diversificado.
	 *
	 * @param id the ID of this chart diversificado
	 */
	@Override
	public void setId(long id) {
		_chartDiversificado.setId(id);
	}

	/**
	 * Sets the inferior of this chart diversificado.
	 *
	 * @param inferior the inferior of this chart diversificado
	 */
	@Override
	public void setInferior(double inferior) {
		_chartDiversificado.setInferior(inferior);
	}

	/**
	 * Sets the medio of this chart diversificado.
	 *
	 * @param medio the medio of this chart diversificado
	 */
	@Override
	public void setMedio(double medio) {
		_chartDiversificado.setMedio(medio);
	}

	/**
	 * Sets the modified date of this chart diversificado.
	 *
	 * @param modifiedDate the modified date of this chart diversificado
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_chartDiversificado.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_chartDiversificado.setNew(n);
	}

	/**
	 * Sets the nombre of this chart diversificado.
	 *
	 * @param nombre the nombre of this chart diversificado
	 */
	@Override
	public void setNombre(String nombre) {
		_chartDiversificado.setNombre(nombre);
	}

	/**
	 * Sets the primary key of this chart diversificado.
	 *
	 * @param primaryKey the primary key of this chart diversificado
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_chartDiversificado.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_chartDiversificado.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the superior of this chart diversificado.
	 *
	 * @param superior the superior of this chart diversificado
	 */
	@Override
	public void setSuperior(double superior) {
		_chartDiversificado.setSuperior(superior);
	}

	/**
	 * Sets the tiempo of this chart diversificado.
	 *
	 * @param tiempo the tiempo of this chart diversificado
	 */
	@Override
	public void setTiempo(int tiempo) {
		_chartDiversificado.setTiempo(tiempo);
	}

	/**
	 * Sets the user ID of this chart diversificado.
	 *
	 * @param userId the user ID of this chart diversificado
	 */
	@Override
	public void setUserId(long userId) {
		_chartDiversificado.setUserId(userId);
	}

	/**
	 * Sets the user name of this chart diversificado.
	 *
	 * @param userName the user name of this chart diversificado
	 */
	@Override
	public void setUserName(String userName) {
		_chartDiversificado.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this chart diversificado.
	 *
	 * @param userUuid the user uuid of this chart diversificado
	 */
	@Override
	public void setUserUuid(String userUuid) {
		_chartDiversificado.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this chart diversificado.
	 *
	 * @param uuid the uuid of this chart diversificado
	 */
	@Override
	public void setUuid(String uuid) {
		_chartDiversificado.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ChartDiversificado>
		toCacheModel() {

		return _chartDiversificado.toCacheModel();
	}

	@Override
	public ChartDiversificado toEscapedModel() {
		return new ChartDiversificadoWrapper(
			_chartDiversificado.toEscapedModel());
	}

	@Override
	public String toString() {
		return _chartDiversificado.toString();
	}

	@Override
	public ChartDiversificado toUnescapedModel() {
		return new ChartDiversificadoWrapper(
			_chartDiversificado.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _chartDiversificado.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ChartDiversificadoWrapper)) {
			return false;
		}

		ChartDiversificadoWrapper chartDiversificadoWrapper =
			(ChartDiversificadoWrapper)obj;

		if (Objects.equals(
				_chartDiversificado,
				chartDiversificadoWrapper._chartDiversificado)) {

			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _chartDiversificado.getStagedModelType();
	}

	@Override
	public ChartDiversificado getWrappedModel() {
		return _chartDiversificado;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _chartDiversificado.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _chartDiversificado.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_chartDiversificado.resetOriginalValues();
	}

	private final ChartDiversificado _chartDiversificado;

}