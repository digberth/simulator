/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ChartPuraLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ChartPuraLocalService
 * @generated
 */
@ProviderType
public class ChartPuraLocalServiceWrapper
	implements ChartPuraLocalService, ServiceWrapper<ChartPuraLocalService> {

	public ChartPuraLocalServiceWrapper(
		ChartPuraLocalService chartPuraLocalService) {

		_chartPuraLocalService = chartPuraLocalService;
	}

	/**
	 * Adds the chart pura to the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartPura the chart pura
	 * @return the chart pura that was added
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura addChartPura(
		com.porvenir.luis.portafolio.model.ChartPura chartPura) {

		return _chartPuraLocalService.addChartPura(chartPura);
	}

	@Override
	public com.porvenir.luis.portafolio.model.ChartPura addChartPura(
			long userId, int score, int tiempo, double inferior, double medio,
			double superior,
			com.liferay.portal.kernel.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartPuraLocalService.addChartPura(
			userId, score, tiempo, inferior, medio, superior, context);
	}

	/**
	 * Creates a new chart pura with the primary key. Does not add the chart pura to the database.
	 *
	 * @param id the primary key for the new chart pura
	 * @return the new chart pura
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura createChartPura(
		long id) {

		return _chartPuraLocalService.createChartPura(id);
	}

	@Override
	public void deleteAll() {
		_chartPuraLocalService.deleteAll();
	}

	/**
	 * Deletes the chart pura from the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartPura the chart pura
	 * @return the chart pura that was removed
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura deleteChartPura(
		com.porvenir.luis.portafolio.model.ChartPura chartPura) {

		return _chartPuraLocalService.deleteChartPura(chartPura);
	}

	/**
	 * Deletes the chart pura with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura that was removed
	 * @throws PortalException if a chart pura with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura deleteChartPura(long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartPuraLocalService.deleteChartPura(id);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartPuraLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _chartPuraLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _chartPuraLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _chartPuraLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _chartPuraLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _chartPuraLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _chartPuraLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.porvenir.luis.portafolio.model.ChartPura fetchChartPura(
		long id) {

		return _chartPuraLocalService.fetchChartPura(id);
	}

	/**
	 * Returns the chart pura matching the UUID and group.
	 *
	 * @param uuid the chart pura's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura
		fetchChartPuraByUuidAndGroupId(String uuid, long groupId) {

		return _chartPuraLocalService.fetchChartPuraByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.porvenir.luis.portafolio.model.ChartPura findByScore_Time(
			int score, int time)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return _chartPuraLocalService.findByScore_Time(score, time);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _chartPuraLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getByScore(int score, int max) {

		return _chartPuraLocalService.getByScore(score, max);
	}

	/**
	 * Returns the chart pura with the primary key.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura
	 * @throws PortalException if a chart pura with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura getChartPura(long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartPuraLocalService.getChartPura(id);
	}

	/**
	 * Returns the chart pura matching the UUID and group.
	 *
	 * @param uuid the chart pura's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart pura
	 * @throws PortalException if a matching chart pura could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura
			getChartPuraByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartPuraLocalService.getChartPuraByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of chart puras
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getChartPuras(int start, int end) {

		return _chartPuraLocalService.getChartPuras(start, end);
	}

	/**
	 * Returns all the chart puras matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart puras
	 * @param companyId the primary key of the company
	 * @return the matching chart puras, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getChartPurasByUuidAndCompanyId(String uuid, long companyId) {

		return _chartPuraLocalService.getChartPurasByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of chart puras matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart puras
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching chart puras, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getChartPurasByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.porvenir.luis.portafolio.model.ChartPura>
					orderByComparator) {

		return _chartPuraLocalService.getChartPurasByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of chart puras.
	 *
	 * @return the number of chart puras
	 */
	@Override
	public int getChartPurasCount() {
		return _chartPuraLocalService.getChartPurasCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _chartPuraLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _chartPuraLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _chartPuraLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartPuraLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the chart pura in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param chartPura the chart pura
	 * @return the chart pura that was updated
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartPura updateChartPura(
		com.porvenir.luis.portafolio.model.ChartPura chartPura) {

		return _chartPuraLocalService.updateChartPura(chartPura);
	}

	@Override
	public ChartPuraLocalService getWrappedService() {
		return _chartPuraLocalService;
	}

	@Override
	public void setWrappedService(ChartPuraLocalService chartPuraLocalService) {
		_chartPuraLocalService = chartPuraLocalService;
	}

	private ChartPuraLocalService _chartPuraLocalService;

}