/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.porvenir.luis.portafolio.model.ChartDiversificado;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the chart diversificado service. This utility wraps <code>com.porvenir.luis.portafolio.service.persistence.impl.ChartDiversificadoPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartDiversificadoPersistence
 * @generated
 */
@ProviderType
public class ChartDiversificadoUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ChartDiversificado chartDiversificado) {
		getPersistence().clearCache(chartDiversificado);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, ChartDiversificado> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ChartDiversificado> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ChartDiversificado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ChartDiversificado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ChartDiversificado update(
		ChartDiversificado chartDiversificado) {

		return getPersistence().update(chartDiversificado);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ChartDiversificado update(
		ChartDiversificado chartDiversificado, ServiceContext serviceContext) {

		return getPersistence().update(chartDiversificado, serviceContext);
	}

	/**
	 * Returns all the chart diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByUuid_First(
			String uuid,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByUuid_First(
		String uuid, OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByUuid_Last(
			String uuid,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByUuid_Last(
		String uuid, OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public static ChartDiversificado[] findByUuid_PrevAndNext(
			long id, String uuid,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByUuid_PrevAndNext(
			id, uuid, orderByComparator);
	}

	/**
	 * Removes all the chart diversificados where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of chart diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching chart diversificados
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByUUID_G(String uuid, long groupId)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	 * Removes the chart diversificado where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the chart diversificado that was removed
	 */
	public static ChartDiversificado removeByUUID_G(String uuid, long groupId)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of chart diversificados where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching chart diversificados
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public static ChartDiversificado[] findByUuid_C_PrevAndNext(
			long id, String uuid, long companyId,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByUuid_C_PrevAndNext(
			id, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the chart diversificados where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching chart diversificados
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByNombre_Tiempo(
			String nombre, int tiempo)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByNombre_Tiempo(nombre, tiempo);
	}

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByNombre_Tiempo(
		String nombre, int tiempo) {

		return getPersistence().fetchByNombre_Tiempo(nombre, tiempo);
	}

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByNombre_Tiempo(
		String nombre, int tiempo, boolean retrieveFromCache) {

		return getPersistence().fetchByNombre_Tiempo(
			nombre, tiempo, retrieveFromCache);
	}

	/**
	 * Removes the chart diversificado where nombre = &#63; and tiempo = &#63; from the database.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the chart diversificado that was removed
	 */
	public static ChartDiversificado removeByNombre_Tiempo(
			String nombre, int tiempo)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().removeByNombre_Tiempo(nombre, tiempo);
	}

	/**
	 * Returns the number of chart diversificados where nombre = &#63; and tiempo = &#63;.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the number of matching chart diversificados
	 */
	public static int countByNombre_Tiempo(String nombre, int tiempo) {
		return getPersistence().countByNombre_Tiempo(nombre, tiempo);
	}

	/**
	 * Returns all the chart diversificados where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching chart diversificados
	 */
	public static List<ChartDiversificado> findByNombre(String nombre) {
		return getPersistence().findByNombre(nombre);
	}

	/**
	 * Returns a range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByNombre(
		String nombre, int start, int end) {

		return getPersistence().findByNombre(nombre, start, end);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByNombre(
		String nombre, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().findByNombre(
			nombre, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	public static List<ChartDiversificado> findByNombre(
		String nombre, int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByNombre(
			nombre, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByNombre_First(
			String nombre,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByNombre_First(nombre, orderByComparator);
	}

	/**
	 * Returns the first chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByNombre_First(
		String nombre,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().fetchByNombre_First(nombre, orderByComparator);
	}

	/**
	 * Returns the last chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado findByNombre_Last(
			String nombre,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByNombre_Last(nombre, orderByComparator);
	}

	/**
	 * Returns the last chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static ChartDiversificado fetchByNombre_Last(
		String nombre,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().fetchByNombre_Last(nombre, orderByComparator);
	}

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public static ChartDiversificado[] findByNombre_PrevAndNext(
			long id, String nombre,
			OrderByComparator<ChartDiversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByNombre_PrevAndNext(
			id, nombre, orderByComparator);
	}

	/**
	 * Removes all the chart diversificados where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 */
	public static void removeByNombre(String nombre) {
		getPersistence().removeByNombre(nombre);
	}

	/**
	 * Returns the number of chart diversificados where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching chart diversificados
	 */
	public static int countByNombre(String nombre) {
		return getPersistence().countByNombre(nombre);
	}

	/**
	 * Caches the chart diversificado in the entity cache if it is enabled.
	 *
	 * @param chartDiversificado the chart diversificado
	 */
	public static void cacheResult(ChartDiversificado chartDiversificado) {
		getPersistence().cacheResult(chartDiversificado);
	}

	/**
	 * Caches the chart diversificados in the entity cache if it is enabled.
	 *
	 * @param chartDiversificados the chart diversificados
	 */
	public static void cacheResult(
		List<ChartDiversificado> chartDiversificados) {

		getPersistence().cacheResult(chartDiversificados);
	}

	/**
	 * Creates a new chart diversificado with the primary key. Does not add the chart diversificado to the database.
	 *
	 * @param id the primary key for the new chart diversificado
	 * @return the new chart diversificado
	 */
	public static ChartDiversificado create(long id) {
		return getPersistence().create(id);
	}

	/**
	 * Removes the chart diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado that was removed
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public static ChartDiversificado remove(long id)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().remove(id);
	}

	public static ChartDiversificado updateImpl(
		ChartDiversificado chartDiversificado) {

		return getPersistence().updateImpl(chartDiversificado);
	}

	/**
	 * Returns the chart diversificado with the primary key or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public static ChartDiversificado findByPrimaryKey(long id)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getPersistence().findByPrimaryKey(id);
	}

	/**
	 * Returns the chart diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado, or <code>null</code> if a chart diversificado with the primary key could not be found
	 */
	public static ChartDiversificado fetchByPrimaryKey(long id) {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	 * Returns all the chart diversificados.
	 *
	 * @return the chart diversificados
	 */
	public static List<ChartDiversificado> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of chart diversificados
	 */
	public static List<ChartDiversificado> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of chart diversificados
	 */
	public static List<ChartDiversificado> findAll(
		int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of chart diversificados
	 */
	public static List<ChartDiversificado> findAll(
		int start, int end,
		OrderByComparator<ChartDiversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Removes all the chart diversificados from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of chart diversificados.
	 *
	 * @return the number of chart diversificados
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ChartDiversificadoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<ChartDiversificadoPersistence, ChartDiversificadoPersistence>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			ChartDiversificadoPersistence.class);

		ServiceTracker
			<ChartDiversificadoPersistence, ChartDiversificadoPersistence>
				serviceTracker =
					new ServiceTracker
						<ChartDiversificadoPersistence,
						 ChartDiversificadoPersistence>(
							 bundle.getBundleContext(),
							 ChartDiversificadoPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}