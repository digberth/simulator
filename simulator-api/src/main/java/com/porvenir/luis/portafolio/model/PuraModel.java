/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.GroupedModel;
import com.liferay.portal.kernel.model.ShardedModel;
import com.liferay.portal.kernel.model.StagedAuditedModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Pura service. Represents a row in the &quot;PORTAFOLIO_Pura&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation <code>com.porvenir.luis.portafolio.model.impl.PuraModelImpl</code> exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in <code>com.porvenir.luis.portafolio.model.impl.PuraImpl</code>.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Pura
 * @generated
 */
@ProviderType
public interface PuraModel
	extends BaseModel<Pura>, GroupedModel, ShardedModel, StagedAuditedModel {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a pura model instance should use the {@link Pura} interface instead.
	 */

	/**
	 * Returns the primary key of this pura.
	 *
	 * @return the primary key of this pura
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this pura.
	 *
	 * @param primaryKey the primary key of this pura
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the uuid of this pura.
	 *
	 * @return the uuid of this pura
	 */
	@AutoEscape
	@Override
	public String getUuid();

	/**
	 * Sets the uuid of this pura.
	 *
	 * @param uuid the uuid of this pura
	 */
	@Override
	public void setUuid(String uuid);

	/**
	 * Returns the div ID of this pura.
	 *
	 * @return the div ID of this pura
	 */
	public long getDivId();

	/**
	 * Sets the div ID of this pura.
	 *
	 * @param divId the div ID of this pura
	 */
	public void setDivId(long divId);

	/**
	 * Returns the group ID of this pura.
	 *
	 * @return the group ID of this pura
	 */
	@Override
	public long getGroupId();

	/**
	 * Sets the group ID of this pura.
	 *
	 * @param groupId the group ID of this pura
	 */
	@Override
	public void setGroupId(long groupId);

	/**
	 * Returns the company ID of this pura.
	 *
	 * @return the company ID of this pura
	 */
	@Override
	public long getCompanyId();

	/**
	 * Sets the company ID of this pura.
	 *
	 * @param companyId the company ID of this pura
	 */
	@Override
	public void setCompanyId(long companyId);

	/**
	 * Returns the user ID of this pura.
	 *
	 * @return the user ID of this pura
	 */
	@Override
	public long getUserId();

	/**
	 * Sets the user ID of this pura.
	 *
	 * @param userId the user ID of this pura
	 */
	@Override
	public void setUserId(long userId);

	/**
	 * Returns the user uuid of this pura.
	 *
	 * @return the user uuid of this pura
	 */
	@Override
	public String getUserUuid();

	/**
	 * Sets the user uuid of this pura.
	 *
	 * @param userUuid the user uuid of this pura
	 */
	@Override
	public void setUserUuid(String userUuid);

	/**
	 * Returns the user name of this pura.
	 *
	 * @return the user name of this pura
	 */
	@AutoEscape
	@Override
	public String getUserName();

	/**
	 * Sets the user name of this pura.
	 *
	 * @param userName the user name of this pura
	 */
	@Override
	public void setUserName(String userName);

	/**
	 * Returns the create date of this pura.
	 *
	 * @return the create date of this pura
	 */
	@Override
	public Date getCreateDate();

	/**
	 * Sets the create date of this pura.
	 *
	 * @param createDate the create date of this pura
	 */
	@Override
	public void setCreateDate(Date createDate);

	/**
	 * Returns the modified date of this pura.
	 *
	 * @return the modified date of this pura
	 */
	@Override
	public Date getModifiedDate();

	/**
	 * Sets the modified date of this pura.
	 *
	 * @param modifiedDate the modified date of this pura
	 */
	@Override
	public void setModifiedDate(Date modifiedDate);

	/**
	 * Returns the score of this pura.
	 *
	 * @return the score of this pura
	 */
	public int getScore();

	/**
	 * Sets the score of this pura.
	 *
	 * @param score the score of this pura
	 */
	public void setScore(int score);

	/**
	 * Returns the alternativa of this pura.
	 *
	 * @return the alternativa of this pura
	 */
	@AutoEscape
	public String getAlternativa();

	/**
	 * Sets the alternativa of this pura.
	 *
	 * @param alternativa the alternativa of this pura
	 */
	public void setAlternativa(String alternativa);

	/**
	 * Returns the nombre of this pura.
	 *
	 * @return the nombre of this pura
	 */
	@AutoEscape
	public String getNombre();

	/**
	 * Sets the nombre of this pura.
	 *
	 * @param nombre the nombre of this pura
	 */
	public void setNombre(String nombre);

	/**
	 * Returns the peso of this pura.
	 *
	 * @return the peso of this pura
	 */
	public double getPeso();

	/**
	 * Sets the peso of this pura.
	 *
	 * @param peso the peso of this pura
	 */
	public void setPeso(double peso);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(Pura pura);

	@Override
	public int hashCode();

	@Override
	public CacheModel<Pura> toCacheModel();

	@Override
	public Pura toEscapedModel();

	@Override
	public Pura toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();

}