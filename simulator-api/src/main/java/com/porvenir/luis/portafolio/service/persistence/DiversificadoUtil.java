/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.porvenir.luis.portafolio.model.Diversificado;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the diversificado service. This utility wraps <code>com.porvenir.luis.portafolio.service.persistence.impl.DiversificadoPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DiversificadoPersistence
 * @generated
 */
@ProviderType
public class DiversificadoUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Diversificado diversificado) {
		getPersistence().clearCache(diversificado);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Diversificado> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Diversificado> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Diversificado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Diversificado> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Diversificado update(Diversificado diversificado) {
		return getPersistence().update(diversificado);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Diversificado update(
		Diversificado diversificado, ServiceContext serviceContext) {

		return getPersistence().update(diversificado, serviceContext);
	}

	/**
	 * Returns all the diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching diversificados
	 */
	public static List<Diversificado> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	public static List<Diversificado> findByUuid(
		String uuid, int start, int end) {

		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	public static List<Diversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	public static List<Diversificado> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public static Diversificado findByUuid_First(
			String uuid, OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByUuid_First(
		String uuid, OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public static Diversificado findByUuid_Last(
			String uuid, OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByUuid_Last(
		String uuid, OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public static Diversificado[] findByUuid_PrevAndNext(
			long divId, String uuid,
			OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByUuid_PrevAndNext(
			divId, uuid, orderByComparator);
	}

	/**
	 * Removes all the diversificados where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching diversificados
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDiversificadoException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public static Diversificado findByUUID_G(String uuid, long groupId)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	 * Removes the diversificado where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the diversificado that was removed
	 */
	public static Diversificado removeByUUID_G(String uuid, long groupId)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of diversificados where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching diversificados
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching diversificados
	 */
	public static List<Diversificado> findByUuid_C(
		String uuid, long companyId) {

		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	public static List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	public static List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	public static List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public static Diversificado findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public static Diversificado findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public static Diversificado[] findByUuid_C_PrevAndNext(
			long divId, String uuid, long companyId,
			OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByUuid_C_PrevAndNext(
			divId, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the diversificados where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching diversificados
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns all the diversificados where score = &#63;.
	 *
	 * @param score the score
	 * @return the matching diversificados
	 */
	public static List<Diversificado> findByScore(int score) {
		return getPersistence().findByScore(score);
	}

	/**
	 * Returns a range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	public static List<Diversificado> findByScore(
		int score, int start, int end) {

		return getPersistence().findByScore(score, start, end);
	}

	/**
	 * Returns an ordered range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	public static List<Diversificado> findByScore(
		int score, int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().findByScore(
			score, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	public static List<Diversificado> findByScore(
		int score, int start, int end,
		OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByScore(
			score, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public static Diversificado findByScore_First(
			int score, OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByScore_First(score, orderByComparator);
	}

	/**
	 * Returns the first diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByScore_First(
		int score, OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().fetchByScore_First(score, orderByComparator);
	}

	/**
	 * Returns the last diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public static Diversificado findByScore_Last(
			int score, OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByScore_Last(score, orderByComparator);
	}

	/**
	 * Returns the last diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public static Diversificado fetchByScore_Last(
		int score, OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().fetchByScore_Last(score, orderByComparator);
	}

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where score = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public static Diversificado[] findByScore_PrevAndNext(
			long divId, int score,
			OrderByComparator<Diversificado> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByScore_PrevAndNext(
			divId, score, orderByComparator);
	}

	/**
	 * Removes all the diversificados where score = &#63; from the database.
	 *
	 * @param score the score
	 */
	public static void removeByScore(int score) {
		getPersistence().removeByScore(score);
	}

	/**
	 * Returns the number of diversificados where score = &#63;.
	 *
	 * @param score the score
	 * @return the number of matching diversificados
	 */
	public static int countByScore(int score) {
		return getPersistence().countByScore(score);
	}

	/**
	 * Caches the diversificado in the entity cache if it is enabled.
	 *
	 * @param diversificado the diversificado
	 */
	public static void cacheResult(Diversificado diversificado) {
		getPersistence().cacheResult(diversificado);
	}

	/**
	 * Caches the diversificados in the entity cache if it is enabled.
	 *
	 * @param diversificados the diversificados
	 */
	public static void cacheResult(List<Diversificado> diversificados) {
		getPersistence().cacheResult(diversificados);
	}

	/**
	 * Creates a new diversificado with the primary key. Does not add the diversificado to the database.
	 *
	 * @param divId the primary key for the new diversificado
	 * @return the new diversificado
	 */
	public static Diversificado create(long divId) {
		return getPersistence().create(divId);
	}

	/**
	 * Removes the diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado that was removed
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public static Diversificado remove(long divId)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().remove(divId);
	}

	public static Diversificado updateImpl(Diversificado diversificado) {
		return getPersistence().updateImpl(diversificado);
	}

	/**
	 * Returns the diversificado with the primary key or throws a <code>NoSuchDiversificadoException</code> if it could not be found.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public static Diversificado findByPrimaryKey(long divId)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchDiversificadoException {

		return getPersistence().findByPrimaryKey(divId);
	}

	/**
	 * Returns the diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado, or <code>null</code> if a diversificado with the primary key could not be found
	 */
	public static Diversificado fetchByPrimaryKey(long divId) {
		return getPersistence().fetchByPrimaryKey(divId);
	}

	/**
	 * Returns all the diversificados.
	 *
	 * @return the diversificados
	 */
	public static List<Diversificado> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of diversificados
	 */
	public static List<Diversificado> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of diversificados
	 */
	public static List<Diversificado> findAll(
		int start, int end,
		OrderByComparator<Diversificado> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of diversificados
	 */
	public static List<Diversificado> findAll(
		int start, int end, OrderByComparator<Diversificado> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Removes all the diversificados from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of diversificados.
	 *
	 * @return the number of diversificados
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static DiversificadoPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<DiversificadoPersistence, DiversificadoPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(DiversificadoPersistence.class);

		ServiceTracker<DiversificadoPersistence, DiversificadoPersistence>
			serviceTracker =
				new ServiceTracker
					<DiversificadoPersistence, DiversificadoPersistence>(
						bundle.getBundleContext(),
						DiversificadoPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}