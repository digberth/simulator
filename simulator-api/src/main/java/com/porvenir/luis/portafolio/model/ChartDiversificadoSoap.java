/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ChartDiversificadoSoap implements Serializable {

	public static ChartDiversificadoSoap toSoapModel(ChartDiversificado model) {
		ChartDiversificadoSoap soapModel = new ChartDiversificadoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setId(model.getId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNombre(model.getNombre());
		soapModel.setTiempo(model.getTiempo());
		soapModel.setInferior(model.getInferior());
		soapModel.setMedio(model.getMedio());
		soapModel.setSuperior(model.getSuperior());

		return soapModel;
	}

	public static ChartDiversificadoSoap[] toSoapModels(
		ChartDiversificado[] models) {

		ChartDiversificadoSoap[] soapModels =
			new ChartDiversificadoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ChartDiversificadoSoap[][] toSoapModels(
		ChartDiversificado[][] models) {

		ChartDiversificadoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels =
				new ChartDiversificadoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ChartDiversificadoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ChartDiversificadoSoap[] toSoapModels(
		List<ChartDiversificado> models) {

		List<ChartDiversificadoSoap> soapModels =
			new ArrayList<ChartDiversificadoSoap>(models.size());

		for (ChartDiversificado model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(
			new ChartDiversificadoSoap[soapModels.size()]);
	}

	public ChartDiversificadoSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public int getTiempo() {
		return _tiempo;
	}

	public void setTiempo(int tiempo) {
		_tiempo = tiempo;
	}

	public double getInferior() {
		return _inferior;
	}

	public void setInferior(double inferior) {
		_inferior = inferior;
	}

	public double getMedio() {
		return _medio;
	}

	public void setMedio(double medio) {
		_medio = medio;
	}

	public double getSuperior() {
		return _superior;
	}

	public void setSuperior(double superior) {
		_superior = superior;
	}

	private String _uuid;
	private long _id;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nombre;
	private int _tiempo;
	private double _inferior;
	private double _medio;
	private double _superior;

}