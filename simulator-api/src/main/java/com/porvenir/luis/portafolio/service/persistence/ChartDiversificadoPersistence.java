/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.porvenir.luis.portafolio.exception.NoSuchChartDiversificadoException;
import com.porvenir.luis.portafolio.model.ChartDiversificado;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the chart diversificado service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartDiversificadoUtil
 * @generated
 */
@ProviderType
public interface ChartDiversificadoPersistence
	extends BasePersistence<ChartDiversificado> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ChartDiversificadoUtil} to access the chart diversificado persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, ChartDiversificado> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the chart diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid(String uuid);

	/**
	 * Returns a range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public ChartDiversificado[] findByUuid_PrevAndNext(
			long id, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Removes all the chart diversificados where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of chart diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching chart diversificados
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByUUID_G(String uuid, long groupId)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the chart diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache);

	/**
	 * Removes the chart diversificado where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the chart diversificado that was removed
	 */
	public ChartDiversificado removeByUUID_G(String uuid, long groupId)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the number of chart diversificados where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching chart diversificados
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the first chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the last chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public ChartDiversificado[] findByUuid_C_PrevAndNext(
			long id, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Removes all the chart diversificados where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of chart diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching chart diversificados
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByNombre_Tiempo(String nombre, int tiempo)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByNombre_Tiempo(String nombre, int tiempo);

	/**
	 * Returns the chart diversificado where nombre = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByNombre_Tiempo(
		String nombre, int tiempo, boolean retrieveFromCache);

	/**
	 * Removes the chart diversificado where nombre = &#63; and tiempo = &#63; from the database.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the chart diversificado that was removed
	 */
	public ChartDiversificado removeByNombre_Tiempo(String nombre, int tiempo)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the number of chart diversificados where nombre = &#63; and tiempo = &#63;.
	 *
	 * @param nombre the nombre
	 * @param tiempo the tiempo
	 * @return the number of matching chart diversificados
	 */
	public int countByNombre_Tiempo(String nombre, int tiempo);

	/**
	 * Returns all the chart diversificados where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByNombre(String nombre);

	/**
	 * Returns a range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByNombre(
		String nombre, int start, int end);

	/**
	 * Returns an ordered range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByNombre(
		String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart diversificados where nombre = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param nombre the nombre
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart diversificados
	 */
	public java.util.List<ChartDiversificado> findByNombre(
		String nombre, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByNombre_First(
			String nombre,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the first chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByNombre_First(
		String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns the last chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado
	 * @throws NoSuchChartDiversificadoException if a matching chart diversificado could not be found
	 */
	public ChartDiversificado findByNombre_Last(
			String nombre,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the last chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public ChartDiversificado fetchByNombre_Last(
		String nombre,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns the chart diversificados before and after the current chart diversificado in the ordered set where nombre = &#63;.
	 *
	 * @param id the primary key of the current chart diversificado
	 * @param nombre the nombre
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public ChartDiversificado[] findByNombre_PrevAndNext(
			long id, String nombre,
			com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
				orderByComparator)
		throws NoSuchChartDiversificadoException;

	/**
	 * Removes all the chart diversificados where nombre = &#63; from the database.
	 *
	 * @param nombre the nombre
	 */
	public void removeByNombre(String nombre);

	/**
	 * Returns the number of chart diversificados where nombre = &#63;.
	 *
	 * @param nombre the nombre
	 * @return the number of matching chart diversificados
	 */
	public int countByNombre(String nombre);

	/**
	 * Caches the chart diversificado in the entity cache if it is enabled.
	 *
	 * @param chartDiversificado the chart diversificado
	 */
	public void cacheResult(ChartDiversificado chartDiversificado);

	/**
	 * Caches the chart diversificados in the entity cache if it is enabled.
	 *
	 * @param chartDiversificados the chart diversificados
	 */
	public void cacheResult(
		java.util.List<ChartDiversificado> chartDiversificados);

	/**
	 * Creates a new chart diversificado with the primary key. Does not add the chart diversificado to the database.
	 *
	 * @param id the primary key for the new chart diversificado
	 * @return the new chart diversificado
	 */
	public ChartDiversificado create(long id);

	/**
	 * Removes the chart diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado that was removed
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public ChartDiversificado remove(long id)
		throws NoSuchChartDiversificadoException;

	public ChartDiversificado updateImpl(ChartDiversificado chartDiversificado);

	/**
	 * Returns the chart diversificado with the primary key or throws a <code>NoSuchChartDiversificadoException</code> if it could not be found.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado
	 * @throws NoSuchChartDiversificadoException if a chart diversificado with the primary key could not be found
	 */
	public ChartDiversificado findByPrimaryKey(long id)
		throws NoSuchChartDiversificadoException;

	/**
	 * Returns the chart diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado, or <code>null</code> if a chart diversificado with the primary key could not be found
	 */
	public ChartDiversificado fetchByPrimaryKey(long id);

	/**
	 * Returns all the chart diversificados.
	 *
	 * @return the chart diversificados
	 */
	public java.util.List<ChartDiversificado> findAll();

	/**
	 * Returns a range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of chart diversificados
	 */
	public java.util.List<ChartDiversificado> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of chart diversificados
	 */
	public java.util.List<ChartDiversificado> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of chart diversificados
	 */
	public java.util.List<ChartDiversificado> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartDiversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the chart diversificados from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of chart diversificados.
	 *
	 * @return the number of chart diversificados
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}