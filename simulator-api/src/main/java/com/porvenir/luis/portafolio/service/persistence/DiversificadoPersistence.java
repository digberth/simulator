/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.porvenir.luis.portafolio.exception.NoSuchDiversificadoException;
import com.porvenir.luis.portafolio.model.Diversificado;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the diversificado service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DiversificadoUtil
 * @generated
 */
@ProviderType
public interface DiversificadoPersistence
	extends BasePersistence<Diversificado> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DiversificadoUtil} to access the diversificado persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, Diversificado> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid(String uuid);

	/**
	 * Returns a range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public Diversificado findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public Diversificado findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where uuid = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public Diversificado[] findByUuid_PrevAndNext(
			long divId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Removes all the diversificados where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of diversificados where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching diversificados
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDiversificadoException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public Diversificado findByUUID_G(String uuid, long groupId)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the diversificado where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache);

	/**
	 * Removes the diversificado where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the diversificado that was removed
	 */
	public Diversificado removeByUUID_G(String uuid, long groupId)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the number of diversificados where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching diversificados
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid_C(
		String uuid, long companyId);

	/**
	 * Returns a range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	public java.util.List<Diversificado> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public Diversificado findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the first diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public Diversificado findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the last diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public Diversificado[] findByUuid_C_PrevAndNext(
			long divId, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Removes all the diversificados where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of diversificados where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching diversificados
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns all the diversificados where score = &#63;.
	 *
	 * @param score the score
	 * @return the matching diversificados
	 */
	public java.util.List<Diversificado> findByScore(int score);

	/**
	 * Returns a range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of matching diversificados
	 */
	public java.util.List<Diversificado> findByScore(
		int score, int start, int end);

	/**
	 * Returns an ordered range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching diversificados
	 */
	public java.util.List<Diversificado> findByScore(
		int score, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the diversificados where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching diversificados
	 */
	public java.util.List<Diversificado> findByScore(
		int score, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public Diversificado findByScore_First(
			int score,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the first diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByScore_First(
		int score,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns the last diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado
	 * @throws NoSuchDiversificadoException if a matching diversificado could not be found
	 */
	public Diversificado findByScore_Last(
			int score,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the last diversificado in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	public Diversificado fetchByScore_Last(
		int score,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns the diversificados before and after the current diversificado in the ordered set where score = &#63;.
	 *
	 * @param divId the primary key of the current diversificado
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public Diversificado[] findByScore_PrevAndNext(
			long divId, int score,
			com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
				orderByComparator)
		throws NoSuchDiversificadoException;

	/**
	 * Removes all the diversificados where score = &#63; from the database.
	 *
	 * @param score the score
	 */
	public void removeByScore(int score);

	/**
	 * Returns the number of diversificados where score = &#63;.
	 *
	 * @param score the score
	 * @return the number of matching diversificados
	 */
	public int countByScore(int score);

	/**
	 * Caches the diversificado in the entity cache if it is enabled.
	 *
	 * @param diversificado the diversificado
	 */
	public void cacheResult(Diversificado diversificado);

	/**
	 * Caches the diversificados in the entity cache if it is enabled.
	 *
	 * @param diversificados the diversificados
	 */
	public void cacheResult(java.util.List<Diversificado> diversificados);

	/**
	 * Creates a new diversificado with the primary key. Does not add the diversificado to the database.
	 *
	 * @param divId the primary key for the new diversificado
	 * @return the new diversificado
	 */
	public Diversificado create(long divId);

	/**
	 * Removes the diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado that was removed
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public Diversificado remove(long divId) throws NoSuchDiversificadoException;

	public Diversificado updateImpl(Diversificado diversificado);

	/**
	 * Returns the diversificado with the primary key or throws a <code>NoSuchDiversificadoException</code> if it could not be found.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado
	 * @throws NoSuchDiversificadoException if a diversificado with the primary key could not be found
	 */
	public Diversificado findByPrimaryKey(long divId)
		throws NoSuchDiversificadoException;

	/**
	 * Returns the diversificado with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado, or <code>null</code> if a diversificado with the primary key could not be found
	 */
	public Diversificado fetchByPrimaryKey(long divId);

	/**
	 * Returns all the diversificados.
	 *
	 * @return the diversificados
	 */
	public java.util.List<Diversificado> findAll();

	/**
	 * Returns a range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of diversificados
	 */
	public java.util.List<Diversificado> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of diversificados
	 */
	public java.util.List<Diversificado> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator);

	/**
	 * Returns an ordered range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of diversificados
	 */
	public java.util.List<Diversificado> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Diversificado>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the diversificados from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of diversificados.
	 *
	 * @return the number of diversificados
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}