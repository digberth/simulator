/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Pura}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Pura
 * @generated
 */
@ProviderType
public class PuraWrapper implements Pura, ModelWrapper<Pura> {

	public PuraWrapper(Pura pura) {
		_pura = pura;
	}

	@Override
	public Class<?> getModelClass() {
		return Pura.class;
	}

	@Override
	public String getModelClassName() {
		return Pura.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("divId", getDivId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("score", getScore());
		attributes.put("alternativa", getAlternativa());
		attributes.put("nombre", getNombre());
		attributes.put("peso", getPeso());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long divId = (Long)attributes.get("divId");

		if (divId != null) {
			setDivId(divId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Integer score = (Integer)attributes.get("score");

		if (score != null) {
			setScore(score);
		}

		String alternativa = (String)attributes.get("alternativa");

		if (alternativa != null) {
			setAlternativa(alternativa);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Double peso = (Double)attributes.get("peso");

		if (peso != null) {
			setPeso(peso);
		}
	}

	@Override
	public Object clone() {
		return new PuraWrapper((Pura)_pura.clone());
	}

	@Override
	public int compareTo(Pura pura) {
		return _pura.compareTo(pura);
	}

	/**
	 * Returns the alternativa of this pura.
	 *
	 * @return the alternativa of this pura
	 */
	@Override
	public String getAlternativa() {
		return _pura.getAlternativa();
	}

	/**
	 * Returns the company ID of this pura.
	 *
	 * @return the company ID of this pura
	 */
	@Override
	public long getCompanyId() {
		return _pura.getCompanyId();
	}

	/**
	 * Returns the create date of this pura.
	 *
	 * @return the create date of this pura
	 */
	@Override
	public Date getCreateDate() {
		return _pura.getCreateDate();
	}

	/**
	 * Returns the div ID of this pura.
	 *
	 * @return the div ID of this pura
	 */
	@Override
	public long getDivId() {
		return _pura.getDivId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _pura.getExpandoBridge();
	}

	/**
	 * Returns the group ID of this pura.
	 *
	 * @return the group ID of this pura
	 */
	@Override
	public long getGroupId() {
		return _pura.getGroupId();
	}

	/**
	 * Returns the modified date of this pura.
	 *
	 * @return the modified date of this pura
	 */
	@Override
	public Date getModifiedDate() {
		return _pura.getModifiedDate();
	}

	/**
	 * Returns the nombre of this pura.
	 *
	 * @return the nombre of this pura
	 */
	@Override
	public String getNombre() {
		return _pura.getNombre();
	}

	/**
	 * Returns the peso of this pura.
	 *
	 * @return the peso of this pura
	 */
	@Override
	public double getPeso() {
		return _pura.getPeso();
	}

	/**
	 * Returns the primary key of this pura.
	 *
	 * @return the primary key of this pura
	 */
	@Override
	public long getPrimaryKey() {
		return _pura.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _pura.getPrimaryKeyObj();
	}

	/**
	 * Returns the score of this pura.
	 *
	 * @return the score of this pura
	 */
	@Override
	public int getScore() {
		return _pura.getScore();
	}

	/**
	 * Returns the user ID of this pura.
	 *
	 * @return the user ID of this pura
	 */
	@Override
	public long getUserId() {
		return _pura.getUserId();
	}

	/**
	 * Returns the user name of this pura.
	 *
	 * @return the user name of this pura
	 */
	@Override
	public String getUserName() {
		return _pura.getUserName();
	}

	/**
	 * Returns the user uuid of this pura.
	 *
	 * @return the user uuid of this pura
	 */
	@Override
	public String getUserUuid() {
		return _pura.getUserUuid();
	}

	/**
	 * Returns the uuid of this pura.
	 *
	 * @return the uuid of this pura
	 */
	@Override
	public String getUuid() {
		return _pura.getUuid();
	}

	@Override
	public int hashCode() {
		return _pura.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _pura.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _pura.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _pura.isNew();
	}

	@Override
	public void persist() {
		_pura.persist();
	}

	/**
	 * Sets the alternativa of this pura.
	 *
	 * @param alternativa the alternativa of this pura
	 */
	@Override
	public void setAlternativa(String alternativa) {
		_pura.setAlternativa(alternativa);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_pura.setCachedModel(cachedModel);
	}

	/**
	 * Sets the company ID of this pura.
	 *
	 * @param companyId the company ID of this pura
	 */
	@Override
	public void setCompanyId(long companyId) {
		_pura.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this pura.
	 *
	 * @param createDate the create date of this pura
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_pura.setCreateDate(createDate);
	}

	/**
	 * Sets the div ID of this pura.
	 *
	 * @param divId the div ID of this pura
	 */
	@Override
	public void setDivId(long divId) {
		_pura.setDivId(divId);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_pura.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_pura.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_pura.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the group ID of this pura.
	 *
	 * @param groupId the group ID of this pura
	 */
	@Override
	public void setGroupId(long groupId) {
		_pura.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this pura.
	 *
	 * @param modifiedDate the modified date of this pura
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_pura.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_pura.setNew(n);
	}

	/**
	 * Sets the nombre of this pura.
	 *
	 * @param nombre the nombre of this pura
	 */
	@Override
	public void setNombre(String nombre) {
		_pura.setNombre(nombre);
	}

	/**
	 * Sets the peso of this pura.
	 *
	 * @param peso the peso of this pura
	 */
	@Override
	public void setPeso(double peso) {
		_pura.setPeso(peso);
	}

	/**
	 * Sets the primary key of this pura.
	 *
	 * @param primaryKey the primary key of this pura
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_pura.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_pura.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the score of this pura.
	 *
	 * @param score the score of this pura
	 */
	@Override
	public void setScore(int score) {
		_pura.setScore(score);
	}

	/**
	 * Sets the user ID of this pura.
	 *
	 * @param userId the user ID of this pura
	 */
	@Override
	public void setUserId(long userId) {
		_pura.setUserId(userId);
	}

	/**
	 * Sets the user name of this pura.
	 *
	 * @param userName the user name of this pura
	 */
	@Override
	public void setUserName(String userName) {
		_pura.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this pura.
	 *
	 * @param userUuid the user uuid of this pura
	 */
	@Override
	public void setUserUuid(String userUuid) {
		_pura.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this pura.
	 *
	 * @param uuid the uuid of this pura
	 */
	@Override
	public void setUuid(String uuid) {
		_pura.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Pura> toCacheModel() {
		return _pura.toCacheModel();
	}

	@Override
	public Pura toEscapedModel() {
		return new PuraWrapper(_pura.toEscapedModel());
	}

	@Override
	public String toString() {
		return _pura.toString();
	}

	@Override
	public Pura toUnescapedModel() {
		return new PuraWrapper(_pura.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _pura.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof PuraWrapper)) {
			return false;
		}

		PuraWrapper puraWrapper = (PuraWrapper)obj;

		if (Objects.equals(_pura, puraWrapper._pura)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _pura.getStagedModelType();
	}

	@Override
	public Pura getWrappedModel() {
		return _pura;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _pura.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _pura.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_pura.resetOriginalValues();
	}

	private final Pura _pura;

}