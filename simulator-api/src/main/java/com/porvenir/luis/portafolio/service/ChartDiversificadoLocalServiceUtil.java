/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ChartDiversificado. This utility wraps
 * <code>com.porvenir.luis.portafolio.service.impl.ChartDiversificadoLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ChartDiversificadoLocalService
 * @generated
 */
@ProviderType
public class ChartDiversificadoLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.porvenir.luis.portafolio.service.impl.ChartDiversificadoLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the chart diversificado to the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartDiversificado the chart diversificado
	 * @return the chart diversificado that was added
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
		addChartDiversificado(
			com.porvenir.luis.portafolio.model.ChartDiversificado
				chartDiversificado) {

		return getService().addChartDiversificado(chartDiversificado);
	}

	public static com.porvenir.luis.portafolio.model.ChartDiversificado
			addChartDiversificado(
				long userId, String nombre, int tiempo, double inferior,
				double medio, double superior,
				com.liferay.portal.kernel.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().addChartDiversificado(
			userId, nombre, tiempo, inferior, medio, superior, context);
	}

	/**
	 * Creates a new chart diversificado with the primary key. Does not add the chart diversificado to the database.
	 *
	 * @param id the primary key for the new chart diversificado
	 * @return the new chart diversificado
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
		createChartDiversificado(long id) {

		return getService().createChartDiversificado(id);
	}

	public static void deleteAll() {
		getService().deleteAll();
	}

	/**
	 * Deletes the chart diversificado from the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartDiversificado the chart diversificado
	 * @return the chart diversificado that was removed
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
		deleteChartDiversificado(
			com.porvenir.luis.portafolio.model.ChartDiversificado
				chartDiversificado) {

		return getService().deleteChartDiversificado(chartDiversificado);
	}

	/**
	 * Deletes the chart diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado that was removed
	 * @throws PortalException if a chart diversificado with the primary key could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
			deleteChartDiversificado(long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteChartDiversificado(id);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.porvenir.luis.portafolio.model.ChartDiversificado
		fetchChartDiversificado(long id) {

		return getService().fetchChartDiversificado(id);
	}

	/**
	 * Returns the chart diversificado matching the UUID and group.
	 *
	 * @param uuid the chart diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
		fetchChartDiversificadoByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchChartDiversificadoByUuidAndGroupId(
			uuid, groupId);
	}

	public static com.porvenir.luis.portafolio.model.ChartDiversificado
			findByName_Time(String name, int time)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return getService().findByName_Time(name, time);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static java.util.List
		<com.porvenir.luis.portafolio.model.ChartDiversificado> getByName(
			String name, int max) {

		return getService().getByName(name, max);
	}

	/**
	 * Returns the chart diversificado with the primary key.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado
	 * @throws PortalException if a chart diversificado with the primary key could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
			getChartDiversificado(long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getChartDiversificado(id);
	}

	/**
	 * Returns the chart diversificado matching the UUID and group.
	 *
	 * @param uuid the chart diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart diversificado
	 * @throws PortalException if a matching chart diversificado could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
			getChartDiversificadoByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getChartDiversificadoByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of chart diversificados
	 */
	public static java.util.List
		<com.porvenir.luis.portafolio.model.ChartDiversificado>
			getChartDiversificados(int start, int end) {

		return getService().getChartDiversificados(start, end);
	}

	/**
	 * Returns all the chart diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart diversificados
	 * @param companyId the primary key of the company
	 * @return the matching chart diversificados, or an empty list if no matches were found
	 */
	public static java.util.List
		<com.porvenir.luis.portafolio.model.ChartDiversificado>
			getChartDiversificadosByUuidAndCompanyId(
				String uuid, long companyId) {

		return getService().getChartDiversificadosByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of chart diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart diversificados
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching chart diversificados, or an empty list if no matches were found
	 */
	public static java.util.List
		<com.porvenir.luis.portafolio.model.ChartDiversificado>
			getChartDiversificadosByUuidAndCompanyId(
				String uuid, long companyId, int start, int end,
				com.liferay.portal.kernel.util.OrderByComparator
					<com.porvenir.luis.portafolio.model.ChartDiversificado>
						orderByComparator) {

		return getService().getChartDiversificadosByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of chart diversificados.
	 *
	 * @return the number of chart diversificados
	 */
	public static int getChartDiversificadosCount() {
		return getService().getChartDiversificadosCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the chart diversificado in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param chartDiversificado the chart diversificado
	 * @return the chart diversificado that was updated
	 */
	public static com.porvenir.luis.portafolio.model.ChartDiversificado
		updateChartDiversificado(
			com.porvenir.luis.portafolio.model.ChartDiversificado
				chartDiversificado) {

		return getService().updateChartDiversificado(chartDiversificado);
	}

	public static ChartDiversificadoLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker
		<ChartDiversificadoLocalService, ChartDiversificadoLocalService>
			_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(
			ChartDiversificadoLocalService.class);

		ServiceTracker
			<ChartDiversificadoLocalService, ChartDiversificadoLocalService>
				serviceTracker =
					new ServiceTracker
						<ChartDiversificadoLocalService,
						 ChartDiversificadoLocalService>(
							 bundle.getBundleContext(),
							 ChartDiversificadoLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}