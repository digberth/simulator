/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class DiversificadoSoap implements Serializable {

	public static DiversificadoSoap toSoapModel(Diversificado model) {
		DiversificadoSoap soapModel = new DiversificadoSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setDivId(model.getDivId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setScore(model.getScore());
		soapModel.setNombre(model.getNombre());
		soapModel.setSuma_peso(model.getSuma_peso());
		soapModel.setFactor_riesgo(model.getFactor_riesgo());
		soapModel.setDescobertura(model.getDescobertura());

		return soapModel;
	}

	public static DiversificadoSoap[] toSoapModels(Diversificado[] models) {
		DiversificadoSoap[] soapModels = new DiversificadoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DiversificadoSoap[][] toSoapModels(Diversificado[][] models) {
		DiversificadoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DiversificadoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DiversificadoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DiversificadoSoap[] toSoapModels(List<Diversificado> models) {
		List<DiversificadoSoap> soapModels = new ArrayList<DiversificadoSoap>(
			models.size());

		for (Diversificado model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DiversificadoSoap[soapModels.size()]);
	}

	public DiversificadoSoap() {
	}

	public long getPrimaryKey() {
		return _divId;
	}

	public void setPrimaryKey(long pk) {
		setDivId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getDivId() {
		return _divId;
	}

	public void setDivId(long divId) {
		_divId = divId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public int getScore() {
		return _score;
	}

	public void setScore(int score) {
		_score = score;
	}

	public String getNombre() {
		return _nombre;
	}

	public void setNombre(String nombre) {
		_nombre = nombre;
	}

	public double getSuma_peso() {
		return _suma_peso;
	}

	public void setSuma_peso(double suma_peso) {
		_suma_peso = suma_peso;
	}

	public String getFactor_riesgo() {
		return _factor_riesgo;
	}

	public void setFactor_riesgo(String factor_riesgo) {
		_factor_riesgo = factor_riesgo;
	}

	public double getDescobertura() {
		return _descobertura;
	}

	public void setDescobertura(double descobertura) {
		_descobertura = descobertura;
	}

	private String _uuid;
	private long _divId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private int _score;
	private String _nombre;
	private double _suma_peso;
	private String _factor_riesgo;
	private double _descobertura;

}