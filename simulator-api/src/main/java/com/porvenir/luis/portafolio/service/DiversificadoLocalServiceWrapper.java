/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DiversificadoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DiversificadoLocalService
 * @generated
 */
@ProviderType
public class DiversificadoLocalServiceWrapper
	implements DiversificadoLocalService,
			   ServiceWrapper<DiversificadoLocalService> {

	public DiversificadoLocalServiceWrapper(
		DiversificadoLocalService diversificadoLocalService) {

		_diversificadoLocalService = diversificadoLocalService;
	}

	/**
	 * Adds the diversificado to the database. Also notifies the appropriate model listeners.
	 *
	 * @param diversificado the diversificado
	 * @return the diversificado that was added
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado addDiversificado(
		com.porvenir.luis.portafolio.model.Diversificado diversificado) {

		return _diversificadoLocalService.addDiversificado(diversificado);
	}

	@Override
	public com.porvenir.luis.portafolio.model.Diversificado addDiversificado(
			long userId, int score, String nombre, double sumaPeso,
			String factorRiesgo, double descobertura,
			com.liferay.portal.kernel.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _diversificadoLocalService.addDiversificado(
			userId, score, nombre, sumaPeso, factorRiesgo, descobertura,
			context);
	}

	/**
	 * Creates a new diversificado with the primary key. Does not add the diversificado to the database.
	 *
	 * @param divId the primary key for the new diversificado
	 * @return the new diversificado
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado createDiversificado(
		long divId) {

		return _diversificadoLocalService.createDiversificado(divId);
	}

	@Override
	public void deleteAll() {
		_diversificadoLocalService.deleteAll();
	}

	/**
	 * Deletes the diversificado from the database. Also notifies the appropriate model listeners.
	 *
	 * @param diversificado the diversificado
	 * @return the diversificado that was removed
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado deleteDiversificado(
		com.porvenir.luis.portafolio.model.Diversificado diversificado) {

		return _diversificadoLocalService.deleteDiversificado(diversificado);
	}

	/**
	 * Deletes the diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado that was removed
	 * @throws PortalException if a diversificado with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado deleteDiversificado(
			long divId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _diversificadoLocalService.deleteDiversificado(divId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _diversificadoLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _diversificadoLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _diversificadoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _diversificadoLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _diversificadoLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _diversificadoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _diversificadoLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.porvenir.luis.portafolio.model.Diversificado fetchDiversificado(
		long divId) {

		return _diversificadoLocalService.fetchDiversificado(divId);
	}

	/**
	 * Returns the diversificado matching the UUID and group.
	 *
	 * @param uuid the diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching diversificado, or <code>null</code> if a matching diversificado could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado
		fetchDiversificadoByUuidAndGroupId(String uuid, long groupId) {

		return _diversificadoLocalService.fetchDiversificadoByUuidAndGroupId(
			uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _diversificadoLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Diversificado>
		getByScore(int score) {

		return _diversificadoLocalService.getByScore(score);
	}

	/**
	 * Returns the diversificado with the primary key.
	 *
	 * @param divId the primary key of the diversificado
	 * @return the diversificado
	 * @throws PortalException if a diversificado with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado getDiversificado(
			long divId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _diversificadoLocalService.getDiversificado(divId);
	}

	/**
	 * Returns the diversificado matching the UUID and group.
	 *
	 * @param uuid the diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching diversificado
	 * @throws PortalException if a matching diversificado could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado
			getDiversificadoByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _diversificadoLocalService.getDiversificadoByUuidAndGroupId(
			uuid, groupId);
	}

	/**
	 * Returns a range of all the diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.DiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @return the range of diversificados
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Diversificado>
		getDiversificados(int start, int end) {

		return _diversificadoLocalService.getDiversificados(start, end);
	}

	/**
	 * Returns all the diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the diversificados
	 * @param companyId the primary key of the company
	 * @return the matching diversificados, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Diversificado>
		getDiversificadosByUuidAndCompanyId(String uuid, long companyId) {

		return _diversificadoLocalService.getDiversificadosByUuidAndCompanyId(
			uuid, companyId);
	}

	/**
	 * Returns a range of diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the diversificados
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of diversificados
	 * @param end the upper bound of the range of diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching diversificados, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.Diversificado>
		getDiversificadosByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.porvenir.luis.portafolio.model.Diversificado>
					orderByComparator) {

		return _diversificadoLocalService.getDiversificadosByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of diversificados.
	 *
	 * @return the number of diversificados
	 */
	@Override
	public int getDiversificadosCount() {
		return _diversificadoLocalService.getDiversificadosCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _diversificadoLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _diversificadoLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _diversificadoLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _diversificadoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the diversificado in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param diversificado the diversificado
	 * @return the diversificado that was updated
	 */
	@Override
	public com.porvenir.luis.portafolio.model.Diversificado updateDiversificado(
		com.porvenir.luis.portafolio.model.Diversificado diversificado) {

		return _diversificadoLocalService.updateDiversificado(diversificado);
	}

	@Override
	public DiversificadoLocalService getWrappedService() {
		return _diversificadoLocalService;
	}

	@Override
	public void setWrappedService(
		DiversificadoLocalService diversificadoLocalService) {

		_diversificadoLocalService = diversificadoLocalService;
	}

	private DiversificadoLocalService _diversificadoLocalService;

}