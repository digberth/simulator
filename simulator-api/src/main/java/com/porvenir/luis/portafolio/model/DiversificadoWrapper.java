/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.exportimport.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Diversificado}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Diversificado
 * @generated
 */
@ProviderType
public class DiversificadoWrapper
	implements Diversificado, ModelWrapper<Diversificado> {

	public DiversificadoWrapper(Diversificado diversificado) {
		_diversificado = diversificado;
	}

	@Override
	public Class<?> getModelClass() {
		return Diversificado.class;
	}

	@Override
	public String getModelClassName() {
		return Diversificado.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("divId", getDivId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("score", getScore());
		attributes.put("nombre", getNombre());
		attributes.put("suma_peso", getSuma_peso());
		attributes.put("factor_riesgo", getFactor_riesgo());
		attributes.put("descobertura", getDescobertura());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long divId = (Long)attributes.get("divId");

		if (divId != null) {
			setDivId(divId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		Integer score = (Integer)attributes.get("score");

		if (score != null) {
			setScore(score);
		}

		String nombre = (String)attributes.get("nombre");

		if (nombre != null) {
			setNombre(nombre);
		}

		Double suma_peso = (Double)attributes.get("suma_peso");

		if (suma_peso != null) {
			setSuma_peso(suma_peso);
		}

		String factor_riesgo = (String)attributes.get("factor_riesgo");

		if (factor_riesgo != null) {
			setFactor_riesgo(factor_riesgo);
		}

		Double descobertura = (Double)attributes.get("descobertura");

		if (descobertura != null) {
			setDescobertura(descobertura);
		}
	}

	@Override
	public Object clone() {
		return new DiversificadoWrapper((Diversificado)_diversificado.clone());
	}

	@Override
	public int compareTo(Diversificado diversificado) {
		return _diversificado.compareTo(diversificado);
	}

	/**
	 * Returns the company ID of this diversificado.
	 *
	 * @return the company ID of this diversificado
	 */
	@Override
	public long getCompanyId() {
		return _diversificado.getCompanyId();
	}

	/**
	 * Returns the create date of this diversificado.
	 *
	 * @return the create date of this diversificado
	 */
	@Override
	public Date getCreateDate() {
		return _diversificado.getCreateDate();
	}

	/**
	 * Returns the descobertura of this diversificado.
	 *
	 * @return the descobertura of this diversificado
	 */
	@Override
	public double getDescobertura() {
		return _diversificado.getDescobertura();
	}

	/**
	 * Returns the div ID of this diversificado.
	 *
	 * @return the div ID of this diversificado
	 */
	@Override
	public long getDivId() {
		return _diversificado.getDivId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _diversificado.getExpandoBridge();
	}

	/**
	 * Returns the factor_riesgo of this diversificado.
	 *
	 * @return the factor_riesgo of this diversificado
	 */
	@Override
	public String getFactor_riesgo() {
		return _diversificado.getFactor_riesgo();
	}

	/**
	 * Returns the group ID of this diversificado.
	 *
	 * @return the group ID of this diversificado
	 */
	@Override
	public long getGroupId() {
		return _diversificado.getGroupId();
	}

	/**
	 * Returns the modified date of this diversificado.
	 *
	 * @return the modified date of this diversificado
	 */
	@Override
	public Date getModifiedDate() {
		return _diversificado.getModifiedDate();
	}

	/**
	 * Returns the nombre of this diversificado.
	 *
	 * @return the nombre of this diversificado
	 */
	@Override
	public String getNombre() {
		return _diversificado.getNombre();
	}

	/**
	 * Returns the primary key of this diversificado.
	 *
	 * @return the primary key of this diversificado
	 */
	@Override
	public long getPrimaryKey() {
		return _diversificado.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _diversificado.getPrimaryKeyObj();
	}

	/**
	 * Returns the score of this diversificado.
	 *
	 * @return the score of this diversificado
	 */
	@Override
	public int getScore() {
		return _diversificado.getScore();
	}

	/**
	 * Returns the suma_peso of this diversificado.
	 *
	 * @return the suma_peso of this diversificado
	 */
	@Override
	public double getSuma_peso() {
		return _diversificado.getSuma_peso();
	}

	/**
	 * Returns the user ID of this diversificado.
	 *
	 * @return the user ID of this diversificado
	 */
	@Override
	public long getUserId() {
		return _diversificado.getUserId();
	}

	/**
	 * Returns the user name of this diversificado.
	 *
	 * @return the user name of this diversificado
	 */
	@Override
	public String getUserName() {
		return _diversificado.getUserName();
	}

	/**
	 * Returns the user uuid of this diversificado.
	 *
	 * @return the user uuid of this diversificado
	 */
	@Override
	public String getUserUuid() {
		return _diversificado.getUserUuid();
	}

	/**
	 * Returns the uuid of this diversificado.
	 *
	 * @return the uuid of this diversificado
	 */
	@Override
	public String getUuid() {
		return _diversificado.getUuid();
	}

	@Override
	public int hashCode() {
		return _diversificado.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _diversificado.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _diversificado.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _diversificado.isNew();
	}

	@Override
	public void persist() {
		_diversificado.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_diversificado.setCachedModel(cachedModel);
	}

	/**
	 * Sets the company ID of this diversificado.
	 *
	 * @param companyId the company ID of this diversificado
	 */
	@Override
	public void setCompanyId(long companyId) {
		_diversificado.setCompanyId(companyId);
	}

	/**
	 * Sets the create date of this diversificado.
	 *
	 * @param createDate the create date of this diversificado
	 */
	@Override
	public void setCreateDate(Date createDate) {
		_diversificado.setCreateDate(createDate);
	}

	/**
	 * Sets the descobertura of this diversificado.
	 *
	 * @param descobertura the descobertura of this diversificado
	 */
	@Override
	public void setDescobertura(double descobertura) {
		_diversificado.setDescobertura(descobertura);
	}

	/**
	 * Sets the div ID of this diversificado.
	 *
	 * @param divId the div ID of this diversificado
	 */
	@Override
	public void setDivId(long divId) {
		_diversificado.setDivId(divId);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {

		_diversificado.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_diversificado.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_diversificado.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	 * Sets the factor_riesgo of this diversificado.
	 *
	 * @param factor_riesgo the factor_riesgo of this diversificado
	 */
	@Override
	public void setFactor_riesgo(String factor_riesgo) {
		_diversificado.setFactor_riesgo(factor_riesgo);
	}

	/**
	 * Sets the group ID of this diversificado.
	 *
	 * @param groupId the group ID of this diversificado
	 */
	@Override
	public void setGroupId(long groupId) {
		_diversificado.setGroupId(groupId);
	}

	/**
	 * Sets the modified date of this diversificado.
	 *
	 * @param modifiedDate the modified date of this diversificado
	 */
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_diversificado.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_diversificado.setNew(n);
	}

	/**
	 * Sets the nombre of this diversificado.
	 *
	 * @param nombre the nombre of this diversificado
	 */
	@Override
	public void setNombre(String nombre) {
		_diversificado.setNombre(nombre);
	}

	/**
	 * Sets the primary key of this diversificado.
	 *
	 * @param primaryKey the primary key of this diversificado
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		_diversificado.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_diversificado.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	 * Sets the score of this diversificado.
	 *
	 * @param score the score of this diversificado
	 */
	@Override
	public void setScore(int score) {
		_diversificado.setScore(score);
	}

	/**
	 * Sets the suma_peso of this diversificado.
	 *
	 * @param suma_peso the suma_peso of this diversificado
	 */
	@Override
	public void setSuma_peso(double suma_peso) {
		_diversificado.setSuma_peso(suma_peso);
	}

	/**
	 * Sets the user ID of this diversificado.
	 *
	 * @param userId the user ID of this diversificado
	 */
	@Override
	public void setUserId(long userId) {
		_diversificado.setUserId(userId);
	}

	/**
	 * Sets the user name of this diversificado.
	 *
	 * @param userName the user name of this diversificado
	 */
	@Override
	public void setUserName(String userName) {
		_diversificado.setUserName(userName);
	}

	/**
	 * Sets the user uuid of this diversificado.
	 *
	 * @param userUuid the user uuid of this diversificado
	 */
	@Override
	public void setUserUuid(String userUuid) {
		_diversificado.setUserUuid(userUuid);
	}

	/**
	 * Sets the uuid of this diversificado.
	 *
	 * @param uuid the uuid of this diversificado
	 */
	@Override
	public void setUuid(String uuid) {
		_diversificado.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Diversificado>
		toCacheModel() {

		return _diversificado.toCacheModel();
	}

	@Override
	public Diversificado toEscapedModel() {
		return new DiversificadoWrapper(_diversificado.toEscapedModel());
	}

	@Override
	public String toString() {
		return _diversificado.toString();
	}

	@Override
	public Diversificado toUnescapedModel() {
		return new DiversificadoWrapper(_diversificado.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _diversificado.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof DiversificadoWrapper)) {
			return false;
		}

		DiversificadoWrapper diversificadoWrapper = (DiversificadoWrapper)obj;

		if (Objects.equals(
				_diversificado, diversificadoWrapper._diversificado)) {

			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _diversificado.getStagedModelType();
	}

	@Override
	public Diversificado getWrappedModel() {
		return _diversificado;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _diversificado.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _diversificado.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_diversificado.resetOriginalValues();
	}

	private final Diversificado _diversificado;

}