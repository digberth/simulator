/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ChartPura. This utility wraps
 * <code>com.porvenir.luis.portafolio.service.impl.ChartPuraLocalServiceImpl</code> and
 * is an access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ChartPuraLocalService
 * @generated
 */
@ProviderType
public class ChartPuraLocalServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>com.porvenir.luis.portafolio.service.impl.ChartPuraLocalServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the chart pura to the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartPura the chart pura
	 * @return the chart pura that was added
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura addChartPura(
		com.porvenir.luis.portafolio.model.ChartPura chartPura) {

		return getService().addChartPura(chartPura);
	}

	public static com.porvenir.luis.portafolio.model.ChartPura addChartPura(
			long userId, int score, int tiempo, double inferior, double medio,
			double superior,
			com.liferay.portal.kernel.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().addChartPura(
			userId, score, tiempo, inferior, medio, superior, context);
	}

	/**
	 * Creates a new chart pura with the primary key. Does not add the chart pura to the database.
	 *
	 * @param id the primary key for the new chart pura
	 * @return the new chart pura
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura createChartPura(
		long id) {

		return getService().createChartPura(id);
	}

	public static void deleteAll() {
		getService().deleteAll();
	}

	/**
	 * Deletes the chart pura from the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartPura the chart pura
	 * @return the chart pura that was removed
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura deleteChartPura(
		com.porvenir.luis.portafolio.model.ChartPura chartPura) {

		return getService().deleteChartPura(chartPura);
	}

	/**
	 * Deletes the chart pura with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura that was removed
	 * @throws PortalException if a chart pura with the primary key could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura deleteChartPura(
			long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deleteChartPura(id);
	}

	/**
	 * @throws PortalException
	 */
	public static com.liferay.portal.kernel.model.PersistedModel
			deletePersistedModel(
				com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery
		dynamicQuery() {

		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return getService().dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static com.porvenir.luis.portafolio.model.ChartPura fetchChartPura(
		long id) {

		return getService().fetchChartPura(id);
	}

	/**
	 * Returns the chart pura matching the UUID and group.
	 *
	 * @param uuid the chart pura's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura
		fetchChartPuraByUuidAndGroupId(String uuid, long groupId) {

		return getService().fetchChartPuraByUuidAndGroupId(uuid, groupId);
	}

	public static com.porvenir.luis.portafolio.model.ChartPura findByScore_Time(
			int score, int time)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getService().findByScore_Time(score, time);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return getService().getActionableDynamicQuery();
	}

	public static java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getByScore(int score, int max) {

		return getService().getByScore(score, max);
	}

	/**
	 * Returns the chart pura with the primary key.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura
	 * @throws PortalException if a chart pura with the primary key could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura getChartPura(
			long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getChartPura(id);
	}

	/**
	 * Returns the chart pura matching the UUID and group.
	 *
	 * @param uuid the chart pura's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart pura
	 * @throws PortalException if a matching chart pura could not be found
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura
			getChartPuraByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getChartPuraByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of chart puras
	 */
	public static java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getChartPuras(int start, int end) {

		return getService().getChartPuras(start, end);
	}

	/**
	 * Returns all the chart puras matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart puras
	 * @param companyId the primary key of the company
	 * @return the matching chart puras, or an empty list if no matches were found
	 */
	public static java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getChartPurasByUuidAndCompanyId(String uuid, long companyId) {

		return getService().getChartPurasByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of chart puras matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart puras
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching chart puras, or an empty list if no matches were found
	 */
	public static java.util.List<com.porvenir.luis.portafolio.model.ChartPura>
		getChartPurasByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.porvenir.luis.portafolio.model.ChartPura>
					orderByComparator) {

		return getService().getChartPurasByUuidAndCompanyId(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of chart puras.
	 *
	 * @return the number of chart puras
	 */
	public static int getChartPurasCount() {
		return getService().getChartPurasCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static
		com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
			getIndexableActionableDynamicQuery() {

		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel
			getPersistedModel(java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the chart pura in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param chartPura the chart pura
	 * @return the chart pura that was updated
	 */
	public static com.porvenir.luis.portafolio.model.ChartPura updateChartPura(
		com.porvenir.luis.portafolio.model.ChartPura chartPura) {

		return getService().updateChartPura(chartPura);
	}

	public static ChartPuraLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ChartPuraLocalService, ChartPuraLocalService>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ChartPuraLocalService.class);

		ServiceTracker<ChartPuraLocalService, ChartPuraLocalService>
			serviceTracker =
				new ServiceTracker
					<ChartPuraLocalService, ChartPuraLocalService>(
						bundle.getBundleContext(), ChartPuraLocalService.class,
						null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}