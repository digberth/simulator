/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.porvenir.luis.portafolio.exception.NoSuchChartPuraException;
import com.porvenir.luis.portafolio.model.ChartPura;

import java.io.Serializable;

import java.util.Map;
import java.util.Set;

/**
 * The persistence interface for the chart pura service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartPuraUtil
 * @generated
 */
@ProviderType
public interface ChartPuraPersistence extends BasePersistence<ChartPura> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ChartPuraUtil} to access the chart pura persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */
	@Override
	public Map<Serializable, ChartPura> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys);

	/**
	 * Returns all the chart puras where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid(String uuid);

	/**
	 * Returns a range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid(
		String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public ChartPura[] findByUuid_PrevAndNext(
			long id, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Removes all the chart puras where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of chart puras where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching chart puras
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByUUID_G(String uuid, long groupId)
		throws NoSuchChartPuraException;

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache);

	/**
	 * Removes the chart pura where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the chart pura that was removed
	 */
	public ChartPura removeByUUID_G(String uuid, long groupId)
		throws NoSuchChartPuraException;

	/**
	 * Returns the number of chart puras where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching chart puras
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid_C(String uuid, long companyId);

	/**
	 * Returns a range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end);

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	public java.util.List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByUuid_C_First(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByUuid_C_First(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByUuid_C_Last(
			String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByUuid_C_Last(
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public ChartPura[] findByUuid_C_PrevAndNext(
			long id, String uuid, long companyId,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Removes all the chart puras where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public void removeByUuid_C(String uuid, long companyId);

	/**
	 * Returns the number of chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching chart puras
	 */
	public int countByUuid_C(String uuid, long companyId);

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByScore_Tiempo(int score, int tiempo)
		throws NoSuchChartPuraException;

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByScore_Tiempo(int score, int tiempo);

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByScore_Tiempo(
		int score, int tiempo, boolean retrieveFromCache);

	/**
	 * Removes the chart pura where score = &#63; and tiempo = &#63; from the database.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the chart pura that was removed
	 */
	public ChartPura removeByScore_Tiempo(int score, int tiempo)
		throws NoSuchChartPuraException;

	/**
	 * Returns the number of chart puras where score = &#63; and tiempo = &#63;.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the number of matching chart puras
	 */
	public int countByScore_Tiempo(int score, int tiempo);

	/**
	 * Returns all the chart puras where score = &#63;.
	 *
	 * @param score the score
	 * @return the matching chart puras
	 */
	public java.util.List<ChartPura> findByScore(int score);

	/**
	 * Returns a range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	public java.util.List<ChartPura> findByScore(int score, int start, int end);

	/**
	 * Returns an ordered range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	public java.util.List<ChartPura> findByScore(
		int score, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	public java.util.List<ChartPura> findByScore(
		int score, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Returns the first chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByScore_First(
			int score,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Returns the first chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByScore_First(
		int score,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns the last chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public ChartPura findByScore_Last(
			int score,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Returns the last chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public ChartPura fetchByScore_Last(
		int score,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where score = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public ChartPura[] findByScore_PrevAndNext(
			long id, int score,
			com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
				orderByComparator)
		throws NoSuchChartPuraException;

	/**
	 * Removes all the chart puras where score = &#63; from the database.
	 *
	 * @param score the score
	 */
	public void removeByScore(int score);

	/**
	 * Returns the number of chart puras where score = &#63;.
	 *
	 * @param score the score
	 * @return the number of matching chart puras
	 */
	public int countByScore(int score);

	/**
	 * Caches the chart pura in the entity cache if it is enabled.
	 *
	 * @param chartPura the chart pura
	 */
	public void cacheResult(ChartPura chartPura);

	/**
	 * Caches the chart puras in the entity cache if it is enabled.
	 *
	 * @param chartPuras the chart puras
	 */
	public void cacheResult(java.util.List<ChartPura> chartPuras);

	/**
	 * Creates a new chart pura with the primary key. Does not add the chart pura to the database.
	 *
	 * @param id the primary key for the new chart pura
	 * @return the new chart pura
	 */
	public ChartPura create(long id);

	/**
	 * Removes the chart pura with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura that was removed
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public ChartPura remove(long id) throws NoSuchChartPuraException;

	public ChartPura updateImpl(ChartPura chartPura);

	/**
	 * Returns the chart pura with the primary key or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public ChartPura findByPrimaryKey(long id) throws NoSuchChartPuraException;

	/**
	 * Returns the chart pura with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura, or <code>null</code> if a chart pura with the primary key could not be found
	 */
	public ChartPura fetchByPrimaryKey(long id);

	/**
	 * Returns all the chart puras.
	 *
	 * @return the chart puras
	 */
	public java.util.List<ChartPura> findAll();

	/**
	 * Returns a range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of chart puras
	 */
	public java.util.List<ChartPura> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of chart puras
	 */
	public java.util.List<ChartPura> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator);

	/**
	 * Returns an ordered range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of chart puras
	 */
	public java.util.List<ChartPura> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ChartPura>
			orderByComparator,
		boolean retrieveFromCache);

	/**
	 * Removes all the chart puras from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of chart puras.
	 *
	 * @return the number of chart puras
	 */
	public int countAll();

	@Override
	public Set<String> getBadColumnNames();

}