/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.porvenir.luis.portafolio.model.ChartPura;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the chart pura service. This utility wraps <code>com.porvenir.luis.portafolio.service.persistence.impl.ChartPuraPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ChartPuraPersistence
 * @generated
 */
@ProviderType
public class ChartPuraUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ChartPura chartPura) {
		getPersistence().clearCache(chartPura);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, ChartPura> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ChartPura> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ChartPura> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ChartPura> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ChartPura update(ChartPura chartPura) {
		return getPersistence().update(chartPura);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ChartPura update(
		ChartPura chartPura, ServiceContext serviceContext) {

		return getPersistence().update(chartPura, serviceContext);
	}

	/**
	 * Returns all the chart puras where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching chart puras
	 */
	public static List<ChartPura> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	public static List<ChartPura> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	public static List<ChartPura> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	public static List<ChartPura> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByUuid_First(
			String uuid, OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByUuid_First(
		String uuid, OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByUuid_Last(
			String uuid, OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByUuid_Last(
		String uuid, OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where uuid = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public static ChartPura[] findByUuid_PrevAndNext(
			long id, String uuid,
			OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByUuid_PrevAndNext(
			id, uuid, orderByComparator);
	}

	/**
	 * Removes all the chart puras where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of chart puras where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching chart puras
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByUUID_G(String uuid, long groupId)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the chart pura where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByUUID_G(
		String uuid, long groupId, boolean retrieveFromCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	 * Removes the chart pura where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the chart pura that was removed
	 */
	public static ChartPura removeByUUID_G(String uuid, long groupId)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of chart puras where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching chart puras
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the matching chart puras
	 */
	public static List<ChartPura> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	 * Returns a range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	public static List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end) {

		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	public static List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	public static List<ChartPura> findByUuid_C(
		String uuid, long companyId, int start, int end,
		OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByUuid_C(
			uuid, companyId, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByUuid_C_First(
			String uuid, long companyId,
			OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the first chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByUuid_C_First(
		String uuid, long companyId,
		OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().fetchByUuid_C_First(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByUuid_C_Last(
			String uuid, long companyId,
			OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the last chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByUuid_C_Last(
		String uuid, long companyId,
		OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().fetchByUuid_C_Last(
			uuid, companyId, orderByComparator);
	}

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where uuid = &#63; and companyId = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public static ChartPura[] findByUuid_C_PrevAndNext(
			long id, String uuid, long companyId,
			OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByUuid_C_PrevAndNext(
			id, uuid, companyId, orderByComparator);
	}

	/**
	 * Removes all the chart puras where uuid = &#63; and companyId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 */
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the number of chart puras where uuid = &#63; and companyId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param companyId the company ID
	 * @return the number of matching chart puras
	 */
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByScore_Tiempo(int score, int tiempo)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByScore_Tiempo(score, tiempo);
	}

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByScore_Tiempo(int score, int tiempo) {
		return getPersistence().fetchByScore_Tiempo(score, tiempo);
	}

	/**
	 * Returns the chart pura where score = &#63; and tiempo = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByScore_Tiempo(
		int score, int tiempo, boolean retrieveFromCache) {

		return getPersistence().fetchByScore_Tiempo(
			score, tiempo, retrieveFromCache);
	}

	/**
	 * Removes the chart pura where score = &#63; and tiempo = &#63; from the database.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the chart pura that was removed
	 */
	public static ChartPura removeByScore_Tiempo(int score, int tiempo)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().removeByScore_Tiempo(score, tiempo);
	}

	/**
	 * Returns the number of chart puras where score = &#63; and tiempo = &#63;.
	 *
	 * @param score the score
	 * @param tiempo the tiempo
	 * @return the number of matching chart puras
	 */
	public static int countByScore_Tiempo(int score, int tiempo) {
		return getPersistence().countByScore_Tiempo(score, tiempo);
	}

	/**
	 * Returns all the chart puras where score = &#63;.
	 *
	 * @param score the score
	 * @return the matching chart puras
	 */
	public static List<ChartPura> findByScore(int score) {
		return getPersistence().findByScore(score);
	}

	/**
	 * Returns a range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of matching chart puras
	 */
	public static List<ChartPura> findByScore(int score, int start, int end) {
		return getPersistence().findByScore(score, start, end);
	}

	/**
	 * Returns an ordered range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching chart puras
	 */
	public static List<ChartPura> findByScore(
		int score, int start, int end,
		OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().findByScore(
			score, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart puras where score = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param score the score
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching chart puras
	 */
	public static List<ChartPura> findByScore(
		int score, int start, int end,
		OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findByScore(
			score, start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Returns the first chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByScore_First(
			int score, OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByScore_First(score, orderByComparator);
	}

	/**
	 * Returns the first chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByScore_First(
		int score, OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().fetchByScore_First(score, orderByComparator);
	}

	/**
	 * Returns the last chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura
	 * @throws NoSuchChartPuraException if a matching chart pura could not be found
	 */
	public static ChartPura findByScore_Last(
			int score, OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByScore_Last(score, orderByComparator);
	}

	/**
	 * Returns the last chart pura in the ordered set where score = &#63;.
	 *
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching chart pura, or <code>null</code> if a matching chart pura could not be found
	 */
	public static ChartPura fetchByScore_Last(
		int score, OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().fetchByScore_Last(score, orderByComparator);
	}

	/**
	 * Returns the chart puras before and after the current chart pura in the ordered set where score = &#63;.
	 *
	 * @param id the primary key of the current chart pura
	 * @param score the score
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public static ChartPura[] findByScore_PrevAndNext(
			long id, int score, OrderByComparator<ChartPura> orderByComparator)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByScore_PrevAndNext(
			id, score, orderByComparator);
	}

	/**
	 * Removes all the chart puras where score = &#63; from the database.
	 *
	 * @param score the score
	 */
	public static void removeByScore(int score) {
		getPersistence().removeByScore(score);
	}

	/**
	 * Returns the number of chart puras where score = &#63;.
	 *
	 * @param score the score
	 * @return the number of matching chart puras
	 */
	public static int countByScore(int score) {
		return getPersistence().countByScore(score);
	}

	/**
	 * Caches the chart pura in the entity cache if it is enabled.
	 *
	 * @param chartPura the chart pura
	 */
	public static void cacheResult(ChartPura chartPura) {
		getPersistence().cacheResult(chartPura);
	}

	/**
	 * Caches the chart puras in the entity cache if it is enabled.
	 *
	 * @param chartPuras the chart puras
	 */
	public static void cacheResult(List<ChartPura> chartPuras) {
		getPersistence().cacheResult(chartPuras);
	}

	/**
	 * Creates a new chart pura with the primary key. Does not add the chart pura to the database.
	 *
	 * @param id the primary key for the new chart pura
	 * @return the new chart pura
	 */
	public static ChartPura create(long id) {
		return getPersistence().create(id);
	}

	/**
	 * Removes the chart pura with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura that was removed
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public static ChartPura remove(long id)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().remove(id);
	}

	public static ChartPura updateImpl(ChartPura chartPura) {
		return getPersistence().updateImpl(chartPura);
	}

	/**
	 * Returns the chart pura with the primary key or throws a <code>NoSuchChartPuraException</code> if it could not be found.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura
	 * @throws NoSuchChartPuraException if a chart pura with the primary key could not be found
	 */
	public static ChartPura findByPrimaryKey(long id)
		throws com.porvenir.luis.portafolio.exception.NoSuchChartPuraException {

		return getPersistence().findByPrimaryKey(id);
	}

	/**
	 * Returns the chart pura with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the chart pura
	 * @return the chart pura, or <code>null</code> if a chart pura with the primary key could not be found
	 */
	public static ChartPura fetchByPrimaryKey(long id) {
		return getPersistence().fetchByPrimaryKey(id);
	}

	/**
	 * Returns all the chart puras.
	 *
	 * @return the chart puras
	 */
	public static List<ChartPura> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @return the range of chart puras
	 */
	public static List<ChartPura> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of chart puras
	 */
	public static List<ChartPura> findAll(
		int start, int end, OrderByComparator<ChartPura> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the chart puras.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>ChartPuraModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart puras
	 * @param end the upper bound of the range of chart puras (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of chart puras
	 */
	public static List<ChartPura> findAll(
		int start, int end, OrderByComparator<ChartPura> orderByComparator,
		boolean retrieveFromCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, retrieveFromCache);
	}

	/**
	 * Removes all the chart puras from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of chart puras.
	 *
	 * @return the number of chart puras
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ChartPuraPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ChartPuraPersistence, ChartPuraPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ChartPuraPersistence.class);

		ServiceTracker<ChartPuraPersistence, ChartPuraPersistence>
			serviceTracker =
				new ServiceTracker<ChartPuraPersistence, ChartPuraPersistence>(
					bundle.getBundleContext(), ChartPuraPersistence.class,
					null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}