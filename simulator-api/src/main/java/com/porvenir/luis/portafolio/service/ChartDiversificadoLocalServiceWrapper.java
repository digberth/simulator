/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.porvenir.luis.portafolio.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ChartDiversificadoLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ChartDiversificadoLocalService
 * @generated
 */
@ProviderType
public class ChartDiversificadoLocalServiceWrapper
	implements ChartDiversificadoLocalService,
			   ServiceWrapper<ChartDiversificadoLocalService> {

	public ChartDiversificadoLocalServiceWrapper(
		ChartDiversificadoLocalService chartDiversificadoLocalService) {

		_chartDiversificadoLocalService = chartDiversificadoLocalService;
	}

	/**
	 * Adds the chart diversificado to the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartDiversificado the chart diversificado
	 * @return the chart diversificado that was added
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
		addChartDiversificado(
			com.porvenir.luis.portafolio.model.ChartDiversificado
				chartDiversificado) {

		return _chartDiversificadoLocalService.addChartDiversificado(
			chartDiversificado);
	}

	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
			addChartDiversificado(
				long userId, String nombre, int tiempo, double inferior,
				double medio, double superior,
				com.liferay.portal.kernel.service.ServiceContext context)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartDiversificadoLocalService.addChartDiversificado(
			userId, nombre, tiempo, inferior, medio, superior, context);
	}

	/**
	 * Creates a new chart diversificado with the primary key. Does not add the chart diversificado to the database.
	 *
	 * @param id the primary key for the new chart diversificado
	 * @return the new chart diversificado
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
		createChartDiversificado(long id) {

		return _chartDiversificadoLocalService.createChartDiversificado(id);
	}

	@Override
	public void deleteAll() {
		_chartDiversificadoLocalService.deleteAll();
	}

	/**
	 * Deletes the chart diversificado from the database. Also notifies the appropriate model listeners.
	 *
	 * @param chartDiversificado the chart diversificado
	 * @return the chart diversificado that was removed
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
		deleteChartDiversificado(
			com.porvenir.luis.portafolio.model.ChartDiversificado
				chartDiversificado) {

		return _chartDiversificadoLocalService.deleteChartDiversificado(
			chartDiversificado);
	}

	/**
	 * Deletes the chart diversificado with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado that was removed
	 * @throws PortalException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
			deleteChartDiversificado(long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartDiversificadoLocalService.deleteChartDiversificado(id);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartDiversificadoLocalService.deletePersistedModel(
			persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _chartDiversificadoLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _chartDiversificadoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _chartDiversificadoLocalService.dynamicQuery(
			dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _chartDiversificadoLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _chartDiversificadoLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _chartDiversificadoLocalService.dynamicQueryCount(
			dynamicQuery, projection);
	}

	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
		fetchChartDiversificado(long id) {

		return _chartDiversificadoLocalService.fetchChartDiversificado(id);
	}

	/**
	 * Returns the chart diversificado matching the UUID and group.
	 *
	 * @param uuid the chart diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart diversificado, or <code>null</code> if a matching chart diversificado could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
		fetchChartDiversificadoByUuidAndGroupId(String uuid, long groupId) {

		return _chartDiversificadoLocalService.
			fetchChartDiversificadoByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
			findByName_Time(String name, int time)
		throws com.porvenir.luis.portafolio.exception.
			NoSuchChartDiversificadoException {

		return _chartDiversificadoLocalService.findByName_Time(name, time);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _chartDiversificadoLocalService.getActionableDynamicQuery();
	}

	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartDiversificado>
		getByName(String name, int max) {

		return _chartDiversificadoLocalService.getByName(name, max);
	}

	/**
	 * Returns the chart diversificado with the primary key.
	 *
	 * @param id the primary key of the chart diversificado
	 * @return the chart diversificado
	 * @throws PortalException if a chart diversificado with the primary key could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
			getChartDiversificado(long id)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartDiversificadoLocalService.getChartDiversificado(id);
	}

	/**
	 * Returns the chart diversificado matching the UUID and group.
	 *
	 * @param uuid the chart diversificado's UUID
	 * @param groupId the primary key of the group
	 * @return the matching chart diversificado
	 * @throws PortalException if a matching chart diversificado could not be found
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
			getChartDiversificadoByUuidAndGroupId(String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartDiversificadoLocalService.
			getChartDiversificadoByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the chart diversificados.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code>), then the query will include the default ORDER BY logic from <code>com.porvenir.luis.portafolio.model.impl.ChartDiversificadoModelImpl</code>. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @return the range of chart diversificados
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartDiversificado>
		getChartDiversificados(int start, int end) {

		return _chartDiversificadoLocalService.getChartDiversificados(
			start, end);
	}

	/**
	 * Returns all the chart diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart diversificados
	 * @param companyId the primary key of the company
	 * @return the matching chart diversificados, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartDiversificado>
		getChartDiversificadosByUuidAndCompanyId(String uuid, long companyId) {

		return _chartDiversificadoLocalService.
			getChartDiversificadosByUuidAndCompanyId(uuid, companyId);
	}

	/**
	 * Returns a range of chart diversificados matching the UUID and company.
	 *
	 * @param uuid the UUID of the chart diversificados
	 * @param companyId the primary key of the company
	 * @param start the lower bound of the range of chart diversificados
	 * @param end the upper bound of the range of chart diversificados (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the range of matching chart diversificados, or an empty list if no matches were found
	 */
	@Override
	public java.util.List<com.porvenir.luis.portafolio.model.ChartDiversificado>
		getChartDiversificadosByUuidAndCompanyId(
			String uuid, long companyId, int start, int end,
			com.liferay.portal.kernel.util.OrderByComparator
				<com.porvenir.luis.portafolio.model.ChartDiversificado>
					orderByComparator) {

		return _chartDiversificadoLocalService.
			getChartDiversificadosByUuidAndCompanyId(
				uuid, companyId, start, end, orderByComparator);
	}

	/**
	 * Returns the number of chart diversificados.
	 *
	 * @return the number of chart diversificados
	 */
	@Override
	public int getChartDiversificadosCount() {
		return _chartDiversificadoLocalService.getChartDiversificadosCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery
		getExportActionableDynamicQuery(
			com.liferay.exportimport.kernel.lar.PortletDataContext
				portletDataContext) {

		return _chartDiversificadoLocalService.getExportActionableDynamicQuery(
			portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _chartDiversificadoLocalService.
			getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _chartDiversificadoLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _chartDiversificadoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the chart diversificado in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param chartDiversificado the chart diversificado
	 * @return the chart diversificado that was updated
	 */
	@Override
	public com.porvenir.luis.portafolio.model.ChartDiversificado
		updateChartDiversificado(
			com.porvenir.luis.portafolio.model.ChartDiversificado
				chartDiversificado) {

		return _chartDiversificadoLocalService.updateChartDiversificado(
			chartDiversificado);
	}

	@Override
	public ChartDiversificadoLocalService getWrappedService() {
		return _chartDiversificadoLocalService;
	}

	@Override
	public void setWrappedService(
		ChartDiversificadoLocalService chartDiversificadoLocalService) {

		_chartDiversificadoLocalService = chartDiversificadoLocalService;
	}

	private ChartDiversificadoLocalService _chartDiversificadoLocalService;

}